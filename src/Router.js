import React from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import { Actions, Scene, Router, Modal, Tabs, Drawer, Stack } from 'react-native-router-flux';
import { Images } from './utils'
import DrawerContent from './screens/drawer/DrawerContent';
import TabView from './screens/TabView';
import TabIcon from './screens/TabIcon';
import Icon from 'react-native-vector-icons/Ionicons';

import SplashScreen from './screens/SplashScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import HomeScreen from './screens/HomeScreen';
import ReportScreen from './screens/ReportScreen';
import DropDownScreen from './screens/DropDownScreen';
import SignupScreen from './screens/Signup';
import LoginScreen from './screens/Login';
import SigninScreen from './screens/Signin';
import OtpScreen from './screens/Otp';
import AllRestaurantScreen from './screens/AllRestaurants';
import OrderConfirmScreen from './screens/OrderConfirm';
import OrderStatusScreen from './screens/OrderStatus';
import PastOrderScreen from './screens/PastOrder';

import DriverOrderDeliveredScreen from './screens/DriverOrderDelivered';
import DriverOrderHistoryScreen from './screens/DriverOrderHistory';
import DriverOrderDetailsScreen from './screens/DriverOrderDetails';
import DriverProfileScreen from './screens/DriverProfile';
import DriverEditProfileScreen from './screens/DriverEditProfile';
import DriverDashboard2Screen from './screens/DriverDashboard2';
import DriverDashboard3Screen from './screens/DriverDashboard3';
import DriverDashboard4Screen from './screens/DriverDashboard4';
import OrderDetailsScreen from './screens/OrderDetail';
import NotificationScreen from './screens/Notification';
import CmsScreen from './screens/CmsScreen';


let ScreenWidth = Dimensions.get("window").width;
/**
 *  RouterComponent is rendered when that route matches the URL assicated with Component.
 *  initial :- this props Indicate the first Component shown when App launch in device.
 * check this link for more detail:- https://www.npmjs.com/package/react-native-router-flux
 */

const styles = StyleSheet.create({
  tabBarStyle: {
    height: 58,
    borderTopColor: '#E9E9E9',
    borderTopWidth: 1,
    opacity: 1,
    // justifyContent:'space-between',
    backgroundColor: '#FFFFFF',
    width: ScreenWidth,
  }
});

const RouterComponent = (props) => {
  const { authenticated, loginUserData } = props
  console.log('authenticatedauthenticated:: ', authenticated);
  //props.changeLoginUserData(loginUserData)
  return (
    <Router>
      <Scene key="root">
        <Scene
          key="splashScreen"
          component={SplashScreen}
          hideNavBar={true}
          //initial={true}
          initial={!authenticated}
        />
        <Scene
          key="signupscreen"
          component={SignupScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="loginscreen"
          component={LoginScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="signinscreen"
          component={SigninScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="otpscreen"
          component={OtpScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="allrestaurantScreen"
          component={AllRestaurantScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="orderconfirmscreen"
          component={OrderConfirmScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="orderstatusscreen"
          component={OrderStatusScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />

        <Scene
          key="pastorderscreen"
          component={PastOrderScreen}
          hideNavBar={true}
        //initial={true}
        //initial={!authenticated}
        />
        <Scene
          key="orderdetailscreen"
          component={OrderDetailsScreen}
          hideNavBar={true}
        //initial={true}
        //initial={!authenticated}
        />
        <Scene
          key="notification"
          component={NotificationScreen}
          hideNavBar={true}
        //initial={true}
        //initial={!authenticated}
        />
        <Scene
          key="cmsscreen"
          component={CmsScreen}
          hideNavBar={true}
        //initial={true}
        //initial={!authenticated}
        />
        <Scene
          key="welcomeScreen"
          component={WelcomeScreen}
          hideNavBar={true}
        //initial={true}
        //initial={!authenticated}
        />
        <Scene
          key="dropDownScreen"
          component={DropDownScreen}
          hideNavBar={true}
        //initial={true}
        />
        <Scene
          key="driverorderdeliveredscreen"
          component={DriverOrderDeliveredScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverorderhistoryscreen"
          component={DriverOrderHistoryScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverorderdetailsscreen"
          component={DriverOrderDetailsScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverprofilescreen"
          component={DriverProfileScreen}
          hideNavBar={true}
        //initial={true}
        //initial={!authenticated}
        />
        <Scene
          key="drivereditprofilescreen"
          component={DriverEditProfileScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverdashboard2screen"
          component={DriverDashboard2Screen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverdashboard3screen"
          component={DriverDashboard3Screen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverdashboard4screen"
          component={DriverDashboard4Screen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Scene
          key="driverprofile"
          component={DriverProfileScreen}
          hideNavBar={true}
        />
        <Scene
          key="reportScreen"
          component={ReportScreen}
          hideNavBar={true}
        //initial={true}
        // initial={!authenticated}
        />
        <Drawer
          hideNavBar
          key="drawer"
          initial={authenticated}
          onExit={() => {
            console.log('Drawer closed');
          }}
          onEnter={() => {
            console.log('Drawer opened');
          }}
          contentComponent={DrawerContent}
          drawerImage={Images.menuActive1}
          drawerWidth={ScreenWidth - 80}
        >
          <Scene
            hideNavBar
            panHandlers={null}
            initial={authenticated}
            key="tabsLog"
          >
            <Tabs
              key="tabbar"
              routeName="tabbar"
              backToInitial
              onTabOnPress={() => {
                console.log('Back to initial and also print this');
              }}
              showLabel={false}
              tabBarStyle={styles.tabBarStyle}
            >
              <Stack
                key="tab_1"
                icon={TabIcon}
                tabKey="t1"
                title="Order"
                initial={true}
              >
                <Scene
                  key="tab1"
                  component={HomeScreen}
                  title="Order"
                  icon={TabIcon}
                  hideNavBar={true}
                />
              </Stack>
              {/* <Stack
                key="tab_3"
                icon={TabIcon}
                tabKey="t3"
                title="Report"
                initial={false}
              >
                <Scene
                  key="tab3"
                  component={ReportScreen}
                  title="Report"
                  icon={TabIcon}
                  hideNavBar={true}
                />
              </Stack> */}
              <Stack
                key="tab_2"
                icon={TabIcon}
                tabKey="t2"
                title="Account"
                initial={false}
              >
                <Scene
                  key="tab2"
                  component={DriverProfileScreen}
                  title="Account"
                  icon={TabIcon}
                  hideNavBar={true}
                />
              </Stack>
            </Tabs>
          </Scene>
        </Drawer>
      </Scene>
    </Router>
  );
}

// const mapDispatchToProps = (dispatch) => {
//   return {
//     changeLoginUserData: (userData) => {

//       console.log('userDatauserDatauserDatauserData:: ',userData)
//       dispatch({
//         type: 'CHANGE_LOGIN_USERDATA',
//         payload: userData
//       })
//     }
//   }
// }

export default RouterComponent;