import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
  backgroundImage: {
    width: ScreenWidth,
    height: ScreenHeight,
  },
  pastorderholder: {},
  flex1: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  flex2: {
    display: 'flex',
    flexDirection: 'row',
  },
  orderproduct: {
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
  },
  orderheading1: {
    fontSize: 18,
    lineHeight: 20,
    fontWeight: 'bold',
    color: Color.blackColor,
  },
  orderheading2: {
    fontSize: 15,
    lineHeight: 20,
    fontWeight: 'bold',
    color: Color.blackColor,
  },
  ordertext: {
    fontSize: 15,
    lineHeight: 26,
    color: Color.greyColor,
  },
  ordertext1: {
    fontSize: 15,
    lineHeight: 20,
    color: Color.greyColor,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  ordercontent: {
    width: '50%',
  },
  orderprice: {
    position: 'absolute',
    top: '50%',
    right: 20,
  },
  detailsTopBorder: {
    borderTopWidth: 0.5,
    borderColor: Color.lightBlack,
  },
  BtnHolder: {
    display: 'flex',
    flexDirection: 'row',
  },
  Btn: {
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Color.orangeColor,
    textAlign: 'center',
    alignItems: 'center',
  },
  Btn1: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Color.greyColor,
  },
  signSubmit: {
    backgroundColor: Color.orangeColor,
    color: Color.whiteColor,
    textAlign: 'center',
    fontSize: 26,
    fontWeight: 'bold',
    borderRadius: 10,
    padding: 15,
    marginBottom: 15,
    alignSelf: 'stretch',
    flex: 1,
  },
});