import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    }, 
    pastorderholder:{
        
    },
    orderproduct:{
        display:'flex',
        flexDirection:'row', 
        position:'relative'
    }, 
    ordercontent:{
        width:"50%",
    },
    orderprice:{
        position:'absolute',
        top:'30%',
        right:20,
    }, 
    dldetails:{

    }
});