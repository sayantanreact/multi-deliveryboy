import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    }, 
    otpHolder:{
        display:'flex',
        flexDirection:'row',
        justifyContent: 'space-between',
        padding: 5
        
    },
    otpInput:{
        width:66,
        height:66, 
        backgroundColor: Color.grayDash,
        borderRadius:33,
        textAlign:'center',
        color: Color.whiteColor,
        fontSize:26,
        fontWeight:'bold',
        // marginLeft:10,
        // marginRight:10
    }

});