import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    profileico:{
        flexDirection:'row',
        justifyContent:'center'
    },
    driverinput:{
        borderBottomWidth:1,
        borderColor:Color.greyColor2,
    },
    driverinput2:{
        borderBottomWidth:1,
        borderColor:"#CACACA",
        paddingBottom:10,
        color:Color.blackColor
    },
    labeltext:{
        fontSize:14,
        color:Color.greyColor,
    },
    driverinpico:{
        position:'absolute',
        right:0,
        top:18
    },
    profiledit:{
        position:'absolute',
        right:0,
        top:0
    },
    driverinputholder:{
        borderBottomWidth:1,
        borderColor:Color.greyColor2,
        position:'relative',
    },
    driverinputholder2:{
        borderBottomWidth:1,
        borderColor:"#CACACA",
        position:'relative',
    },
    line:{ 
        borderTopWidth:1,
        borderColor:Color.greyColor2,
        position:'relative',
        top:15
    },  
    line2:{ 
        borderTopWidth:1,
        borderColor:"#CACACA",    
        position:'relative',
        top:17
    },
    inputfile:{ 
        paddingLeft:15,
        paddingRight:15,
        paddingTop:8,
        paddingBottom:8,
        backgroundColor:Color.orangeColor,
        color:Color.whiteColor,
        // marginBottom:10,
        borderRadius:10,
        width:120,
        textAlign:'center'
    },
    driverinput2holder:{
        borderRadius:10,
    }
});