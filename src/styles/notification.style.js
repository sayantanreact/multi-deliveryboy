import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    },  
    notificationlist:{

    },
    ntflex1:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
    },
    ntflex2:{
        display:'flex',
        flexDirection:'row', 
    },
    nthead1:{
        fontSize:16,
    },
    nttext:{
        fontSize:12,
    },
    nttext2:{
        fontSize:10,
        textTransform:'uppercase',
        color: Color.orangeColor,
    },
    nttext1:{
        fontSize:14,
    },
    notibtn:{
        borderRadius:5,
        paddingBottom:10,
        paddingTop:10,
        paddingLeft:10,
        paddingRight:10, 
        width:190,
        textAlign:'center',
    }
    
});