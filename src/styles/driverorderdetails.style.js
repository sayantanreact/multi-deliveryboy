import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    },  
    notificationlist:{

    },
    ntflex1:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
    },
    ntflex2:{
        display:'flex',
        flexDirection:'row', 
    },
    nthead1:{
        fontSize:16,
        lineHeight:16,
        fontWeight:"500",
        padding:10
    },
    nthead2:{
        fontSize:14,
        lineHeight:16,
        fontWeight:"400",
        padding:10
    },
    nttext:{
        fontSize:12,
        lineHeight:18,
        color:"#7C7C7C",
        padding:10

    },
    ntdetails:{
        borderTopWidth:1,
        borderTopColor:"#CACACA",
        paddingTop:10,
        borderStyle:'dotted'
    },
    notibtn:{
        borderRadius:10,
        paddingBottom:10,
        paddingTop:10,
        paddingLeft:25,
        paddingRight:25, 
        width:190,
        textAlign:'center',
    },
    ntborder:{
        borderBottomWidth:1,
        borderBottomColor:"#CACACA"
    }
    
});