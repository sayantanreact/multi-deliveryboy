import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { Images, Dimen, Color } from '../utils'
import { RF } from '../utils/responsive';
import Fonts, { fonts, fontSizes } from '../utils/Fonts';

import { ThemeColors } from './main.style';
import { color } from 'react-native-reanimated';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
    mainView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: "space-around",
        padding: 0,
        alignItems: 'center',
        marginBottom: 25,
        marginTop: 25,
        //backgroundColor: 'red'
    },
    headingtext: {
        textTransform: 'capitalize',
        fontSize: 26,
        fontWeight: "600",
        //marginRight: "42%",
        textAlign: "center",
        fontFamily: Fonts.semiBold
    },
    forgotpass: {
        // textAlign:'right',
        // justifyContent:"flex-end",
        alignItems: "flex-end",
        marginTop: 10,
        paddingRight: 20
    },
    forgotText: {
        fontWeight: "400",
        fontSize: RF(14),
        fontFamily: Fonts.regular,
        color: "#7C7C7C"

    },
    bottomSection: {
        flexDirection: "row", justifyContent: "center"
    },
    Alreadytext: {
        color: "#000000",
        fontFamily: Fonts.regular,
        fontSize: RF(14),

    },
    Siguptext: {
        fontFamily: Fonts.regular,
        fontSize: RF(14),
        color: "#FF7900"
    },
    conditiontextView: {
        marginTop: 15, width: "84%", paddingLeft: 0, flexDirection: "row"
    },
    insideText: {
        lineHeight: 20, fontWeight: "600", color: "grey", fontFamily: Fonts.regular, fontSize: 14
    },
    termText: {
        lineHeight: 20, fontWeight: "600", color: "#FF7900", marginLeft: 5, fontFamily: Fonts.regular, fontSize: 14
    },
    secView: {
        marginTop: 2, width: "84%", paddingLeft: 0, flexDirection: "row"
    },
    serviceText: {
        lineHeight: 20, fontWeight: "600", color: "#FF7900", fontFamily: Fonts.regular, fontSize: 14
    },
    andText: {
        lineHeight: 20, fontWeight: "600", color: "#7C7C7C", marginLeft: 5, fontFamily: Fonts.regular, fontSize: 14
    },
    policyText: {
        lineHeight: 20, fontWeight: "600", color: "#FF7900", marginLeft: 5, fontFamily: Fonts.regular, fontSize: 14
    }


});