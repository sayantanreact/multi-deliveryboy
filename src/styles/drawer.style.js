import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { ThemeColors } from './main.style';
import { Color } from '../utils'; 
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
  Container: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  Width: {
    width: ScreenWidth - 40,
  },
  GreyBg: {
    backgroundColor: '#F3F3F3',
  },
  BlueBg: {
    backgroundColor: '#0F1B46',
    padding: 15,
  },
  Logo: {
    marginTop: 15,
    marginBottom: 15,
  },
  ProfileImg: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  PicUpload: {
    marginTop: 5,
  },
  MenuListWrap: {
    backgroundColor: ThemeColors.whiteColor,
    paddingBottom: 15,
    paddingTop: 15,
  },
  Hello: {
    color: ThemeColors.whiteColor,
    fontSize: 16,
    marginLeft: 10,
  },
  MenuList: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    height: 42,
    marginBottom: 1,
    //backgroundColor: ThemeColors.lightgreyColor,
  },
  ListIcon: {
    marginRight: 10,
    width: 30,
  },
  ListTitle: {
    color: ThemeColors.blackColor,
    fontSize: 16,
    textTransform: 'uppercase',
  },
  LinkText: {
    color: ThemeColors.darkgreyColor,
    fontSize: 14,
    marginLeft: 6,
    marginRight: 6,
  },
  ActionBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  MenuBottom: {
    backgroundColor: ThemeColors.lightgreyColor,
    paddingTop: 15,
    paddingBottom: 15,
  },
  DrawerMenu: {
    paddingTop: 6,
    paddingBottom: 6,
    paddingLeft: 15,
    paddingRight: 15,
    height: 34,
  },
  DrawerMenuName: {
    color: ThemeColors.blackColor,
    fontSize: 14,
  },
  PowerbyWrap: {
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 30,
  },
  Currency: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 2,
    flexDirection: 'row',
  },
  modalMain2: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    // opacity:0.3
  },
  bottomModalContentHolder: {
    backgroundColor: Color.whiteColor,
    width: '100%',
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 30,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
  },
  modalMain1: {
    backgroundColor: Color.blackColor,
    height: '100%',
  },
  leftModalContentHolder: {
    backgroundColor: Color.whiteColor,
    width: '100%',
    height: '100%',
  },
  modalhead: {
    fontSize: 20,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    padding: 15,
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderColor: Color.bordergrey1,
  },
  modalheadtext: {
    fontSize: 20,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    padding: 15,
  },
  modalheadrigt: {
    fontSize: 15,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    padding: 15,
    borderBottomWidth: 0,
    borderStyle: 'solid',
    borderColor: Color.bordergrey1,
    color: Color.orangeColor,
  },
  draweredit: {
    fontSize: 15,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    padding: 15,
    color: Color.orangeColor,
  },
  modalhead1: {
    fontSize: 20,
    textTransform: 'capitalize',
  },
  modalhead2: {
    fontSize: 16,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  modaltext: {
    fontSize: 16,
  },
  accountmodallist: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Color.whiteColor,
    padding: 15,
    position: 'relative',
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderColor: Color.bordergrey1,
  },
  clickarrow: {
    position: 'absolute',
    right: 15,
    top: 22,
  },
  modalBtnHolder: {
    display: 'flex',
    flexDirection: 'row',
  },
  modalBtn: {
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Color.greyColor,
    backgroundColor: Color.whiteColor,
    textAlign: 'center',
    alignItems: 'center',
  },
  modalBtn1: {
    backgroundColor: Color.orangeColor,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Color.orangeColor,
  },
  modalflex1: {
    flexDirection: 'row',
    display: 'flex',
  },
});