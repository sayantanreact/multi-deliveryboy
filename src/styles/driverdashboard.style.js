import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    }, 
    dashboardTop:{
        height: 190,
        backgroundColor: Color.primaryColor,
    },
    profileico:{ 
        width: 88,
        height: 88,
        borderColor: Color.whiteColor,
        borderWidth: 1,
        borderRadius: 44,
        alignSelf: 'center',
        marginTop: 10,
        alignItems: 'center'
    },
    dashTopText:{
        textAlign: 'center', 
        fontSize: 18,
        color: Color.whiteColor
    }, 
    dashTopText1:{
        textAlign: 'center', 
        fontSize: 12,
        color: Color.whiteColor
    },  
    activebox:{
        flexDirection: 'row',
        position:'relative',
        height: 40,
        backgroundColor: 'white',
        marginTop: -20,
        alignSelf: 'center',
        justifyContent:'space-between',
        borderRadius: 5,
        marginBottom:10,
    },
    activetext:{ 
        padding: 8.5,
        marginLeft: 5,
        fontSize: 16
    },  
    dashboardcontentholder:{
        padding:15,
        backgroundColor:Color.whiteColor,
        borderRadius:8
    },
    contentdiv:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        paddingTop:10,
        paddingBottom:10
    },
    contentdiv1:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between', 
        paddingBottom:15
    },
    flex1:{
        display:'flex',
        flexDirection:'row', 
    },
    contentleft:{

    },
    contentRight:{

    },
    signSubmit:{
        backgroundColor: Color.orangeColor,
        color: Color.whiteColor,
        textAlign:'center',
        fontSize:26,
        fontWeight:'bold',
        borderRadius:10,
        padding:15,
        marginBottom:15, 
    },
    signSubmit1:{
        // backgroundColor: Color.greenColor,
        color: Color.whiteColor,
        textAlign:'center',
        fontSize:26,
        fontWeight:'bold',
        borderRadius:10,
        padding:15,
        marginBottom:15, 
    },
    dashhead1:{
        marginTop:5,
        fontSize:16,
        lineHeight:20,
        color: Color.blackColor,
    },
    dashhead2:{
        fontSize:14,
        lineHeight:20,
        color: Color.blackColor,
    },
    dashtext:{
        fontSize:12,
        lineHeight:20,
        color: Color.greyColor,
    },
    dashtext1:{
        fontSize:14,
        lineHeight:20,
        color: Color.greyColor,
    },
    BtnHolder:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        borderTopWidth:1,
        borderColor:Color.grayDash,
        paddingTop:15,
    },
    Btn:{
        paddingBottom:10,
        paddingTop:10,
        paddingLeft:20,
        paddingRight:20, 
        borderRadius:25,
        textAlign:'center',
        alignItems:'center', 
        borderWidth:1,
        borderColor:Color.greenColor,
        backgroundColor:Color.greenLight,
        width:140
      },
      Btn1:{  
        borderWidth:1,
        borderColor:Color.redColor,  
        backgroundColor:Color.redLight,
        },
});