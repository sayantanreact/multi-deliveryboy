import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    }, 
    signSubmit:{
        backgroundColor: Color.orangeColor,
        color: Color.whiteColor,
        textAlign:'center',
        fontSize:26,
        fontWeight:'bold',
        borderRadius:10,
        padding:15,
        marginBottom:15, 
    },
    orderstatushead1:{
        fontSize:18,
        lineHeight:20,
        fontWeight:'bold',
        color: Color.lightBlack,
    },
    orderstatushead2:{
        fontSize:24,
        lineHeight:26,
        color: Color.orangeColor,
    },
    orderstatushead4:{
        fontSize:20,
        lineHeight:26,
        color: Color.orangeColor,
    },
    orderstatushead3:{
        fontSize:17,
        lineHeight:20,
        color: Color.greyColor,
    },
    ordertext:{
        fontSize:12,
        lineHeight:20,
        color: Color.greyColor1,
    },
    statusHolder:{
        display:'flex',
        flexDirection:'row',
        position:'relative',
        alignItems:'center'
    },
    orderlistRight:{
        position:'absolute',
        right:0,
        top:12
    },
    ratebtn:{
        borderColor:Color.orangeColor,
        color:Color.orangeColor,
        paddingLeft:12,
        paddingRight:12,
        paddingTop:6,
        paddingBottom:6,
        borderRadius:10,
        borderWidth:1, 
    },
});