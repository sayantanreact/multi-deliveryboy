import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    }, 
    signSubmit:{
        backgroundColor: Color.orangeColor,
        color: Color.whiteColor,
        textAlign:'center',
        fontSize:26,
        fontWeight:'bold',
        borderRadius:10,
        padding:15,
        marginBottom:15, 
    },
    orderhead1:{
        fontSize:24,
        lineHeight:26,
        color: Color.lightBlack,
    },
    ordertext:{
        fontSize:12,
        lineHeight:20,
        color: Color.greyColor1,
    },
});