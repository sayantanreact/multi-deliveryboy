import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    },   
    productItem:{
        display:'flex',
        flexDirection:'row',
        marginBottom:15,
    },
    productImg:{
        width:"50%",
    },
    productContent:{
        backgroundColor: Color.whiteColor,
        padding:15,
        width:"50%",
        position:'relative',
        borderTopRightRadius:5,
        borderBottomRightRadius:5,
    },
    watchlist:{
        position:'absolute',
        right:10,
        top:10
    },
    productHeading:{
        fontSize:16,
        color: Color.blackColor,
        textTransform:'capitalize',
        fontWeight:'bold',
    },
    productText:{
        color: Color.greyColor,
        fontSize:13,
    },


});