import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const recipeNumColums = 4;
const catererNumColums = 1.4;
// item size
const RECIPE_ITEM_HEIGHT = 80;
const RECIPE_ITEM_MARGIN = 10; 

export default StyleSheet.create({
    CategoryList:{
        marginBottom: 15,
    },
    Category: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: RECIPE_ITEM_MARGIN,
        marginTop: 20,
        width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums,
        height: RECIPE_ITEM_HEIGHT + 30,
    },
    photo: {
        width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums,
        height: RECIPE_ITEM_HEIGHT,
        borderRadius: 6,
    },
    title: {
        flex: 1,
        fontSize: 14,
        fontWeight: 'bold',
        textAlign: 'center',
        color: ThemeColors.darkgreyColor,
        marginTop: 5,
    },
    Container:{
        paddingLeft: 10,
        paddingRight: 10,
    },
    Location:{
        borderColor: ThemeColors.primaryColor,
        borderWidth: 1,
        borderRadius: 6,
        height: 40,
        flexDirection: 'row',
        padding: 8,
        marginTop: 15,
        marginBottom: 15,
    },
    Welcome:{
        fontSize: 16,
    },
    SearchBarWrap:{
        flexDirection: 'row',
        backgroundColor: '#F4F4F5',
        borderColor: '#E1E2EB',
        borderWidth: 1,
        borderRadius: 6,
        marginTop: 15,
        marginBottom: 5,
    },
    SearchBar:{
        // height: 40,
        width: '90%',
        paddingRight: 10,
    },
    Heading:{
        fontSize: 16,
        fontWeight: 'bold',
        color: ThemeColors.blackColor,
    },

    CatererBox:{
        flexDirection: 'row', 
        flex: 1,
        marginLeft: RECIPE_ITEM_MARGIN,
        marginTop: 20,
        width: (SCREEN_WIDTH - (catererNumColums + 1) * RECIPE_ITEM_MARGIN) / catererNumColums,
    },
    CatererLeft:{
        width: 86,
        height: 104,
    },
    CatererRight:{
        paddingLeft: 10,
    },
    Catererphoto:{
        width: '100%',
        height: '100%',
        borderRadius: 6,
    },
    Caterertitle:{
        fontSize: 16,
        fontWeight: 'bold',
        color: ThemeColors.darkgreyColor,
        marginTop: 0,
    },
    CatererTag:{
        fontSize: 12,
        color: ThemeColors.darkgreyColor,
    },
    Rating:{
      flexDirection: 'row',  
      marginTop: 5,
    },
    OverallRating:{
        color: ThemeColors.primaryColor,
        fontSize: 12,
        marginLeft: 4,
    },
    TotalReview:{
        fontSize: 12,
        color: ThemeColors.darkgreyColor,
        fontWeight: 'bold',
        marginLeft: 4,
    },
    MinOrder:{
        flexDirection: 'row', 
        borderTopColor: ThemeColors.lightgreyColor,
        borderTopWidth: 1,
        paddingTop: 2,
        marginTop: 2,
    },

    PopularList:{
        marginBottom: 15,
        paddingBottom: 10,
    },
    PopularBox:{
        flexDirection: 'row', 
        flex: 1,
        marginLeft: RECIPE_ITEM_MARGIN,
        marginTop: 5,
        marginBottom: 10,
        padding: 5,
        width: (SCREEN_WIDTH - (catererNumColums + 1) * RECIPE_ITEM_MARGIN) / catererNumColums,
        backgroundColor: ThemeColors.whiteColor,
        shadowColor: '#000000',
        shadowOpacity: 0.20,
        shadowRadius: 2,
        elevation: 2,
        shadowOffset: { height: 0, width: 0 },
        borderRadius: 6,
        borderWidth: 0.5,
        borderColor: ThemeColors.offwhiteColor,
    },
    PopularLeft:{
        width: 75,
        height: 75,
    },
    PopularRight:{
        paddingLeft: 10,
    },
    Popularphoto:{
        width: '100%',
        height: '100%',
        borderRadius: 6,
    },
    Populartitle:{
        fontSize: 16,
        fontWeight: 'bold',
        color: ThemeColors.darkgreyColor,
        marginTop: 0,
    },
    //Modal
    BackDrop: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        height: '100%',
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    ModalCross:{
        padding: 15,
    },
    modalView: {
        backgroundColor: ThemeColors.whiteColor,
        width: ScreenWidth-30,
        height: ScreenHeight / 2.4,
        borderRadius: 5,
    },
    ModalContainer:{
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
        paddingBottom: 5,
    },
    modalTitle:{
        borderBottomWidth: 1,
        borderBottomColor: ThemeColors.lightgreyColor,
        marginBottom: 5,
        paddingLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    modalTitleQr:{
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    modalTitleText:{
        fontSize: 16,
        fontWeight: 'bold',
        color: ThemeColors.secondaryColor,
    },
    ModalHeading:{
        fontSize: 20,
        color: ThemeColors.blackColor,
        marginBottom: 10,
        fontWeight: "bold",
        marginTop: 20,
    },
    TextMuted:{
        color: ThemeColors.darkgreyColor,
        fontSize: 14,
        marginTop: 10,
    },
    SelectBox:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: ThemeColors.primaryColor,
        borderWidth: 1,
        borderRadius: 4,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        height: 38,
        marginTop: 5,
        marginBottom: 5,
    },

});