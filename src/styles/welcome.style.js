import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
  Wrapper: {
    // flex: 1,
    //backgroundColor: ThemeColors.secondaryColor,
    width: ScreenWidth,
    minHeight: ScreenHeight,
  },
  backgroundImage: {
    width: ScreenWidth,
    height: ScreenHeight,
  },
  Container: {
    // alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    // position: 'absolute',
    // top: 0,
    // bottom: 0,
    // left: 0,
    // right: 0,
    // paddingTop: 150,
  },
  HiText: {
    fontSize: 18,
    color: ThemeColors.whiteColor,
  },
  WelcomeText: {
    fontSize: 18,
    color: ThemeColors.whiteColor,
    fontWeight: 'bold',
  },
  Logo: {
    marginTop: 80,
  },
  LestStart: {
    marginTop: 100,
    width: '100%',
    paddingLeft: 30,
    paddingRight: 30,
  },
  BigButton: {
    backgroundColor: ThemeColors.primaryColor,
    borderRadius: 4,
    height: 60,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  BigButtonText: {
    color: ThemeColors.whiteColor,
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center',
  },
});