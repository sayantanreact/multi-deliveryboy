import { StyleSheet, PixelRatio, Dimensions } from "react-native";
import { ThemeColors } from "./main.style";
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  fixToText: {
    flexDirection: "row",
    justifyContent: "center",
  },
  textBlack: {
    fontSize: 18,
    color: ThemeColors.blackColor,
  },
  PersonList: {
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: ThemeColors.primaryColor,
    shadowColor: ThemeColors.blackColor,
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    shadowOffset: { height: 2, width: 0 },
    padding: 15,
    borderRadius: 6,
    margin: 5,
    marginTop: 10,
  },
  Row: {
    flexDirection: "row",
  },
  PersonDetailsName: {
    fontSize: 18,
    color: ThemeColors.whiteColor,
    marginRight: 15,
    marginLeft: 4,
  },
  PersonDetailsName: {
    fontSize: 18,
    color: ThemeColors.blackColor,
    marginRight: 15,
    marginLeft: 4,
  },
  CloseButton: {
    alignSelf: "flex-start",
    position: "absolute",
    right: 15,
    top: 40,
    zIndex: 1,
    justifyContent: "center",
    alignContent: "center",
    textAlign: "center",
    //backgroundColor: 'red',
    width: 30,
    height: 30,
  },
  modalViewDial: {
    margin: 0,
    backgroundColor: ThemeColors.whiteColor,
    borderRadius: 6,
    padding: 15,
    paddingTop: 40,
    width: "100%",
    height: ScreenHeight,
  },
});