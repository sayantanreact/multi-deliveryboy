import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    },
    pastorderholder: {
        borderBottomWidth: 2,
        borderColor: Color.bordergrey1,
    },
    orderproduct: {
        display: 'flex',
        flexDirection: 'row',
        position: 'relative',
    },
    orderheading1: {
        fontSize: 16,
        lineHeight: 20,
        fontWeight: 'bold',
        color: Color.blackColor,
    },
    orderheading2: {
        fontSize: 12,
        lineHeight: 20,
        color: Color.blackColor,
    },
    ordertext: {
        fontSize: 12,
        lineHeight: 20,
        color: Color.greyColor,
    },
    ordercontent: {
        width: "50%",
    },
    orderprice: {
        position:'absolute',
        top:'50%',
        right:6,
    },
    dldetails: {

    },
    BtnHolder: {
        display: 'flex',
        flexDirection: 'row',
    },
    Btn: {
        paddingBottom: 10,
        paddingTop: 10,
        paddingLeft: 15,
        paddingRight: 15,
        textAlign: 'center',
        alignItems: 'center',
        backgroundColor: Color.successColor,

    },
    Btn1: {
        backgroundColor: Color.rejectColor,
    },
    flex1: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    fullline: {
        width: 100,
        height: 1,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: Color.greenColor
    },
    movableDot: {
        width: 15,
        height: 15,
        borderRadius: 8,
        position: 'absolute',
        backgroundColor: Color.greenColor,
        top: -8,
    }


});