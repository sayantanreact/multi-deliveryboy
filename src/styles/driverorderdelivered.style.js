import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import { Color, Fonts } from '../utils';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({ 
    backgroundImage: {
        width: ScreenWidth,
        height: ScreenHeight,
    }, 
    signSubmit:{
        backgroundColor: Color.orangeColor,
        color: Color.whiteColor,
        textAlign:'center',
        fontSize:26,
        fontWeight:'bold',
        borderRadius:10,
        padding:15,
        marginBottom:15, 
    },
    orderhead1:{
        fontSize:24,
        lineHeight:26,
        color: Color.lightBlack,
        fontWeight:'bold'
    },
    orderhead12:{
        fontSize:20,
        lineHeight:22,
        color: Color.lightBlack,
        fontWeight:'600'
    },
    ordertext:{
        fontSize:12,
        lineHeight:20,
        color: Color.greyColor1,
    },
    ordertext2:{
        fontSize:16,
        lineHeight:18,
        color:"#7C7C7C",
        fontWeight:'400',
        fontFamily:Fonts.semiBold

    },
});