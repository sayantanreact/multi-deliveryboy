// Imports: Dependencies
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import reducers from './reducers';

const authConfig = {
  key: 'root',
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers from combine list)
  whitelist: [
    'carts'
  ],
  // Blacklist (Don't Save Specific Reducers)
  blacklist: [
    'counterReducer',
  ],
};

const reducer = persistReducer(authConfig, reducers);
const middleware = [ReduxThunk];

const store = createStore(reducer, compose(applyMiddleware(...middleware)));
const persistor = persistStore(store);

// Exports
export {
  store,
  persistor
};