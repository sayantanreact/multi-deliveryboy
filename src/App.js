/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from "react";
import { View, StatusBar, Platform } from "react-native";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
// Imports: Redux Persist Persister
import { store, persistor } from "./store";
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
import Router from "./Router";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ThemeColors } from "./styles/main.style";
import { EventRegister } from "react-native-event-listeners";
import { Apis } from './utils/Apis';
import GlobalFont from 'react-native-global-font'
import { Color, Images, Dimen, Fonts } from './utils';
import messaging from "@react-native-firebase/messaging";
// import Fire from './utils/Fire';
// import { withInAppNotification } from 'react-native-in-app-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios'
import PushNotification from 'react-native-push-notification';
import Fire from './utils/Fire';

class App extends Component {
  constructor() {
    super();
    // Fire.shared.init()
    this.state = {
      isUserLoggedin: false //undefined,
    };
    global.loginUserData = "";
    global.loginUserTokenType = "";
    global.loginUserAccessToken = "";
    global.userLat = "" //"41.78677589999999";
    global.userLong = "" //"-87.7521884";
  }

  componentDidMount() {
    let fontName = Fonts.regular
    GlobalFont.applyGlobal(fontName)
    if (Platform.OS == "ios") {
      Fire.shared.init()
    }
    this.requestPush();
    // this.listener1 = EventRegister.addEventListener('updateTokenToServer', () => {
    //   this.storePushRecordUser()
    // })

    if (Platform.OS == 'ios') {
      PushNotificationIOS.addEventListener('notification', (notification) => {
        this.onRemoteNotification(notification)
      });
      PushNotificationIOS.addEventListener(
        'localNotification',
        (notification) => {
          this.onLocalNotification(notification)
        },
      );
    } else {
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log('TOKEN:', token);
        },
        // (required) Called when a remote or local notification is opened or received
        onNotification: function (notification) {
          console.log('REMOTE NOTIFICATION ==>', notification);
          // process the notification here
        },
        // Android only: GCM or FCM Sender ID
        senderID: '314067776512', //'536589353914',
        popInitialNotification: true,
        requestPermissions: true,
      });
    }
  }

  /**
   * 
   * Push Notification 
   */

   onRemoteNotification = (notification) => {
    console.log("onRemoteNotification:: ", notification)
    //this.redirectFromNotiTap(notification._data)
    // const isClicked = notification.getData().userInteraction === 1;

    // const result = `
    //   Title:  ${notification.getTitle()};\n
    //   Subtitle:  ${notification.getSubtitle()};\n
    //   Message: ${notification.getMessage()};\n
    //   badge: ${notification.getBadgeCount()};\n
    //   sound: ${notification.getSound()};\n
    //   category: ${notification.getCategory()};\n
    //   content-available: ${notification.getContentAvailable()};\n
    //   Notification is clicked: ${String(isClicked)}.`;

    // if (notification.getTitle() == undefined) {
    //   Alert.alert('Silent push notification Received', result, [
    //     {
    //       text: 'Send local push',
    //       onPress: sendLocalNotification,
    //     },
    //   ]);
    // } else {
    //   Alert.alert('Push Notification Received', result, [
    //     {
    //       text: 'Dismiss',
    //       onPress: null,
    //     },
    //   ]);
    // }
  };

  onLocalNotification = (notification) => {
    console.log("onLocalNotification:: ", notification)
    //this.redirectFromNotiTap(notification._data)
    // const isClicked = notification.getData().userInteraction === 1;

    // Alert.alert(
    //   'Local Notification Received',
    //   `Alert title:  ${notification.getTitle()},
    //   Alert subtitle:  ${notification.getSubtitle()},
    //   Alert message:  ${notification.getMessage()},
    //   Badge: ${notification.getBadgeCount()},
    //   Sound: ${notification.getSound()},
    //   Thread Id:  ${notification.getThreadID()},
    //   Action Id:  ${notification.getActionIdentifier()},
    //   User Text:  ${notification.getUserText()},
    //   Notification is clicked: ${String(isClicked)}.`,
    //   [
    //     {
    //       text: 'Dismiss',
    //       onPress: null,
    //     },
    //   ],
    // );
  };

  requestPush() {
    console.log("requestPushrequestPushrequestPushrequestPush");
    this.checkPermission();
    this.createNotificationListeners();
    // this.createMessagingNotificationListeners();
  }

  //1
  async checkPermission() {
    const enabled = await messaging().hasPermission();
    console.log("enabledenabledenabled", enabled);

    if (enabled != -1) {
      console.log("getToken");
      this.getToken();
    } else {
      console.log("requestPermission");
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem("fcmToken");
    console.log("fcmTokenfcmToken:: ", fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      console.log("fcmTokenfcmToken if:: ", fcmToken);
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem("fcmToken", fcmToken);
        // EventRegister.emit("updateTokenToServer", {});
      }
    } else {
      fcmToken = await messaging().getToken();
      console.log("fcmTokenfcmToken else:: ", fcmToken);
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem("fcmToken", fcmToken);
        // EventRegister.emit("updateTokenToServer", {});
      }
    }
  }

  //2
  async requestPermission() {
    try {
      await messaging().registerDeviceForRemoteMessages();
      await messaging().requestPermission();
      // User has authorized
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log("permission rejected");
    }
  }

  async createNotificationListeners() {
    console.log("noti receive noti receive noti receive noti receive");
    // When a user tap on a push notification and the app is in background
    this.backgroundNotificationListener = messaging().onNotificationOpenedApp(
      async (remoteMessage) => {
        console.log("onNotificationOpenedApp background");
        console.log('remoteMessageremoteMessage background:: ', remoteMessage);
        console.log('remoteMessageremoteMessage background data:: ', remoteMessage?.data);
        //alert("Background Push Notification opened");
        //Actions.orderStatusScreen()
      }
    );

    // When a user tap on a push notification and the app is CLOSED
    this.closedAppNotificationListener = messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        console.log("getInitialNotification");
        if (remoteMessage) {
          //alert("App Closed Push Notification opened");
          //Actions.orderStatusScreen()
        }
      });

    // When a user receives a push notification and the app is in foreground
    this.onMessageListener = messaging().onMessage((message) => {
      console.log("foregroundmessage:: ", message);
      // Alert.alert(message.data.title, message.data.body);
      // alert('Foreground Push Notification opened');
      EventRegister.emit('reloadDash', 'it works!!!')
      showMessage({
        message: message.notification.title, //"My message title",
        description: message.notification.body,
        type: "success",
        //color: '#000000',
        backgroundColor: Color.primaryColor, //'#006400', //'#228B22',
        duration: 8000,
        onPress: () => {
          console.log("message foreground", message);
          console.log("message data foreground", message?.data);
          //Actions.orderStatusScreen()
        }
      });


    });
  }


  /**
   * 
   * Push Notification
   */

  /**
   * render() this the main function which used to display different view.
   */

  render() {
    // const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    // const authConfig = {
    //   key: 'root',
    //   storage: AsyncStorage,
    //   whitelist: ['carts'],
    // };

    // const reducer = persistReducer(authConfig, reducers);
    // const middleware = [ReduxThunk];

    // const store = createStore(reducer, compose(applyMiddleware(...middleware)));
    // const persistor = persistStore(store);

    if (this.state.isUserLoggedin != undefined) {
      return (
        <Provider store={store}>
          <PersistGate
            //loading={null}
            persistor={persistor}
          >
            <>
              {/* <SafeAreaView style={{
              flex: 0,
              backgroundColor: "#600f0c"
            }} /> */}
              {/* <SafeAreaView style={{
              flex: 1,
              backgroundColor: "#600f0c"
            }} 
            //forceInset={{'top': 'never'}}
            > */}
              {Platform.OS == 'ios' ? (
                <StatusBar translucent barStyle={'light-content'}></StatusBar>
              ) : (
                <View
                  style={{
                    height: StatusBar.currentHeight,
                    backgroundColor: ThemeColors.primaryColor,
                  }}
                >
                  <StatusBar
                    backgroundColor={ThemeColors.primaryColor}
                    translucent
                    barStyle={'light-content'}
                  ></StatusBar>
                </View>
              )}

              <Router
                authenticated={this.state.isUserLoggedin}
                //loginUserData={global.loginUserData}
              />
              <FlashMessage
                style={{
                  marginTop: Platform.OS == 'ios' ? 0 : StatusBar.currentHeight,
                }}
                position="top"
                animated={true}
              />

              {/* </SafeAreaView> */}
            </>
          </PersistGate>
        </Provider>
      );
    }
    return <View />;
  }
}

export default App;
