import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator, ImageBackground,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import loginpage from '../styles/login.style';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
// import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';
import { Images } from '../utils'; 
import allrestaurantsstyles from '../styles/allrestaurants.style';

// import { FlushMsg } from '../utils'

const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
    },
  ];

class AllRestaurantScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {

    }
    

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}>
                        <TouchableOpacity
                            style={{}}
                            activeOpacity={0.5}
                        >
                            <Image
                                source={Images.leftArrow}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.HeaderMiddle}>
                        <Text style={Styles.Heading}>Restaurants</Text>
                    </View>
                    <View style={styles.HeaderRight}>
                    <TouchableOpacity
                        style={{}}
                        activeOpacity={0.5}  
                    > 
                    <Image  source={Images.filter} />
                            </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    {this.renderHeaderShop()}
                                     
                                    <View style={styles.commonPadding}>
                                        <View style={allrestaurantsstyles.productItem}>
                                            <View style={allrestaurantsstyles.productImg}>
                                                <TouchableOpacity style={{}} activeOpacity={0.5}   > 
                                                    <Image  source={Images.firewater} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={allrestaurantsstyles.productContent}>
                                                <View style={allrestaurantsstyles.watchlist}>
                                                    <TouchableOpacity style={{}} activeOpacity={0.5}   > 
                                                        <Image  source={Images.heart} />
                                                    </TouchableOpacity>	
                                                </View>
                                                <View  style={Styles.mb_5}><Text  style={allrestaurantsstyles.productHeading}>Fire Water</Text></View>
                                                <View style={Styles.mb_10}><Text style={allrestaurantsstyles.productText}>Chinese, American</Text></View>
                                                <View style={Styles.mb_20}><Text>rating</Text></View>
                                                <View><Text style={allrestaurantsstyles.productText}>4.8 (3,200 reviews)</Text></View>
                                            </View> 
                                        </View>
                                        
                                        <View style={allrestaurantsstyles.productItem}>
                                            <View style={allrestaurantsstyles.productImg}>
                                                <TouchableOpacity style={{}} activeOpacity={0.5}   > 
                                                    <Image  source={Images.lil} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={allrestaurantsstyles.productContent}>
                                                <View style={allrestaurantsstyles.watchlist}>
                                                    <TouchableOpacity style={{}} activeOpacity={0.5}   > 
                                                        <Image  source={Images.heart} />
                                                    </TouchableOpacity>	
                                                </View>
                                                <View  style={Styles.mb_5}><Text  style={allrestaurantsstyles.productHeading}>Lil Johnny’s</Text></View>
                                                <View style={Styles.mb_10}><Text style={allrestaurantsstyles.productText}>Chinese, American</Text></View>
                                                <View style={Styles.mb_20}><Text>rating</Text></View>
                                                <View><Text style={allrestaurantsstyles.productText}>4.8 (3,200 reviews)</Text></View>
                                            </View> 
                                        </View>
                                    
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default AllRestaurantScreen;
