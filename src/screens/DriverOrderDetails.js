import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator,CheckBox,ImageBackground,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import { Modal } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style'; 
import driverorderdetailsstyle from '../styles/driverorderdetails.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles'; 

// import { FlushMsg } from '../utils'

class DriverOrderDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible:true
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}> 
                        <TouchableOpacity
                        style={{}}
                        activeOpacity={0.5} 
                         
                        onPress={() => {
                            Actions.pop();
                        }}
                    >
                            <Image
                                source={Images.leftArrow}
                            />
                            </TouchableOpacity>
                    </View>
                    <View style={styles.HeaderMiddle}> 
                           <Text style={Styles.Heading}>Order Details</Text>
                    </View>
                    <View style={styles.HeaderRight}> 
                           <Text>&nbsp; </Text>
                    </View>
                </View> 
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    {this.renderHeaderShop()}
                                    <View> 
                                        <View style={[driverorderdetailsstyle.notificationlist,styles.mb_15,styles.commonPadding,styles.brd5,{
                                            backgroundColor:Color.whiteColor,marginBottom:20 
                                        }]}>
                                            <View style={driverorderdetailsstyle.ntflex1}>
                                                <View>
                                                    <View><Text style={[driverorderdetailsstyle.nthead1,{marginBottom:3}]}>Order # 15632</Text></View>
                                                    <View style={driverorderdetailsstyle.ntflex2}><Text style={driverorderdetailsstyle.nttext}>Payment :</Text><Text style={[driverorderdetailsstyle.nttext,{color:Color.greenColor}]}> Net Banking</Text></View>
                                                </View>
                                                <View> 
                                                    <View><Text style={[driverorderdetailsstyle.nttext,{marginBottom:5,textAlign:'right'}]}>20 July 2021  |  9.30 pm</Text></View>
                                                    <View><Text style={[driverorderdetailsstyle.nttext,{textAlign:'right',color:Color.greenColor}]}>Delivered</Text></View>
                                                </View>
                                            </View> 
                                        </View>   
                                        <View style={[driverorderdetailsstyle.notificationlist,styles.mb_15,styles.commonPadding,styles.brd5,{
                                            backgroundColor:Color.whiteColor,marginBottom:20   
                                        }]}>
                                            <View style={[driverorderdetailsstyle.ntflex1,styles.mb_5]}> 
                                                <View><Text style={driverorderdetailsstyle.nthead1}>Pickup Address</Text></View> 
                                            </View>
                                            <View>
                                                <View style={{width:"70%"}}>
                                                    <View><Text style={[driverorderdetailsstyle.nthead2,{marginBottom:5}]}>Skyz Reastaurent</Text></View>
                                                    <View><Text style={driverorderdetailsstyle.nttext}>42 TRIANGLE WEST, BRISTOL, UK BS8 1ES</Text></View>
                                                </View> 
                                            </View>
                                        </View>  
                                        <View style={[driverorderdetailsstyle.notificationlist,styles.mb_15,styles.commonPadding,styles.brd5,{
                                            backgroundColor:Color.whiteColor,marginBottom:20 
                                        }]}>
                                            <View style={[driverorderdetailsstyle.ntflex1,styles.mb_5]}> 
                                                <View><Text style={driverorderdetailsstyle.nthead1}>Delivery Address</Text></View> 
                                            </View>
                                            <View>
                                                <View style={{width:"70%"}}>
                                                    <View><Text style={[driverorderdetailsstyle.nthead2,{marginBottom:5}]}>DANIEL REX</Text></View>
                                                    <View><Text style={driverorderdetailsstyle.nttext}>Delivery Address : THE BUSINESS CENTRE, 61 WELLFIELD ROAD, UK CF24 3DG</Text></View>
                                                </View> 
                                            </View>
                                        </View> 
                                        <View style={[driverorderdetailsstyle.notificationlist,styles.mb_15,styles.commonPadding,styles.brd5,{
                                            backgroundColor:Color.whiteColor ,marginBottom:20 
                                        }]}>
                                            <View><Text style={driverorderdetailsstyle.nttext}>2 Items</Text></View>
                                            <View style={[driverorderdetailsstyle.ntflex1, styles.commonTBpadding,driverorderdetailsstyle.ntborder]}>
                                                <View>
                                                    <View><Text style={[driverorderdetailsstyle.nthead2,{marginBottom:5}]}>Cheese Butter Masala Combo</Text></View>
                                                    <View><Text style={driverorderdetailsstyle.nttext}>Qty: 1</Text></View>
                                                </View>
                                                <View> 
                                                    <View><Text style={[driverorderdetailsstyle.nttext,{marginBottom:5,textAlign:'right'}]}>£60</Text></View> 
                                                </View>
                                            </View>
                                            <View style={[driverorderdetailsstyle.ntflex1, styles.commonTBpadding,driverorderdetailsstyle.ntborder]}>
                                                <View>
                                                    <View><Text style={[driverorderdetailsstyle.nthead2,{marginBottom:5}]}>Manchurian</Text></View>
                                                    <View><Text style={driverorderdetailsstyle.nttext}>Qty: 1</Text></View>
                                                </View>
                                                <View> 
                                                    <View><Text style={[driverorderdetailsstyle.nttext,{marginBottom:5,textAlign:'right'}]}>£60</Text></View> 
                                                </View>
                                            </View> 
                                            <View><Text style={[driverorderdetailsstyle.nthead1,{textAlign:'right',paddingTop:15}]}>Total :  £120</Text></View>
                                        </View> 
                                    </View>
                                </View>
                            </View>

                             
                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default DriverOrderDetailsScreen;
