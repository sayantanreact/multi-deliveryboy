import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import styles from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import otpstyles from '../styles/otp.style';
import { Actions } from 'react-native-router-flux';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { MainContainer } from '../components';
import { Images, Color } from '../utils';
import Styles from '../utils/CommonStyles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LoginService } from '../services';
import OTPInputView from '@twotalltotems/react-native-otp-input';
// import { FlushMsg } from '../utils'

class OtpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpFiled1: '',
      otpFiled2: '',
      otpFiled3: '',
      otpFiled4: '',
      loading: false,
      code: '',
    };
  }
  componentDidMount() {
    console.log('hello', this.props.isPage);
  }

  subMitOtp = () => {
    if (this.state.code != '') {
      this.verifyRegisterOtpApi();
    } else {
      FlushMsg.showError('Please enter your OTP');
    }
  };
  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };
  verifyRegisterOtpApi = () => {
    this.loaderShowHide(true);
    var myData = {
      otp: this.state.code, //this.state.otpFiled1 + this.state.otpFiled2 + this.state.otpFiled3 + this.state.otpFiled4,
      otp_id: this.props.data.otp_id,
      isPage: this.props.isPage,
    };
    LoginService.verifyOtpDriver(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          //FlushMsg.showSuccess(result.message);
          if (this.props.isPage == 'editProfileDriver') {
            this.props.storeUserData(result.data);
            Actions.pop();
          } else if (this.props.isPage == 'signup') {
            global.loginUserData = JSON.stringify(result.data);
            Actions.reset('driverprofile', {
              isPage: 'detail',
            });
          } else {
            global.loginUserData = JSON.stringify(result.data);
            // AsyncStorage.setItem('@multiProfileHeader', 'detail');
            AsyncStorage.setItem('@multiDRIVERData', global.loginUserData);
            AsyncStorage.getItem('@multiDRIVERData')
              .then((value) => {
                // //console.log('value:: ', value);
                //console.log('loginUserToken:: ', global.loginUserToken);
                global.loginUserData = value;
                Actions.reset('drawer');

                // Actions.reset('driverprofile', {
                //   isPage: 'detail',
                // });
              })
              .catch((error) => {
                //console.log(error);
                //alert('ERROR GETTING DATA FROM FACEBOOK')
              });
          }
        } else {
          FlushMsg.showError(result.message);
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };
  resendOtpApi = () => {
    this.loaderShowHide(true);
    var myData = {
      mobile: this.props.data.mobile,
      otp_id: this.props.data.otp_id,
    };
    LoginService.resendOtpDriver(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          FlushMsg.showSuccess(result.message);
          this.otpFiled1.focus();
        } else {
          FlushMsg.showError(result.message);
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };

  renderHeaderShop = () => {
    return (
      <View>
        <View style={styles.Header}>
          <View style={styles.HeaderLeft}>
            <TouchableOpacity
              style={{}}
              activeOpacity={0.5}
              onPress={() => {
                Actions.pop();
              }}
            >
              <Image source={Images.leftArrow} />
            </TouchableOpacity>
          </View>
          <View style={styles.HeaderMiddle}>
            <Text style={Styles.Heading}>&nbsp;</Text>
          </View>
          <View style={styles.HeaderRight}>
            <Text>&nbsp; </Text>
          </View>
        </View>
      </View>
    );
  };
  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <MainContainer>
          <Loader loading={this.state.loading} />
          <View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={pagestyles.Wrapper}>
                {this.renderHeaderShop()}
                <View style={pagestyles.Container}>
                  <View
                    style={[
                      Styles.mb_70,
                      Styles.commonLRpadding,
                      Styles.TextCenter,
                    ]}
                  >
                    <Text style={[Styles.Heading, Styles.mb_10]}>
                      Phone Verification
                    </Text>
                    <Text style={Styles.subText}>Enter your OTP code here</Text>
                  </View>
                  <View style={[Styles.mb_70, otpstyles.otpHolder]}>
                    <OTPInputView
                      style={{ width: '100%', height: 100 }}
                      pinCount={4}
                      code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                      onCodeChanged={(code) => {
                        this.setState({ code: code });
                      }}
                      autoFocusOnLoad
                      codeInputFieldStyle={[
                        otpstyles.otpInput,
                        {
                          backgroundColor: Color.grayDash,
                        },
                      ]}
                      codeInputHighlightStyle={{
                        backgroundColor: Color.primaryColor,
                      }}
                      // filledCodeInputFieldStyle={{ backgroundColor: Color.primaryColor }}
                      editable={true}
                      onCodeFilled={(code) => {
                        console.log(`Code is ${code}, you are good to go!`);
                        this.setState(
                          {
                            code: code,
                          },
                          () => {
                            this.subMitOtp(code);
                          },
                        );
                      }}
                    />

                    {/* <TextInput
                      style={[
                        otpstyles.otpInput,
                        {
                          backgroundColor:
                            this.state.otpFiled1 != ''
                              ? Color.primaryColor
                              : Color.grayDash,
                        },
                      ]}
                      name={1}
                      maxLength={1}
                      autoFocus={true}
                      keyboardType="number-pad"
                      ref={(input) => {
                        this.otpFiled1 = input;
                      }}
                      onChangeText={(text) => {
                        this.setState(
                          {
                            otpFiled1: text,
                          },
                          () => {
                            if (this.state.otpFiled1 != '') {
                              this.otpFiled2.focus();
                            }
                          },
                        );
                      }}
                      value={this.state.otpFiled1}
                      onSubmitEditing={() => {
                        this.subMitOtp();
                      }}
                    />
                    <TextInput
                      style={[
                        otpstyles.otpInput,
                        {
                          backgroundColor:
                            this.state.otpFiled2 != ''
                              ? Color.primaryColor
                              : Color.grayDash,
                        },
                      ]}
                      name={1}
                      maxLength={1}
                      keyboardType="number-pad"
                      ref={(input) => {
                        this.otpFiled2 = input;
                      }}
                      onChangeText={(text) => {
                        this.setState(
                          {
                            otpFiled2: text,
                          },
                          () => {
                            if (this.state.otpFiled2 != '') {
                              this.otpFiled3.focus();
                            }
                          },
                        );
                      }}
                      value={this.state.otpFiled2}
                      onSubmitEditing={() => {
                        this.subMitOtp();
                      }}
                    />
                    <TextInput
                      style={[
                        otpstyles.otpInput,
                        {
                          backgroundColor:
                            this.state.otpFiled3 != ''
                              ? Color.primaryColor
                              : Color.grayDash,
                        },
                      ]}
                      name={1}
                      maxLength={1}
                      keyboardType="number-pad"
                      ref={(input) => {
                        this.otpFiled3 = input;
                      }}
                      onChangeText={(text) => {
                        this.setState(
                          {
                            otpFiled3: text,
                          },
                          () => {
                            if (this.state.otpFiled3 != '') {
                              this.otpFiled4.focus();
                            }
                          },
                        );
                      }}
                      value={this.state.otpFiled3}
                      onSubmitEditing={() => {
                        this.subMitOtp();
                      }}
                    />
                    <TextInput
                      style={[
                        otpstyles.otpInput,
                        {
                          backgroundColor:
                            this.state.otpFiled4 != ''
                              ? Color.primaryColor
                              : Color.grayDash,
                        },
                      ]}
                      name={1}
                      maxLength={1}
                      keyboardType="number-pad"
                      ref={(input) => {
                        this.otpFiled4 = input;
                      }}
                      onChangeText={(text) => {
                        this.setState(
                          {
                            otpFiled4: text,
                          },
                          () => {
                            if (this.state.otpFiled4 != '') {
                              this.subMitOtp();
                            }
                          },
                        );
                      }}
                      value={this.state.otpFiled4}
                      onSubmitEditing={() => {
                        this.subMitOtp();
                      }}
                    /> */}
                  </View>
                  <View
                    style={[
                      Styles.mb_70,
                      Styles.commonLRpadding,
                      Styles.TextCenter,
                    ]}
                  >
                    <Text style={Styles.subText}>
                      Didn’t you receive any code?
                    </Text>
                    <Text
                      onPress={() => {
                        this.resendOtpApi();
                      }}
                      style={[Styles.subText, { color: Color.orangeColor }]}
                    >
                      Resend a new code
                    </Text>
                  </View>
                  {/* <View style={{ marginTop: 20 }}>
                                        <Button
                                            text={'Submit'}
                                            onLoading={false}
                                            customStyles={{
                                                // mainContainer: styles.butonContainer
                                            }}
                                            onPress={() => {
                                                Actions.signupscreen();
                                            }}
                                        />
                                    </View> */}
                </View>
              </View>
            </ScrollView>
          </View>
        </MainContainer>
      </>
    );
  }
}

export default OtpScreen;
