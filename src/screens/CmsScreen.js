import React, { Component } from 'react';
import { View, SafeAreaView } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import FlushMsg from '../utils/FlushMsg';
import { WebService } from '../services';
import Loader from '../utils/Loader';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { WebView } from 'react-native-webview';
import { TopHeader } from '../components';
import { Actions } from 'react-native-router-flux';

// import { FlushMsg } from '../utils'

// const HTML = `<!DOCTYPE html>\n
// <html>
//   <head>
//     <title>Messaging</title>
//     <meta http-equiv="content-type" content="text/html; charset=utf-8">
//     <meta name="viewport" content="width=320, user-scalable=no">
//     <style type="text/css">
//       body {
//         margin: 0;
//         padding: 0;
//         font: 62.5% arial, sans-serif;
//         background: #ccc;
//       }
//     </style>
//   </head>
//   <body>
//     <button onclick="sendPostMessage()">Send post message from JS to WebView</button>
//     <p id="demo"></p>
//     <script>
//       function sendPostMessage() {
//         window.ReactNativeWebView.postMessage(JSON.stringify({success: true, message: 'Data received'}));
//       }
//       window.addEventListener('message',function(event){
//         console.log("Message received from RN: ",event.data)
//       },false);
//     </script>
//   </body>
// </html>`;

class CmsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      // countrySelector: false
    };
  }
  componentDidMount() {
    this.getWebPagesApi();
  }
  getWebPagesApi = () => {
    this.loaderShowHide(true);
    var myData = {
      key: this.props.title,
    };
    console.log('fulldata ::', myData);
    WebService.webPagesApi(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          this.setState({
            weblink: result.data,
          });
          console.log('result.message:: ', result.message);
          // FlushMsg.showSuccess(result.message);
        } else {
          FlushMsg.showError(result.message);
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };
  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };

  // onMessage = (e) => {
  //   let { data } = e.nativeEvent; // data you will receive from html
  //   console.log('data:: ', data);
  //   console.log('data:: ', JSON.parse(data).status);
  //   console.log('data:: ', JSON.parse(data).message);
  //   // Actions.tab_2();
  //   if (JSON.parse(data).status == true) {
  //     Actions.reset('orderconfirmscreen', {
  //       orderdetails: this.props.orderdetails,
  //     });
  //   } else {
  //     Actions.reset('customerpayment', {
  //       customerAddress: this.props.customerAddress,
  //       cartData: this.props.cartData,
  //       cartType: this.props.cartType,
  //       cartResorGroc: this.props.cartResorGroc,
  //       isPage: 'takepayments',
  //     });
  //   }
  // };

  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <SafeAreaView
          style={{
            flex: 0,
            backgroundColor: ThemeColors.orange,
          }}
        />
        <TopHeader
          title={this.props.heading}
          onPress={() => {
            Actions.pop();
          }}
        />
        <View
          style={{
            flex: 1,
          }}
        >
          <WebView
            ref={(webview) => {
              this.webview = webview;
            }}
            source={{
              uri: this.state.weblink, //"https://multistore.wordsystech.com/api/common/test" //"https://multistore.wordsystech.com/api/order/takepayments/15" //"https://multistore.wordsystech.com/api/common/test"
            }}
            //source={{ html: HTML }}
            //injectedJavaScript={this.injectjs()}
            javaScriptEnabled={true}
            automaticallyAdjustContentInsets={false}
            // onMessage={this.onMessage}
            //style={styles.webview}
          />
          {/* <WebView
          ref={this.webView}
          source={{html: HTML}}
          onLoadEnd={()=>{this.webView.current.postMessage('Hello from RN');}}
          automaticallyAdjustContentInsets={false}
          onMessage={(e: {nativeEvent: {data?: string}}) => {
            Alert.alert('Message received from JS: ', e.nativeEvent.data);
          }}
        /> */}
        </View>
        {/* <MainContainer>
                    <Loader loading={this.state.apiLoader} />
                    <View>
                        <ScrollView keyboardShouldPersistTaps={"handled"} showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    <TopHeader
                                        title={'Customer Login'}
                                        onPress={() => {
                                            Actions.pop();
                                        }}
                                    />
                                    <WebView
                                        ref={webview => { this.webview = webview; }}
                                        source={{
                                            uri: "https://www.google.com"
                                        }}
                                        //injectedJavaScript={this.injectjs()}
                                        javaScriptEnabled={true}
                                    //style={styles.webview}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </MainContainer> */}
      </>
    );
  }
}

export default CmsScreen;
