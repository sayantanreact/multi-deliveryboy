import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import driverorderdeliveredstyle from '../styles/driverorderdelivered.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';

// import { FlushMsg } from '../utils'

class DriverOrderDeliveredScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}>
                        <TouchableOpacity
                            style={{}}
                            activeOpacity={0.5}
                        >
                            <Image
                                source={Images.leftArrow}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.HeaderMiddle}>
                        <Text style={Styles.Heading}>Order Delivered</Text>
                    </View>
                    <View style={styles.HeaderRight}>
                        <Text>&nbsp; </Text>
                    </View>
                </View>
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    {this.renderHeaderShop()}
                                    <View>
                                        <View style={[styles.mb_20, { display: 'flex', flexDirection: 'row', justifyContent: 'center' }]}>
                                            <Image
                                                source={Images.orderdelivered}
                                            />
                                        </View>
                                        <View><Text style={[driverorderdeliveredstyle.orderhead12, styles.TextCenter1, Styles.mb_20]} >Order #11253</Text></View>
                                        <View>
                                            <Text style={[driverorderdeliveredstyle.ordertext2, styles.TextCenter1]}>Order Successfully Delivered</Text>
                                            {/* <Text style={[driverorderdeliveredstyle.ordertext,styles.TextCenter1]}>
                                            make sure your order put on the doorstep</Text> */}
                                        </View>
                                        <View style={{ marginTop: 20, width: "100%" }}>
                                            <Button
                                                style={[driverorderdeliveredstyle.signSubmit]}
                                                text={'Home'}
                                                onLoading={false}
                                                onPress={() => {
                                                    Actions.tab_2();
                                                }}
                                            />
                                        </View>
                                        {/* <View>
                                            <TextInput  style={[orderconfirmstyle.signSubmit]}
                                                    value="Check order status" 
                                                /> 
                                        </View> */}
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default DriverOrderDeliveredScreen;
