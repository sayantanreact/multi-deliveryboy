import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator,CheckBox,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import { Modal } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style'; 
import orderstatusstyle from '../styles/orderstatus.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';

// import { FlushMsg } from '../utils'

class OrderStatusScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible:true
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}> 
                        <TouchableOpacity
                        style={{}}
                        activeOpacity={0.5}  
                    >
                            <Image
                                source={Images.leftArrow}
                            />
                            </TouchableOpacity>
                    </View>
                    <View style={styles.HeaderMiddle}> 
                           <Text style={Styles.Heading}>Order Status</Text>
                    </View>
                    <View style={styles.HeaderRight}> 
                           <Text>&nbsp; </Text>
                    </View>
                </View> 
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    {this.renderHeaderShop()}
                                    <View style={styles.commonPadding}>
                                        <View style={[styles.mb_30,{display:'flex',flexDirection:"row" , justifyContent:'center'}]}>  
                                                <Image
                                                    source={Images.orderimg2}
                                                /> 
                                        </View> 
                                        <View style={[styles.commonPadding,styles.brd5,{backgroundColor:Color.whiteColor}]}>
                                            <View><Text style={[orderstatusstyle.orderstatushead1,styles.mb_10]}>Your order ID</Text></View>
                                            <View><Text style={[orderstatusstyle.orderstatushead2,styles.borderBottom, styles.pb_15]}>Your order ID</Text></View>
                                            
                                            <View style={[styles.borderBottom,styles.commonTBpadding1,orderstatusstyle.statusHolder]}>
                                                <View style={{marginRight:10}}>
                                                    <Image
                                                        source={Images.clickcheckbox}
                                                    />
                                                </View>
                                                <View><Text style={[orderstatusstyle.orderstatushead1]}>Order confirmed</Text></View>
                                                <View style={[orderstatusstyle.orderlistRight]}><Text style={[orderstatusstyle.ordertext]}>12.30 PM</Text></View> 
                                            </View>
                                            <View style={[styles.borderBottom,styles.commonTBpadding1,orderstatusstyle.statusHolder]}>
                                                <View style={{marginRight:10}}>
                                                    <Image
                                                        source={Images.clickcheckbox}
                                                    />
                                                </View>
                                                <View><Text style={[orderstatusstyle.orderstatushead1]}>Preparing food</Text></View>
                                                <View style={[orderstatusstyle.orderlistRight]}><Text style={[orderstatusstyle.ordertext]}>12.40 PM</Text></View> 
                                            </View>
                                            <View style={[styles.borderBottom,styles.commonTBpadding1,orderstatusstyle.statusHolder]}>
                                                <View style={{marginRight:10}}>
                                                    <Image
                                                        source={Images.checkbox}
                                                    />
                                                </View>
                                                <View><Text style={[orderstatusstyle.orderstatushead3]}>Food on the way</Text></View>
                                                <View style={[orderstatusstyle.orderlistRight]}><Text style={[orderstatusstyle.ordertext]}>In Progress</Text></View> 
                                            </View>
                                            <View style={[styles.borderBottom,styles.commonTBpadding1,orderstatusstyle.statusHolder]}>
                                                 <View style={{marginRight:10}}>
                                                    <Image
                                                        source={Images.checkbox}
                                                    />
                                                </View>
                                                <View><Text style={[orderstatusstyle.orderstatushead3]}>Delivered to you</Text></View>
                                                <View style={[orderstatusstyle.orderlistRight]}><Text style={[orderstatusstyle.ordertext]}>In Progress</Text></View> 
                                            </View>
                                            <View style={[styles.borderBottom,styles.commonTBpadding1,orderstatusstyle.statusHolder]}>
                                                <View style={{marginRight:10}}>
                                                    <Image
                                                        source={Images.checkbox}
                                                    />
                                                </View>
                                                <View><Text style={[orderstatusstyle.orderstatushead3]}>Rating</Text></View>
                                                <View style={[orderstatusstyle.orderlistRight,{top:6}]}>
                                                    <TouchableOpacity>
                                                        <Text style={[orderstatusstyle.ratebtn]}>Rate Now</Text>
                                                    </TouchableOpacity>
                                                </View> 
                                            </View>
                                            <View><Text style={[orderstatusstyle.orderstatushead4, styles.pt_15]}>Cancel Order</Text></View>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <Modal 
                            
                                visible={this.state.isVisible}
                                animationType={'slide'}
                                transparent={false}>
                                    {/* <View style={[styles.modalMain]}>
                                        <View style={[styles.orderModalContentHolder,styles.commonPadding]}>  
                                            <View style={{display:'flex', flexDirection:'row'}}>
                                                <View style={{marginRight:10}}><Image
                                                        source={Images.orderico1}
                                                    /></View>  
                                                <View>
                                                    <Text style={[orderstatusstyle.orderstatushead2,styles.borderBottom, styles.pb_15]}>Your order ID</Text>
                                                </View>
                                            </View>
                                            <View style={[Styles.modalBtnHolder]}>
                                                <View style={{width:"49%", marginRight:10}}>
                                                    <TouchableOpacity style={[styles.modalBtn]}><Text style={{color:Color.greyColor}}>DON’T CANCEL</Text></TouchableOpacity> 
                                                </View>
                                                <View style={{width:"49%"}}>
                                                    <TouchableOpacity style={[styles.modalBtn,styles.modalBtn1]}><Text style={{color:Color.whiteColor}}>CANCEL ORDER</Text></TouchableOpacity> 
                                                </View>
                                            </View>
                                        </View>
                                    </View> */}


                                    <View style={[styles.modalMain]}>
                                        <View style={[styles.orderModalContentHolder,styles.commonPadding]}>  
                                            <View style={{display:'flex', flexDirection:'row'}}>
                                                <View style={{marginRight:10}}><Image
                                                        source={Images.orderico1}
                                                    /></View>  
                                                <View>
                                                    <Text style={[orderstatusstyle.orderstatushead2, styles.pb_10]}>Order Cancelled</Text>
                                                </View>
                                            </View>
                                            <View style={[Styles.modalBtnHolder,styles.pb_10]}>
                                                <View style={{width:"49%", marginRight:10}}>
                                                    <Text style={{color:Color.greyColor, fontWeight:'bold', fontSize:18}}>Cancellation fee : 0</Text>
                                                </View>
                                                <View style={{width:"49%"}}>
                                                    <Text style={{color:Color.greyColor, fontWeight:'bold',fontSize:18}}>Refund Amount : 0</Text>
                                                </View>
                                            </View>
                                            <View>
                                                <Text style={{fontSize:18, lineHeight:26}}>Your order from Cookies & Cakes has been cancelled.</Text>
                                            </View>
                                        </View>
                                    </View>
                            </Modal>
                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default OrderStatusScreen;
