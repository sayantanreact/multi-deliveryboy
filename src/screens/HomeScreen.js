import React, { Component } from "react";
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  Switch,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import styles from "../styles/main.style";
import pagestyles from "../styles/welcome.style";
// import pagestyles from '../styles/home.style';
import loginpage from "../styles/login.style";
import driverdashboardstyle from "../styles/driverdashboard.style";
import { Actions } from "react-native-router-flux";
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from "../utils/FlushMsg";
import { LoginService } from "../services";
import { OrderService } from "../services";
import Loader from "../utils/Loader";
//import { Constants, Location, Permissions } from 'expo';
import { connect } from "react-redux";
import {
  addToCart,
  selectStadium,
  getSelectedStadiumData,
  selectStadiumDetailsData,
  getSelectedStadiumDetailsData,
} from "../actions/cartActions";
import { EventRegister } from "react-native-event-listeners";
// import GetLocation from 'react-native-get-location'
import {
  MainContainer,
  Button,
  TopHeader,
  ImageCustom, DeclineResonScreen
} from "../components";
import { Color, Images, Dimen, Fonts, GlobalFunction } from "../utils";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Geolocation from "@react-native-community/geolocation";
import Geocoder from "react-native-geocoding";
import Permissions, {
  PERMISSIONS,
  openSettings,
} from "react-native-permissions";

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loginUserData: JSON.parse(global.loginUserData),
      orderData: {},
      cancel_reason: '',
      isShowReasonBox: false,
      activeStatus:
        JSON.parse(global.loginUserData).login_status == "1" ? true : false,
    };
  }

  componentDidMount() {
    this.updateDeviceTokenApi();
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({
        loginUserData: JSON.parse(global.loginUserData),
      });
      // The screen is focused
      // Call any action
      this.locationFetch();
      this.getCurrentOrder();

    });
    this.listener = EventRegister.addEventListener("reloadDash", (data) => {
      this.getCurrentOrder();
    });
  }

  componentWillUnmount() {
    EventRegister.removeEventListener(this.listener);
  }

  /**
   * Location get
   */

  locationFetch = () => {
    console.log("locationFetch locationFetch::: ");
    // check permission in runtime
    if (Platform.OS == "ios") {
      Permissions.check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then(
        (response) => {
          console.log("response permission::: ", response);
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          this.setState(
            {
              locationPermission: response,
            },
            () => {
              if (response === "undetermined" || response === "unavailable") {
                this._alertForLocationPermission();
              } else if (
                response === "denied" ||
                response === "restricted" ||
                response === "blocked"
              ) {
                this._alertForLocationPermission();
              } else if (response === "granted") {
                this.getMyCurrentLocation();
              } else {
                //Alert.alert("Permission is granted");
                this.getMyCurrentLocation();
              }
            }
          );
        }
      );
    } else {
      Permissions.check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(
        (response) => {
          console.log("response permission android Location ::: ", response);
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          this.setState({ locationPermission: response });
          if (response === "undetermined" || response === "unavailable") {
            this._alertForLocationPermission();
          } else if (
            response === "denied" ||
            response === "restricted" ||
            response === "blocked"
          ) {
            this._alertForLocationPermission();
          } else if (response === "granted") {
            this.getMyCurrentLocation();
          } else {
            //Alert.alert("Permission is granted");
            this._alertForLocationPermission();
          }
        }
      );
    }
  };
  _alertForLocationPermission() {
    //this._requestPermission;
    console.log(
      "_alertForLocationPermission:: ",
      this.state.locationPermission
    );
    if (
      this.state.locationPermission == "undetermined" ||
      this.state.locationPermission == "unavailable" ||
      this.state.locationPermission == "denied" ||
      this.state.locationPermission == "blocked"
    ) {
      if (Platform.OS == "ios" && this.state.locationPermission == "blocked") {
        //console.log('openSettings');
        //Permissions.openSettings;
        //openSettings().catch(() => //console.log('cannot open settings'));
        this.alertForOpenLocationSettings();
      } else if (
        Platform.OS == "android" &&
        this.state.locationPermission == "blocked"
      ) {
        this.alertForOpenLocationSettings();
      } else {
        this._requestPermission();
      }
    } else {
      Permissions.openSettings;
    }
  }

  // Request permission to access photos
  _requestPermission = () => {
    if (Platform.OS == "ios") {
      Permissions.request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then(
        (response) => {
          // Returns once the user has chosen to 'allow' or to 'not allow' access
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          //this.locationFetch();

          //console.log('response location::: ', response);
          if (response === "granted") {
            this.setState(
              {
                locationPermission: response,
              },
              () => {
                this.getMyCurrentLocation();
              }
            );
          } else if (response === "blocked") {
            this.alertForOpenLocationSettings();
          } else {
            this._requestPermission();
          }
        }
      );
    } else {
      Permissions.request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(
        (response) => {
          // Returns once the user has chosen to 'allow' or to 'not allow' access
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          //this.locationFetch();

          console.log("ACCESS_FINE_LOCATION response location::: ", response);
          if (response === "granted") {
            this.setState(
              {
                locationPermission: response,
              },
              () => {
                this.getMyCurrentLocation();
              }
            );
          } else if (response === "blocked") {
            this.alertForOpenLocationSettings();
          } else {
            this._requestPermission();
          }
        }
      );
    }
  };

  alertForOpenLocationSettings = () => {
    Alert.alert(
      "Allow 'MultiMulti' to access your location while you are using the app?",
      "App will track your current location to provide you nearest orders.",
      [
        {
          text: "CANCEL",
          onPress: () => {
            //console.log('Permission denied');
            //this.alertForOpenLocationSettings();
          },
          style: "cancel",
        },
        {
          text: "Open Settings",
          onPress: () => {
            openSettings().catch(() => console.log("cannot open settings"));
          },
        },
      ]
    );
  };

  getMyCurrentLocation = () => {
    //console.log('initialPosition getMyCurrentLocation:: ');
    // Initialize the module (needs to be done only once)
    //AIzaSyAhfsNgce8X5FWPqZwzB_7_ms_XHOzSNgM
    //AIzaSyCX08V5ZLFsRUZfGkE74KQoRA_Uw6Fc0fw

    Geolocation.getCurrentPosition(
      (position) => {
        const initialPosition = JSON.stringify(position);
        console.log("initialPosition InitialPosition:: ", initialPosition);
        this.setState(
          {
            myLocation: initialPosition,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
          () => {
            // this.getHomeData();
            // global.latitude = this.state.latitude;
            // global.longitude = this.state.longitude;
            global.userLat = this.state.latitude;
            global.userLong = this.state.longitude;
            this.updateDriverLocation();
          }
        );
        Geocoder.from(position.coords.latitude, position.coords.longitude)
          .then((json) => {
            //console.log(json);
            var addressComponent = json.results[0].address_components;
            console.log("addressComponent:: ", addressComponent);
            this.setState(
              {
                myAddress: addressComponent[0],
                userAddress:
                  addressComponent[0]?.long_name +
                  ", " +
                  addressComponent[1]?.long_name +
                  ", " +
                  addressComponent[2]?.long_name +
                  ", " +
                  addressComponent[3]?.long_name,
              },
              () => {
                global.userFullAdd = this.state.myAddress;
                global.userAddress = this.state.myAddress;
                console.log("myAddressmyAddress:: ", this.state.myAddress);
              }
            );
            //this.searchText.setAddressText(this.state.myAddress.long_name);
          })
          .catch((error) => {
            console.log("errorerror:: ", error);
          });
      },
      (error) => {
        console.log("error fetch location:: ", JSON.stringify(error));
        //Alert.alert('Error', error.message + ' Please turn on your location to get better result.')
        Alert.alert(
          "Error",
          error.message + " Please turn on your location to get better result.",
          [
            {
              text: "OK",
              onPress: () => {
                //this.homeApi();
              },
            },
            //{ text: 'NO' },
          ]
        );
        // this.setState(
        //     {
        //         latitude: global.latitude,
        //         longitude: global.longitude,
        //         myAddress: global.myAddress,
        //     },
        //     () => {
        //         // this.getHomeData();
        //     },
        // );
      },
      // { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 3600000 }
      // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    this.watchID = Geolocation.watchPosition((position) => {
      const lastPosition = JSON.stringify(position);
      //console.log('lastPosition:: ', lastPosition);
      this.setState({ lastPosition });
    });
  };

  updateDriverLocation = () => {
    var myData = {
      latitude: global.userLat,
      longitude: global.userLong,
      driverID: this.state.loginUserData.driverID,
    };
    LoginService.updateDriverLocation(myData).then(
      function (result) {
        //this.loaderShowHide(false);
        console.log("result:: ", result);
        if (result.status) {
        } else {
          //FlushMsg.showError(result.message);
        }
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        //this.loaderShowHide(false);
        //FlushMsg.showError(result.message);
      }.bind(this)
    );
  };

  /**
   * Location get
   */

  updateDeviceTokenApi = async () => {
    //this.loaderShowHide(true);
    let fcmToken = await AsyncStorage.getItem("fcmToken");
    console.log("fcmTokenfcmToken:: ", fcmToken);
    var myData = {
      device_token: fcmToken, //"41.78677589999999",
      device_type: Platform.OS, //"-87.7521884",
      driverID: this.state.loginUserData.driverID,
    };
    LoginService.updateDeviceToken(myData).then(
      function (result) {
        //this.loaderShowHide(false);
        console.log("result:: ", result);
        if (result.status) {
        } else {
          //FlushMsg.showError(result.message);
        }
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        //this.loaderShowHide(false);
        //FlushMsg.showError(result.message);
      }.bind(this)
    );
  };

  activeInactivestatus = (status) => {
    this.loaderShowHide(true);
    var myData = {
      driverID: this.state.loginUserData.driverID,
      status: status == false ? "0" : "1",
    };
    LoginService.activeInactivestatus(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          this.setState({
            activeStatus: this.state.activeStatus,
          });
        } else {
          FlushMsg.showError(result.message);
          this.setState({
            activeStatus: !this.state.activeStatus,
          });
        }
        console.log("result:: ", result);
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
        this.setState({
          activeStatus: !this.state.activeStatus,
        });
      }.bind(this)
    );
  };

  getCurrentOrder = () => {
    this.loaderShowHide(true);
    var myData = {
      driverID: this.state.loginUserData.driverID,
    };
    OrderService.getCurrentOrder(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          this.setState({
            orderData: result?.data[0],
          });
        } else {
          FlushMsg.showError(result.message);
        }
        console.log("result:: ", result);
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this)
    );
  };

  acceptOrder = (status) => {
    this.loaderShowHide(true);
    var myData = {
      driverID: this.state.loginUserData.driverID,
      orderID: this.state.orderData.orderID,
      astatus: status,
      cancel_reason: this.state.cancel_reason
    };
    OrderService.changeCurrentOrderStatus(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          this.setState({
            orderData: result?.data[0],
            cancel_reason: ''
          });
        } else {
          FlushMsg.showError(result.message);
        }
        console.log("result:: ", result);
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this)
    );
  };

  updateOrderStatus = (status) => {
    this.loaderShowHide(true);
    var myData = {
      driverID: this.state.loginUserData.driverID,
      orderID: this.state.orderData.orderID,
      status: status,
    };
    OrderService.updateOrderStatus(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          if (status == 18) {
            this.setState({
              orderData: {},
            });
          } else {
            this.setState({
              orderData: result?.data[0],
            });
          }
        } else {
          FlushMsg.showError(result.message);
        }
        console.log("result:: ", result);
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this)
    );
  };

  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };

  callCancelOrderAlert() {
    Alert.alert("CANCEL", "Are You Sure?", [
      {
        text: "YES",
        onPress: () => {
          //this.acceptOrder("2");
          this.openReasonBox();
        },
      },
      { text: "NO" },
    ]);
  }

  renderHeaderShop = () => {
    return (
      <View>
        <View style={loginpage.mainView}>
          <View style={[styles.HeaderLeft, styles.HeaderLeftBackButton]}>
            <TouchableOpacity
              style={{}}
              activeOpacity={0.5}
              onPress={() => {
                Actions.pop();
              }}
            >
              <Image source={Images.leftArrow} />
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={[
                loginpage.headingtext,
                {
                  fontSize: 20,
                  marginTop: 4,
                },
              ]}
            >
              Home
            </Text>
          </View>
        </View>
      </View>
    );
  };

  openReasonBox = () => {
    this.setState({
      isShowReasonBox: true,
    });
  };

  closeReasonBox = () => {
    this.setState({
      isShowReasonBox: false,
    });
  };

  submitReasonBox = (info) => {
    console.log("info:: ", info);
    this.setState(
      {
        isShowReasonBox: false,
        cancel_reason: info?.cancel_reason
      },
      () => {
        this.acceptOrder("2");
      }
    );
  };

  render() {
    const { orderData, loginUserData } = this.state;
    console.log(
      "this.state.loginUserData.profile_photo:: ",
      this.state.loginUserData.profile_photo
    );
    return (
      <>
        <MainContainer>
          <Loader loading={this.state.loading} />
          {
            (this.state.isShowReasonBox) && <DeclineResonScreen
              closeReasonBox={this.closeReasonBox.bind(this)}
              submitReasonBox={this.submitReasonBox.bind(this)}
            />
          }
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={pagestyles.Wrapper}>
              <View style={pagestyles.Container}>
                <TopHeader title={"My Order"} isMenu={true} />
              </View>
              <View
                style={[
                  driverdashboardstyle.dashboardTop,
                  { width: Dimen.width },
                ]}
              >
                <ImageBackground
                  source={Images.homebg}
                  resizeMode="cover"
                  style={{
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <View style={driverdashboardstyle.profileico}>
                    <ImageCustom
                      imageUri={this.state.loginUserData.profile_photo}
                      avatar={Images.avatar}
                      customStyles={[
                        {
                          width: 84,
                          height: 84,
                          padding: 5,
                          borderRadius: 200,
                          overflow: "hidden",
                        },
                      ]}
                    />
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={driverdashboardstyle.dashTopText}>
                      {this.state.loginUserData.name}
                    </Text>
                  </View>
                  <View>
                    <Text style={driverdashboardstyle.dashTopText1}>
                      {this.state.loginUserData.email}
                    </Text>
                  </View>
                  {/* <View>
                                        <Text style={driverdashboardstyle.dashTopText1}>{this.state.loginUserData.phone_no}</Text>
                                    </View> */}
                </ImageBackground>
              </View>
              <View
                style={[
                  driverdashboardstyle.activebox,
                  { width: Dimen.width - 30 },
                ]}
              >
                <View>
                  <Text
                    style={[
                      driverdashboardstyle.activetext,
                      {
                        fontSize: 16,
                        fontFamily: Fonts.semiBold,
                        fontWeight: "bold",
                      },
                    ]}
                  >
                    Active
                  </Text>
                </View>
                <View>
                  <Switch
                    trackColor={{ false: "#ddd", true: Color.primaryColor }}
                    thumbColor={this.state.activeStatus ? "#FFF" : "#999"}
                    //ios_backgroundColor="#28D140"
                    onValueChange={() => {
                      this.setState(
                        {
                          activeStatus: !this.state.activeStatus,
                        },
                        () => {
                          this.activeInactivestatus(this.state.activeStatus);
                        }
                      );
                    }}
                    value={this.state.activeStatus}
                    style={{
                      alignSelf: "flex-end",
                      marginTop: 4,
                      marginRight: 12,
                      transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }],
                    }}
                  />
                </View>
              </View>
              <View style={pagestyles.Container}>
                <View style={{ paddingBottom: 5 }}>
                  <Text style={driverdashboardstyle.dashtext1}>
                    Active Order
                  </Text>
                </View>
                {orderData?.order_status == "13" ? (
                  <View style={driverdashboardstyle.dashboardcontentholder}>
                    <View style={driverdashboardstyle.flex1}>
                      <View style={{ marginRight: 10 }}>
                        <Image source={Images.clock} />
                      </View>
                      <View style={{ width: "90%" }}>
                        <View style={[driverdashboardstyle.contentdiv1, {
                          paddingBottom: 5
                        }]}>
                          <View style={driverdashboardstyle.contentleft}>
                            <View>
                              <Text style={driverdashboardstyle.dashhead1}>
                                Order {orderData?.order_number}
                              </Text>
                            </View>
                          </View>
                          <View style={driverdashboardstyle.contentRight}>
                            <View>
                              <Text
                                style={[
                                  driverdashboardstyle.dashtext,
                                  { textAlign: "right", fontWeight: 'bold', color: Color.blackColor, },
                                ]}
                              >
                                {orderData?.order_items?.length > 0
                                  ? orderData?.order_items.length
                                  : 0}{" "}
                                {(orderData?.order_items.length > 1) ? "Items" : "Item"}
                              </Text>
                            </View>
                            {/* <View>
                              <Text
                                style={[
                                  driverdashboardstyle.dashtext,
                                  { textAlign: "right" },
                                ]}
                              >
                                You will earn: £{orderData?.delivery_charge}
                              </Text>
                            </View> */}
                          </View>
                        </View>
                        <View style={driverdashboardstyle.contentdiv1}>
                          <View style={driverdashboardstyle.contentleft}>
                            {
                              (loginUserData.venderID == "") &&
                              <View>
                                <Text
                                  style={[
                                    driverdashboardstyle.dashhead1,
                                    {
                                      //textAlign: "right",
                                      color: Color.orangeColor
                                    },
                                  ]}
                                >
                                  {/* You will earn: £{orderData?.driver_earning} */}
                                  You will earn £{orderData?.delivery_charge} Gross, £{orderData?.driver_earning} Net
                                </Text>
                              </View>
                            }

                            <View style={{ flexDirection: "row" }}>
                              <Text style={driverdashboardstyle.dashtext}>
                                Order assigned{" "}
                                {GlobalFunction.timeAgo(orderData?.order_date)}
                              </Text>
                            </View>
                            {
                              (orderData?.spl_instruct_driver != "") &&
                              <View style={{
                                marginTop: 15
                              }}>
                                <Text style={{
                                  fontWeight: 'bold'
                                }}>Delivery Instructions</Text>
                                <Text style={driverdashboardstyle.dashtext}>{orderData?.spl_instruct_driver}</Text>
                              </View>
                            }


                          </View>
                          
                        </View>
                        <View style={driverdashboardstyle.contentdiv1}>
                          <View style={driverdashboardstyle.contentleft}>
                            <Text style={{
                              fontSize: 14,
                              fontWeight: "bold"
                            }}>Collect From</Text>
                            <View>
                              <Text style={driverdashboardstyle.dashhead1}>
                                {orderData?.store_type == "R"
                                  ? orderData?.pickup_address?.restaurant_name
                                  : orderData?.pickup_address?.store_name}
                              </Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                              <Text style={driverdashboardstyle.dashtext}>
                                {orderData?.pickup_address?.address}
                              </Text>
                            </View>
                            <View
                              style={{
                                marginTop: 6,
                              }}
                            >
                              <TouchableOpacity
                                onPress={() => {
                                  GlobalFunction.dialCall(
                                    orderData?.pickup_address?.phone_no
                                  );
                                }}
                              >
                                <Text
                                  style={[
                                    {
                                      color: Color.orangeColor,
                                      fontWeight: "bold",
                                    },
                                  ]}
                                >
                                  Call {orderData?.pickup_address?.phone_no}
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                        <View style={driverdashboardstyle.contentdiv1}>
                          <View style={driverdashboardstyle.contentleft}>
                            <View>
                              <Text style={driverdashboardstyle.dashhead1}>
                                Deliver To {orderData?.customer_name}
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: "row",
                                marginTop: 6,
                              }}
                            >
                              <Text style={driverdashboardstyle.dashtext}>
                                {orderData?.delivery_address}
                              </Text>
                            </View>
                            <View
                              style={{
                                marginTop: 6,
                              }}
                            >
                              <TouchableOpacity
                                onPress={() => {
                                  GlobalFunction.dialCall(
                                    orderData?.customer_phone
                                  );
                                }}
                              >
                                <Text
                                  style={[
                                    {
                                      color: Color.orangeColor,
                                      fontWeight: "bold",
                                    },
                                  ]}
                                >
                                  Call {orderData?.customer_phone}
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                        <View>
                          <View style={driverdashboardstyle.contentdiv}>
                            <View>
                              <Text style={[driverdashboardstyle.dashtext, {
                                fontWeight: 'bold',
                                color: Color.blackColor,
                                fontSize: 14
                              }]}>
                                {orderData?.order_items?.length > 0
                                  ? orderData?.order_items.length
                                  : 0}{" "}
                                {(orderData?.order_items.length > 1) ? "Items" : "Item"}
                              </Text>
                            </View>
                          </View>
                          {orderData?.order_items?.length > 0 &&
                            orderData?.order_items.map((item, index) => {
                              return (
                                <View
                                  style={[
                                    driverdashboardstyle.contentdiv,
                                    parseInt(orderData?.order_items?.length) - 1 ==
                                      index
                                      ? {}
                                      : {
                                        borderBottomWidth: 1,
                                        borderColor: Color.grayDash,
                                      },
                                  ]}
                                >
                                  <View style={driverdashboardstyle.contentleft}>
                                    <View>
                                      <Text style={driverdashboardstyle.dashhead2}>
                                        {item?.item_name}
                                      </Text>
                                    </View>
                                    <View>
                                      <Text style={driverdashboardstyle.dashtext}>
                                        Qty: {item?.quantity}
                                      </Text>
                                    </View>
                                  </View>
                                  {/* <View style={driverdashboardstyle.contentRight}>
                              <View>
                                <Text style={driverdashboardstyle.dashtext}>
                                  £{item?.price}
                                </Text>
                              </View>
                            </View> */}
                                </View>
                              );
                            })}
                        </View>
                      </View>
                    </View>
                    <View
                      style={[
                        driverdashboardstyle.BtnHolder,
                        { marginTop: 10 },
                      ]}
                    >
                      <View style={{ marginRight: 10 }}>
                        <TouchableOpacity
                          style={[driverdashboardstyle.Btn]}
                          onPress={() => {
                            //Actions.driverdashboard2screen()
                            this.acceptOrder("1");
                          }}
                        >
                          <Text style={{ fontSize: 14 }}>Accept</Text>
                        </TouchableOpacity>
                      </View>
                      <View>
                        <TouchableOpacity
                          onPress={() => {
                            //Actions.driverdashboard2screen()
                            this.callCancelOrderAlert();
                          }}
                          style={[
                            driverdashboardstyle.Btn,
                            driverdashboardstyle.Btn1,
                          ]}
                        >
                          <Text style={{ fontSize: 14 }}>Decline</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : orderData?.order_status == "14" ||
                  orderData?.order_status == "15" ||
                  orderData?.order_status == "16" ? (
                  <View
                    style={[
                      driverdashboardstyle.dashboardcontentholder,
                      {
                        marginBottom: 50,
                      },
                    ]}
                  >
                    <View style={driverdashboardstyle.contentdiv}>
                      <View style={driverdashboardstyle.contentleft}>
                        <View>
                          <Text style={driverdashboardstyle.dashhead1}>
                            Order {orderData?.order_number}
                          </Text>
                        </View>
                        <View
                          style={driverdashboardstyle.contentRight}
                        >
                          {
                            (loginUserData.venderID == "") &&
                            <View>
                              <Text
                                style={[
                                  driverdashboardstyle.dashhead1,
                                  { color: Color.orangeColor },
                                ]}
                              >
                                {/* You will earn: £{orderData?.driver_earning} */}
                                You will earn £{orderData?.delivery_charge} Gross, £{orderData?.driver_earning} Net
                              </Text>
                            </View>
                          }
                        </View>
                        {/* <View style={{ flexDirection: "row" }}>
                          <Text style={driverdashboardstyle.dashtext}>
                            Payment :{" "}
                          </Text>
                          <Text
                            style={[
                              driverdashboardstyle.dashtext,
                              { color: Color.greenColor },
                            ]}
                          >
                            {orderData?.payment_method}
                          </Text>
                        </View> */}
                        <View>
                          <Text style={driverdashboardstyle.dashtext}>
                            Order Assigned{" "}
                            {GlobalFunction.timeAgo(orderData?.order_date)}
                          </Text>
                        </View>
                        {
                          (orderData?.spl_instruct_driver != "") &&
                          <View style={{
                            marginTop: 15
                          }}>
                            <Text style={{
                              fontWeight: 'bold'
                            }}>Delivery Instructions</Text>
                            <Text style={driverdashboardstyle.dashtext}>{orderData?.spl_instruct_driver}</Text>
                          </View>
                        }
                      </View>
                      {/* <View style={driverdashboardstyle.contentRight}>
                        <View>
                          <Text
                            style={[
                              driverdashboardstyle.dashhead1,
                              { color: Color.orangeColor },
                            ]}
                          >
                            You Earn: £{orderData?.delivery_charge}
                          </Text>
                        </View>
                      </View> */}
                    </View>

                    <View style={driverdashboardstyle.contentdiv1}>
                      <View style={driverdashboardstyle.contentleft}>
                        <Text style={{
                          fontSize: 14,
                          fontWeight: "bold"
                        }}>Collect From</Text>
                        <View>
                          <Text style={driverdashboardstyle.dashhead1}>
                            {orderData?.store_type == "R"
                              ? orderData?.pickup_address?.restaurant_name
                              : orderData?.pickup_address?.store_name}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: "row",
                            marginTop: 6,
                          }}
                        >
                          <Text style={driverdashboardstyle.dashtext}>
                            {orderData?.pickup_address?.address}
                          </Text>
                        </View>
                        <View
                          style={{
                            marginTop: 6,
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => {
                              GlobalFunction.dialCall(
                                orderData?.pickup_address?.phone_no
                              );
                            }}
                          >
                            <Text
                              style={[
                                {
                                  color: Color.orangeColor,
                                  fontWeight: "bold",
                                },
                              ]}
                            >
                              Call {orderData?.pickup_address?.phone_no}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                    {/* {orderData?.order_status == "16" && ( */}
                    <View style={driverdashboardstyle.contentdiv1}>
                      <View style={driverdashboardstyle.contentleft}>
                        <View>
                          <Text style={driverdashboardstyle.dashhead1}>
                            Deliver To {orderData?.customer_name}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: "row",
                            marginTop: 6,
                          }}
                        >
                          <Text style={driverdashboardstyle.dashtext}>
                            {orderData?.delivery_address}
                          </Text>
                        </View>
                        <View
                          style={{
                            marginTop: 6,
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => {
                              GlobalFunction.dialCall(
                                orderData?.customer_phone
                              );
                            }}
                          >
                            <Text
                              style={[
                                {
                                  color: Color.orangeColor,
                                  fontWeight: "bold",
                                },
                              ]}
                            >
                              Call {orderData?.customer_phone}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                    {/* )} */}

                    <View style={driverdashboardstyle.contentdiv}>
                      <View>
                        <Text style={[driverdashboardstyle.dashtext, {
                          fontWeight: 'bold',
                          color: Color.blackColor,
                          fontSize: 14
                        }]}>
                          {orderData?.order_items?.length > 0
                            ? orderData?.order_items.length
                            : 0}{" "}
                          {(orderData?.order_items.length > 1) ? "Items" : "Item"}
                        </Text>
                      </View>
                    </View>
                    {orderData?.order_items?.length > 0 &&
                      orderData?.order_items.map((item, index) => {
                        return (
                          <View
                            style={[
                              driverdashboardstyle.contentdiv,
                              parseInt(orderData?.order_items?.length) - 1 ==
                                index
                                ? {}
                                : {
                                  borderBottomWidth: 1,
                                  borderColor: Color.grayDash,
                                },
                            ]}
                          >
                            <View style={driverdashboardstyle.contentleft}>
                              <View>
                                <Text style={driverdashboardstyle.dashhead2}>
                                  {item?.item_name}
                                </Text>
                              </View>
                              <View>
                                <Text style={driverdashboardstyle.dashtext}>
                                  Qty: {item?.quantity}
                                </Text>
                              </View>
                            </View>
                            {/* <View style={driverdashboardstyle.contentRight}>
                              <View>
                                <Text style={driverdashboardstyle.dashtext}>
                                  £{item?.price}
                                </Text>
                              </View>
                            </View> */}
                          </View>
                        );
                      })}

                    <View style={{ marginTop: 20, width: "100%" }}>
                      {orderData?.order_status == "14" ? (
                        <>
                          <Button
                            style={[driverdashboardstyle.signSubmit]}
                            text={"Arrived at Merchant"}
                            onLoading={false}
                            onPress={() => {
                              // Actions.driverdashboard3screen()
                              this.updateOrderStatus(15);
                            }}
                          />
                        </>
                      ) : orderData?.order_status == "15" ? (
                        <Button
                          style={[driverdashboardstyle.signSubmit]}
                          text={"Order Pickup"}
                          onLoading={false}
                          onPress={() => {
                            // Actions.driverdashboard3screen()
                            this.updateOrderStatus(16);
                          }}
                        />
                      ) : (
                        <Button
                          style={[driverdashboardstyle.signSubmit]}
                          text={"Delivered"}
                          onLoading={false}
                          onPress={() => {
                            // Actions.driverdashboard3screen()
                            this.updateOrderStatus(18);
                          }}
                        />
                      )}
                    </View>
                  </View>
                ) : (
                  <View style={driverdashboardstyle.dashboardcontentholder}>
                    <Text
                      style={{
                        textAlign: "center",
                      }}
                    >
                      No order found, please wait for new order.
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </ScrollView>
        </MainContainer>
      </>
    );
  }
}

// export default HomeScreen;

const mapStateToProps = (state) => {
  console.log("statestatestate::: ", state);
  return {
    items: state.carts.items,
    addedItems: state.carts.addedItems,
    totalCartPrice: state.carts.totalCartPrice,
    totalCartQty: state.carts.totalCartQty,
    selectedStadium: state.carts.selectStadium,
    selectStadiumDetails: state.carts.selectStadiumDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (data) => {
      dispatch(addToCart(data));
    },
    selectStadium: (data) => {
      dispatch(selectStadium(data));
    },
    // removeToCart: (data) => { dispatch(removeToCart(data)) },
    // getCartData: (data) => { dispatch(getCartData(data)) }, // ()=>{dispatch({type: 'GET_CART'})},
    getSelectedStadiumData: (data) => {
      dispatch(getSelectedStadiumData(data));
    },
    selectStadiumDetailsData: (data) => {
      dispatch(selectStadiumDetailsData(data));
    },
    getSelectedStadiumDetailsData: (data) => {
      dispatch(getSelectedStadiumDetailsData(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
