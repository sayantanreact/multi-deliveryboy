import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import orderconfirmstyle from '../styles/orderconfirm.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';

// import { FlushMsg } from '../utils'

class OrderConfirmScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}> 
                        <Text style={Styles.Heading}>&nbsp;</Text>
                    </View>
                    <View style={styles.HeaderMiddle}> 
                           <Text style={Styles.Heading}>&nbsp;</Text>
                    </View>
                    <View style={styles.HeaderRight}> 
                           <Text>&nbsp; </Text>
                    </View>
                </View> 
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    {this.renderHeaderShop()}
                                    <View style={styles.commonPadding}>
                                        <View style={[styles.mb_20,{display:'flex', justifyContent:'center'}]}>  
                                                <Image
                                                    source={Images.orderimg}
                                                /> 
                                        </View>
                                        <View><Text style={[orderconfirmstyle.orderhead1,styles.TextCenter1,Styles.mb_20]} >Your order is confirmed!</Text></View>
                                        <View>
                                            <Text style={[orderconfirmstyle.ordertext,styles.TextCenter1]}>we'll deliver your order immediately, </Text>
                                            <Text style={[orderconfirmstyle.ordertext,styles.TextCenter1]}>
                                            make sure your order put on the doorstep</Text>
                                        </View>
                                        <View style={{ marginTop: 20,width:"100%" }}>
                                            <Button
                                               style={[orderconfirmstyle.signSubmit]}
                                                text={'Check order status'}
                                                onLoading={false} 
                                                onPress={() => {
                                                    Actions.otpscreen();
                                                }}
                                            />
                                        </View>
                                        {/* <View>
                                            <TextInput  style={[orderconfirmstyle.signSubmit]}
                                                    value="Check order status" 
                                                /> 
                                        </View> */}
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default OrderConfirmScreen;
