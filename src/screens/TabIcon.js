import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet,Text,View, Image, Dimensions} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Images, Color, Fonts} from '../utils'

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const propTypes = {
  focused: PropTypes.bool,
  title: PropTypes.string,
};

const defaultProps = {
  focused: false,
  title: '',
};

const styles = StyleSheet.create({
  // Notification:{
  //   width: 8,
  //   height: 8,
  //   backgroundColor: '#F53838',
  //   borderRadius: 30,
  //   position: "absolute",
  //   top: 0,
  //   left: 20,
  // },
  MenuIcon:{
    width: ScreenWidth / 5,
    alignItems: "center",
    //borderRightColor:"#eee",
    //borderRightWidth:1,
    //backgroundColor: 'red',
    paddingLeft: 10,
    paddingRight: 10,
    // paddingTop: 5,
    // paddingBottom: 5,
  },
});

const TabIcon = props => (
  <View style={styles.MenuIconWrap}>
      {
        (props.title == 'Menu') ? 
        <View style={styles.MenuIcon}>
          <Image 
          source={(props.focused) ? 
            Images.menuActive1 : Images.menu1} 
            style={{ width: 20, height: 20 }} /> 
        </View>: 
        (props.title == 'Account') ?  

        <View style={styles.MenuIcon}>
          <Image
          source={(props.focused) ? 
            Images.menuActive4 : Images.menu4} 
            style={{ width: 20, height: 20 }} /> 
        </View> : 
        (props.title == 'Order') ?

        <View style={styles.MenuIcon}>
          <Image 
          source={(props.focused) ? 
            Images.menuActive3 : Images.menu3} 
            style={{alignItems: "center", width: 20, height: 20 }} /> 
        </View> : 
        (props.title == 'Report') ?

        <View style={styles.MenuIcon}>
          <Image 
          source={(props.focused) ? 
            Images.menuActive3 : Images.menu3} 
            style={{alignItems: "center", width: 20, height: 20 }} /> 
        </View> :
        (props.title == 'Account') ?

        <View style={styles.MenuIcon}>
          <Image 
            source={(props.focused) ? 
              Images.menuActive4 : Images.menu4} 
              style={{ width: 20, height: 20 }} /> 
        </View> : 
        <Image source={(props.focused) ? Images.menuActive1 : Images.menu1} style={{ width: 20, height: 20 }} />
      }

      <View style={{alignItems: 'center'}}>
        <Text style={{
          color: props.focused ? Color.primaryColor : Color.blackColor, 
          fontSize: 12,
          fontFamily: Fonts.semiBold
          }}>{props.title}</Text> 
      </View>
    
  </View>
);

TabIcon.propTypes = propTypes;
TabIcon.defaultProps = defaultProps;

export default TabIcon;
