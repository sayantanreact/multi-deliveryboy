import React, { Component } from "react";
import {
  Text,
  View,
  Modal,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import styles from "../styles/dropdown.style";
import { Apis } from "../utils/Apis";
import Loader from "../utils/Loader";
import { Images } from '../utils'

class DropDownScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      listData: this.props.displayData, //[],
      page: 1,
      loading: false,
      ended: false,
      totalCount: 0,
      selectItem: props.selectItem,
      selectIndex: props.selectIndex,
      apiLoader: false,
      title: this.props.title,
      displayValue: this.props.displayValue
    };
  }

  componentDidMount() {
    //this.getCityList();
    console.log('dropdown listDatalistData:: ', this.state.listData)
  }

  getCityList = () => {
    let api = "country-list";
    //Apis.callApi(api).then(function (result) {
    Apis.callGetApis(api).then(
      function (result) {
        console.log("Your data: " + JSON.stringify(result));
        //this.loaderShowHide(false);
        if (result.status) {
          console.log("result.message:: ", result.message);
          if (result.data.length > 0) {
            this.setState(
              {
                listData: [...this.state.listData, ...result.data],
                totalCount: 0, //result.totalCount,
                loading: false,
                ended: false,
              },
              () => {
                console.log(
                  "listDatalistDatalistData: ",
                  JSON.stringify(this.state.listData)
                );
              }
            );
          } else {
            this.setState(
              {
                listData: [...this.state.listData, ...result.data],
                totalCount: 0, //result.totalCount,
                next_page: null,
                loading: false,
                ended: true,
              },
              () => {
                console.log(
                  "listDatalistDatalistData: ",
                  JSON.stringify(this.state.listData)
                );
              }
            );
          }
        } else {
          console.log("result.message:: ", result.message);
          this.setState({
            listData: [...this.state.listData, ...result.data],
            loading: false,
            ended: true,
          });
        }
      }.bind(this),
      function () {
        //this.loaderShowHide(false);
        console.log("There was an error fetching the time");
        this.setState({
          listData: [...this.state.listData, ...result.data],
          loading: false,
          ended: true,
        });
      }.bind(this)
    );
  };

  renderFooter = () => {
    const { ended } = this.state;
    if (this.state.contacts.length == 0 && ended)
      return (
        <View style={styles.PersonList}>
          <Text
            style={{
              textAlign: "center",
            }}
          >
            No cities found to display.
          </Text>
        </View>
      );
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: "#000", padding: 20 }}
        color="#001684"
      />
    );
  };

  selectPromotion = (item) => {
    console.log("item.phoneNumbers:: ", item);
    // console.log('this.state.selectItem:: ', this.state.selectItem);
    // console.log('this.state.selectIndex:: ', this.state.selectIndex);
    this.props.closeModal({
      promotionListVisible: false,
      showMsg: true,
      item: item,
    });
  };

  selectDoNotFertilize = () => {
    this.props.closeModal({
      promotionListVisible: false,
      showMsg: true,
      item: null,
    });
  };

  closeModalAction = () => {
    this.props.closeModal({
      promotionListVisible: false,
    });
  };

  loaderShowHide = (status) => {
    this.setState({
      apiLoader: status,
    });
  };

  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={true}
        coverScreen={true}
        onRequestClose={() => {
          //Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.container}>
          <Loader loading={this.state.apiLoader} />
          <View style={styles.modalViewDial}>
            <TouchableOpacity
              style={styles.CloseButton}
              onPress={() => {
                this.closeModalAction(false);
              }}
            >
              {/* <View style={styles.CloseModal}> */}
              <Image
                source={Images.closeIcon}
                style={{ width: 30, height: 30 }}
              />
              {/* </View> */}
            </TouchableOpacity>
            <View>
              <View style={styles.fixToText}>
                <Text
                  style={[
                    styles.textBlack,
                    {
                      height: 40,
                      marginTop: 10,
                    },
                  ]}
                >
                  {this.state.title}
                </Text>
              </View>
            </View>
            <View style={{ flexGrow: 0 }}>
              <FlatList
                data={this.state.listData}
                renderItem={({ item, index }) => {
                  const promotionTile = (this.state.displayValue != "") ? item[this.state.displayValue] : item;
                  return (
                    <TouchableOpacity
                      style={{
                        width: "100%",
                      }}
                      onPress={() => {
                        this.selectPromotion(item);
                      }}
                    >
                      <View style={styles.PersonList}>

                        <View
                          style={[
                            {
                              width: "100%",
                            },
                          ]}
                        >
                          <View
                            style={[
                              styles.Row,
                              {
                                width: "100%",
                              },
                            ]}
                          >
                            <Text
                              style={[
                                styles.PersonDetailsName,
                                {
                                  fontSize: 19,
                                },
                              ]}
                            >
                              {promotionTile}
                            </Text>
                          </View>
                        </View>

                      </View>
                    </TouchableOpacity>
                  );
                }}
                //Setting the number of column
                ListFooterComponent={this.renderFooter.bind(this)}
                numColumns={1}
                keyExtractor={(item, index) => {
                  return "key-" + index.toString();
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

export default DropDownScreen;