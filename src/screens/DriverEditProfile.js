import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ScrollView,
} from "react-native";
import styles, { ThemeColors } from "../styles/main.style";
import pagestyles from "../styles/welcome.style";

import { Actions } from "react-native-router-flux";

import { Button } from "../components";
import Styles from "../utils/CommonStyles";
import { Color, Fonts, Images } from "../utils";
import driverprofilestyle from "../styles/driverprofile.style";

const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "First Item",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Second Item",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Third Item",
  },
];

class DriverEditProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}

  onPressBtn = () => {};

  renderHeaderShop = () => {
    return (
      <View>
        <View style={styles.Header}>
          <View style={styles.HeaderLeft}>
            <TouchableOpacity
              style={{}}
              activeOpacity={0.5}
              onPress={() => {
                Actions.pop();
              }}
            >
              <Image source={Images.leftArrow} />
            </TouchableOpacity>
          </View>
          <View style={styles.HeaderMiddle}>
            <Text style={Styles.Heading}> Edit Profile</Text>
          </View>
          <View style={styles.HeaderRight}></View>
        </View>
      </View>
    );
  };
  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <SafeAreaView
          style={{
            flex: 0,
            backgroundColor: ThemeColors.primaryColor,
          }}
        />
        <SafeAreaView>
          {/* <Loader loading={this.state.apiLoader} /> */}
          <View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  {this.renderHeaderShop()}

                  <View>
                    <View style={styles.commonTBpadding}>
                      <View style={driverprofilestyle.profileico}>
                        <TouchableOpacity>
                          <Image
                            style={{ borderRadius: 40, width: 80, height: 80 }}
                            source={Images.firewater}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Name
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="John Smith"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Email
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="john@gmail.com"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Phone Number
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="01202 292831"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Address
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="89 COMMERCIAL ROAD"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          City/town
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="BOURNEMOUTH"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          County
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="UK"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Postcode
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="BH2 5RR"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Date of Birth
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="5 Jan 1996"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Mode of transport
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="Select Vchicle"
                        />
                        <TouchableOpacity
                          style={driverprofilestyle.driverinpico}
                        >
                          <Image source={Images.downarrow1} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Where do you like to work?{" "}
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="Select Location"
                        />
                        <TouchableOpacity
                          style={driverprofilestyle.driverinpico}
                        >
                          <Image source={Images.downarrow1} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Proof that you can legally work in the UK{" "}
                        </Text>
                      </View>
                      <View
                        style={[
                          driverprofilestyle.driverinputholder2,
                          styles.commonTBpadding,
                        ]}
                      >
                        <Text style={driverprofilestyle.inputfile}>
                          Choose File
                        </Text>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Proof of your Address
                        </Text>
                      </View>
                      <View
                        style={[
                          driverprofilestyle.driverinputholder2,
                          styles.commonTBpadding,
                        ]}
                      >
                        <Text style={driverprofilestyle.inputfile}>
                          Choose File
                        </Text>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Your driving licence
                        </Text>
                      </View>
                      <View
                        style={[
                          driverprofilestyle.driverinputholder2,
                          styles.commonTBpadding,
                        ]}
                      >
                        <Text style={driverprofilestyle.inputfile}>
                          Choose File
                        </Text>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Proof of food delivery insurance{" "}
                        </Text>
                      </View>
                      <View
                        style={[
                          driverprofilestyle.driverinputholder2,
                          styles.commonTBpadding,
                        ]}
                      >
                        <Text style={driverprofilestyle.inputfile}>
                          Choose File
                        </Text>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Do you have any unspent criminal convictions
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="Choose"
                        />
                        <TouchableOpacity
                          style={driverprofilestyle.driverinpico}
                        >
                          <Image source={Images.downarrow1} />
                        </TouchableOpacity>
                      </View>
                      <View style={{ marginTop: 5 }}>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          “We will require you to complete a criminal check
                          later in the process.”
                        </Text>
                      </View>
                    </View>
                    <View
                      style={[styles.commonTBpadding, { position: "relative" }]}
                    >
                      <View style={driverprofilestyle.line2}>
                        <Text style={{ lineHeight: 4 }}>&nbsp;</Text>
                      </View>
                      <View
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              fontSize: 16,
                              color: Color.orangeColor,
                              textAlign: "center",
                              backgroundColor: Color.greyApp,
                            },
                          ]}
                        >
                          Emergency contact{" "}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Name
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="Petter Max"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Relationship
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="Type your relationship"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          Phone Number
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="01276 686657"
                        />
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: "400",
                            },
                          ]}
                        >
                          How did you hear about us?{" "}
                        </Text>
                      </View>
                      <View style={{ position: "relative" }}>
                        <TextInput
                          style={driverprofilestyle.driverinput2}
                          value="Choose"
                        />
                        <TouchableOpacity
                          style={driverprofilestyle.driverinpico}
                        >
                          <Image source={Images.downarrow1} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View>
                      <View>
                        <Button
                          text={"Update"}
                          onLoading={false}
                          customStyles={
                            {
                              // mainContainer: styles.butonContainer
                            }
                          }
                          style={{ backgroundColor: Color.greenColor }}
                          onPress={() => Actions.manageaddrscreen()}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default DriverEditProfileScreen;
