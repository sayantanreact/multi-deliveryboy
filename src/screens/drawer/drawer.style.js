import { StyleSheet, PixelRatio, Dimensions } from 'react-native';
import { ThemeColors } from './main.style';
//import {Fonts} from '../utils/Fonts';
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

export default StyleSheet.create({
    Container:{
        paddingLeft: 15,
        paddingRight:15,
    },
    GreyBg:{
        backgroundColor: '#F3F3F3',
    },
    BlueBg:{
        backgroundColor: '#0F1B46',
        padding: 15,
    },
    Logo:{
        marginTop: 15,
        marginBottom: 15,
    },
    ProfileImg:{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
    },
    PicUpload:{
        marginTop: 5,
    },
    MenuListWrap:{
        backgroundColor: ThemeColors.whiteColor,
        paddingBottom: 15,
        paddingTop: 15,
    },
    Hello:{
        color: ThemeColors.whiteColor,
        fontSize: 16,
        marginLeft: 10,
    },
    MenuList:{
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        height: 42,
        marginBottom: 1,
        //backgroundColor: ThemeColors.lightgreyColor,
    },
    ListIcon:{
        marginRight:10,
        width: 30,
    },
    ListTitle:{
        color: ThemeColors.blackColor,
        fontSize: 16,
        textTransform: 'uppercase',
    },
    LinkText:{
        color: ThemeColors.darkgreyColor,
        fontSize: 14,
        marginLeft: 6,
        marginRight: 6,
    },
    ActionBtn:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 15,
    },
    MenuBottom:{
        backgroundColor: ThemeColors.lightgreyColor,
        paddingTop: 15,
        paddingBottom: 15,
    },
    DrawerMenu:{
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 15,
        paddingRight: 15,
        height: 34,
    },
    DrawerMenuName:{
        color: ThemeColors.blackColor,
        fontSize: 14, 
    },
    PowerbyWrap:{
        paddingLeft: 15,
        paddingRight: 15, 
        marginTop: 30,
    },
    Currency:{
        borderWidth: 1,
        borderColor: '#ccc',
        padding: 2,
        flexDirection: 'row',
    },

});