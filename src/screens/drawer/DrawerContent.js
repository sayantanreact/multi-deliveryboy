import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform, Text, View, Image, Alert,
  ActivityIndicator,
  TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView, Dimensions
} from 'react-native';
import styles, { ThemeColors } from '../../styles/main.style';
import pagestyles from '../../styles/drawer.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MainContainer } from '../../components';
import { Images, Dimen, Fonts, Color } from '../../utils';
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: 'transparent',
//     borderWidth: 2,
//     borderColor: 'red',
//   },
// });

class DrawerContent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    //sceneStyle: ViewPropTypes.style,
    title: PropTypes.string,
  };

  static contextTypes = {
    drawer: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      loginUserData: JSON.parse(global.loginUserData),
    };
  }
  callLogoutAlert() {
    Alert.alert('LOGOUT', 'Are You Sure?', [
      {
        text: 'YES',
        onPress: () => {
          this.logoutAction();
        },
      },
      { text: 'NO' },
    ]);
  }
  logoutAction = () => {
    AsyncStorage.removeItem('@multiDRIVERData')
      .then((value) => {
        //console.log('value:: ', value);
        global.loginUserData = '';
        // global.loginUserTokenType = '';
        // global.loginUserAccessToken = '';
        global.userType = '';
        Actions.reset('signinscreen');
      })
      .catch((error) => {
        //console.log(error);
        //alert('ERROR GETTING DATA FROM FACEBOOK')
      });
  };
  render() {
    const { loginUserData } = this.state;
    console.log(loginUserData);
    return (
      <MainContainer>
        <ScrollView>
          <View style={pagestyles.GreyBg}>
            <View style={[pagestyles.modalMain1]}>
              <View style={[pagestyles.leftModalContentHolder]}>
                <View>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      textTransform: 'capitalize',
                      padding: 15,
                      borderBottomWidth: 0.5,
                      borderStyle: 'solid',
                      borderColor: Color.bordergrey1,
                    }}
                  >
                    Hi, {loginUserData?.name}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    // Actions.reset('drawer')
                    Actions.drawerClose();
                  }}
                >
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: Color.whiteColor,
                      padding: 15,
                      position: 'relative',
                      borderBottomWidth: 0.3,
                      borderStyle: 'solid',
                      borderColor: Color.bordergrey1,
                    }}
                  >
                    <View style={{ marginRight: 15, width: 30 }}>
                      <Image source={Images.homedrawer} />
                    </View>
                    <View>
                      <Text>Home</Text>
                    </View>
                    <View style={pagestyles.clickarrow}>
                      <TouchableOpacity>
                        <Image source={Images.rightarrow} />
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                // onPress={() => {
                //   Actions.currentorderscreen();
                // }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      Actions.drawerClose();
                      Actions.pastorderscreen();
                    }}
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: Color.whiteColor,
                      padding: 15,
                      position: 'relative',
                      borderBottomWidth: 0.3,
                      borderStyle: 'solid',
                      borderColor: Color.bordergrey1,
                    }}
                  >
                    <View style={{ marginRight: 15, width: 30 }}>
                      <Image source={Images.termsico} />
                    </View>
                    <View>
                      <Text>Past Orders</Text>
                    </View>
                    <View style={pagestyles.clickarrow}>
                      <TouchableOpacity>
                        <Image source={Images.rightarrow} />
                      </TouchableOpacity>
                    </View>
                  </TouchableOpacity>
                  {
                    (loginUserData.venderID == "") &&
                    <TouchableOpacity
                      onPress={() => {
                        Actions.drawerClose();
                        Actions.reportScreen();
                      }}
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: Color.whiteColor,
                        padding: 15,
                        position: 'relative',
                        borderBottomWidth: 0.3,
                        borderStyle: 'solid',
                        borderColor: Color.bordergrey1,
                      }}
                    >
                      <View style={{ marginRight: 15, width: 30 }}>
                        <Image source={Images.termsico} />
                      </View>
                      <View>
                        <Text>Report</Text>
                      </View>
                      <View style={pagestyles.clickarrow}>
                        <TouchableOpacity>
                          <Image source={Images.rightarrow} />
                        </TouchableOpacity>
                      </View>
                    </TouchableOpacity>
                  }
                  <TouchableOpacity
                    onPress={() => {
                      Actions.drawerClose();
                      Actions.notification();
                    }}
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: Color.whiteColor,
                      padding: 15,
                      position: 'relative',
                      borderBottomWidth: 0.3,
                      borderStyle: 'solid',
                      borderColor: Color.bordergrey1,
                    }}
                  >
                    <View style={{ marginRight: 15, width: 30 }}>
                      <Image source={Images.termsico} />
                    </View>
                    <View>
                      <Text>Notifications</Text>
                    </View>
                    <View style={pagestyles.clickarrow}>
                      <TouchableOpacity>
                        <Image source={Images.rightarrow} />
                      </TouchableOpacity>
                    </View>
                  </TouchableOpacity>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.callLogoutAlert();
                  }}
                >
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: Color.whiteColor,
                      padding: 15,
                      position: 'relative',
                      borderBottomWidth: 0.3,
                      borderStyle: 'solid',
                      borderColor: Color.bordergrey1,
                    }}
                  >
                    <View style={{ marginRight: 15, width: 30 }}>
                      <Image source={Images.logout} />
                    </View>
                    <View>
                      <Text>Logout</Text>
                    </View>
                    <View style={pagestyles.clickarrow}>
                      <Image source={Images.rightarrow} />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </MainContainer>
    );
  }
}

export default DrawerContent;