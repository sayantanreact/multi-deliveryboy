import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  FlatList,
  Keyboard,
  ScrollView,
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import loginpage from '../styles/login.style';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { Actions } from 'react-native-router-flux';
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import {
  MainContainer,
  AuthInputBoxSec,
  Button,
  TopHeader,
} from '../components';
import { Images } from '../utils';
import { SignupService } from '../services';

// import { FlushMsg } from '../utils'

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      phone: this.props.isPage == 'login' ? this.props.phoneNo : '',
      icEye: 'visibility-off',
      showPassword: false,
    };
  }
  componentDidMount() {}

  signupOnPressBtn = () => {
    this.signupAction();
  };

  signupAction = () => {
    if (this.state.name == '') {
      FlushMsg.showError('Please enter your name');
    } else if (this.state.email == '') {
      FlushMsg.showError('Please enter your email');
    } else if (this.state.password == '') {
      FlushMsg.showError('Please enter your password');
    } else if (this.state.phone == '') {
      FlushMsg.showError('Please Enter Your phone Number');
    } else if (!this.state.phone.startsWith(0)) {
      FlushMsg.showError('Please Add 0 before phone Number');
    } else {
      Keyboard.dismiss();
      this.signupApi();
    }
  };

  changePwdType = () => {
    let newState;
    if (this.state.showPassword) {
      newState = {
        icEye: 'visibility',
        showPassword: false,
        password: this.state.password,
      };
    } else {
      newState = {
        icEye: 'visibility-off',
        showPassword: true,
        password: this.state.password,
      };
    }
    this.setState(newState);
  };
  handlePassword = (password) => {
    let newState = {
      icEye: this.state.icEye,
      showPassword: this.state.showPassword,
      password: password,
    };
    this.setState(newState);
    this.props.callback(password);
  };
  signupApi = () => {
    this.loaderShowHide(true);
    var myData = {
      name: this.state.name,
      phone_no:this.state.phone,
      email: this.state.email,
      password: this.state.password,
      terms_and_policy: '1',
    };
    SignupService.signupDriver(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          Actions.otpscreen({
            data: result.data,
            isPage: 'signup',
          });
        } else {
          FlushMsg.showError(result.message);
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };

  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };
  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <MainContainer>
          {/* <Loader loading={this.state.apiLoader} /> */}
          <View>
            <ScrollView
              keyboardShouldPersistTaps={'handled'}
              showsVerticalScrollIndicator={false}
            >
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  <TopHeader
                    title={' Sign up'}
                    onPress={() => {
                      Actions.pop();
                    }}
                  />
                  <View
                    style={
                      {
                        // marginTop: 10
                      }
                    }
                  >
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        // icon={layer9_icon}
                        lableText={'Name'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.nameInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ name: text })}
                        value={this.state.name}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.phoneInput.focus();
                        }}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        inputLayout={{
                          flexDirection: 'row',
                        }}
                        inputFieldStyle={{
                          width: '100%',
                        }}
                        // showCode={true}
                        // icon={layer9_icon}
                        lableText={'Phone No'}
                        keyboardType={'number-pad'}
                        inputRef={(ref) => {
                          this.phoneInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ phone: text })}
                        value={this.state.phone}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.emailInput.focus();
                        }}
                        maxLength={11}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        // icon={layer9_icon}
                        lableText={'Email'}
                        keyboardType={'email-address'}
                        inputRef={(ref) => {
                          this.emailInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ email: text })}
                        value={this.state.email}
                        caretHidden={false}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.passwordInput.focus();
                        }}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        // icon={layer9_icon}
                        lableText={'Password'}
                        disableImg={true}
                        iconName={this.state.icEye}
                        secureTextEntry={this.state.showPassword ? false : true}
                        // value={firstName}
                        // error={firstNameError}
                        // imgStyle={{ marginLeft: wp(4) }}
                        // inputFieldStyle={{ marginLeft: wp(7) }}
                        iconPressAction={this.changePwdType}
                        inputRef={(ref) => {
                          this.passwordInput = ref;
                        }}
                        // refs={this.passwordInput}
                        onChangeText={(text) =>
                          this.setState({ password: text })
                        }
                        value={this.state.password}
                        autoCapitalize="none"
                        returnKeyType="done"
                        onSubmitEditing={() => {
                          this.signupOnPressBtn();
                        }}
                      />
                      {/* <Icon
                                                style={{
                                                    position: 'absolute',
                                                    top: '50%',
                                                    right: 0,
                                                    color: "#7C7C7C"
                                                }}
                                                name={this.state.icEye}
                                                size={25}
                                                onPress={this.changePwdType}
                                            /> */}
                    </View>
                  </View>
                  <View style={loginpage.conditiontextView}>
                    <Text style={loginpage.insideText}>
                      By continuing, you agree to our
                    </Text>
                    <Text
                      onPress={() => {
                        // Actions.termsconditionscreen();
                        Actions.cmsscreen({
                          title: 'terms-conditions',
                          heading: 'Terms & Conditions',
                        });
                      }}
                      style={loginpage.termText}
                    >
                      Terms &
                    </Text>
                  </View>
                  <View style={loginpage.secView}>
                    <Text style={loginpage.serviceText}>Conditions</Text>
                    <Text style={loginpage.andText}>and</Text>
                    <Text
                      onPress={() => {
                        // this.setState({
                        //   privacypolicyVisible: true,
                        // });
                        Actions.cmsscreen({
                          title: 'privacy-policy',
                          heading: 'Privacy Policy',
                        });
                      }}
                      style={loginpage.policyText}
                    >
                      Privacy Policy.
                    </Text>
                  </View>
                  <View style={{ marginTop: 20 }}>
                    <Button
                      text={'Sign up'}
                      onLoading={this.state.loading}
                      customStyles={
                        {
                          // mainContainer: styles.butonContainer
                        }
                      }
                      onPress={() => this.signupOnPressBtn()}
                    />
                  </View>
                  <View style={loginpage.bottomSection}>
                    <View>
                      <Text style={loginpage.Alreadytext}>
                        Already have an account ?
                      </Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => {
                        Actions.pop();
                      }}
                    >
                      <Text style={loginpage.Siguptext}> Log in</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </MainContainer>
      </>
    );
  }
}

export default SignupScreen;
