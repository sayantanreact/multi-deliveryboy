
import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert, Switch,
    ActivityIndicator, Modal, ImageBackground,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView, PermissionsAndroid
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import driverdashboardstyle from '../styles/driverdashboard.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';

// import { FlushMsg } from '../utils'

class DriverDashboard4Screen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}> 
                    <TouchableOpacity
                            style={{}}
                            activeOpacity={0.5}
                            onPress={() => {
                                Actions.pop()
                            }}
                        >
                            <Image
                                source={Images.leftArrow}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.HeaderMiddle}> 
                           <Text style={Styles.Heading}>Home</Text>
                    </View>
                    <View style={styles.HeaderRight}> 
                           <Text>&nbsp; </Text>
                    </View>
                </View> 
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                        {this.renderHeaderShop()}
                            <View style={[driverdashboardstyle.dashboardTop,{width: Dimen.width,}]}>
                                            <ImageBackground
                                                source={Images.homebg} resizeMode="cover"
                                                style={{
                                                    width: '100%',
                                                    height: '100%'
                                                }}
                                            >
                                                <View style={driverdashboardstyle.profileico}>
                                                    <Image source={Images.avatar}
                                                        style={{
                                                            width: 84,
                                                            height: 84,
                                                            padding: 5
                                                        }}
                                                        resizeMode={'cover'}
                                                    />
                                                </View>
                                                <View style={{  marginTop: 10, }}>
                                                    <Text style={driverdashboardstyle.dashTopText}>John Smith</Text>
                                                </View>
                                                <View>
                                                    <Text style={driverdashboardstyle.dashTopText1}>Delivery Boy</Text>
                                                </View>
                                            </ImageBackground>
                                        </View>
                                        <View style={[driverdashboardstyle.activebox,{width: Dimen.width - 30,}]}>
                                            <View>
                                                <Text style={driverdashboardstyle.activetext}>Active</Text>
                                            </View>
                                            <View>
                                                <Switch
                                                    trackColor={{ false: "#ddd", true: "#DAFFDF" }}
                                                    //thumbColor={isEnabledAccountActivation ? "#28D140" : "#999"}
                                                    ios_backgroundColor="#28D140"
                                                    // onValueChange={() => {
                                                    //     this.setState({
                                                    //         isEnabledAccountActivation: !isEnabledAccountActivation
                                                    //     })
                                                    // }}
                                                    value={true}
                                                    style={{
                                                        alignSelf: 'flex-end',
                                                        marginTop: 4,
                                                        marginRight: 12,
                                                        transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }]
                                                    }}
                                                />
                                            </View>
                                        </View> 
                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}> 
                                    <View style={driverdashboardstyle.dashboardcontentholder}> 
                                        <View style={driverdashboardstyle.contentdiv}>
                                            <View style={driverdashboardstyle.contentleft}>
                                                <View>
                                                    <Text style={driverdashboardstyle.dashhead1}>Order #11253</Text>
                                                </View>
                                                <View style={{flexDirection:'row'}}>
                                                    <Text style={driverdashboardstyle.dashtext}>Payment : </Text><Text style={[driverdashboardstyle.dashtext,{color:Color.greenColor}]}>Net Banking</Text>
                                                </View>
                                                <View>
                                                    <Text style={driverdashboardstyle.dashtext}>Order Assigned 20 min ago</Text>
                                                </View>
                                            </View>
                                            <View style={driverdashboardstyle.contentRight}>
                                                <View>
                                                    <Text style={[driverdashboardstyle.dashhead1,{color:Color.orangeColor}]}>$120</Text>
                                                </View>
                                            </View>
                                        </View> 
                                        <View style={[driverdashboardstyle.contentdiv,{borderTopWidth:1, borderColor:Color.grayDash}]}>
                                            <View style={driverdashboardstyle.contentleft}>
                                                <View>
                                                    <Text style={driverdashboardstyle.dashhead2}>Delivery Address</Text>
                                                </View> 
                                                <View>
                                                    <Text style={driverdashboardstyle.dashhead1}>DANIEL REX</Text>
                                                </View> 
                                                <View>
                                                    <Text style={driverdashboardstyle.dashtext}>Delivery Address : </Text>
                                                </View>
                                                <View>
                                                    <Text style={driverdashboardstyle.dashtext}>THE BUSINESS CENTRE, 61 WELLFIELD ROAD, UK CF24 3DG</Text>
                                                </View>
                                            </View> 
                                        </View> 
                                        <View style={{ marginTop: 20,width:"100%" }}>
                                            <Button
                                               style={[driverdashboardstyle.signSubmit]}
                                                text={'Delivered'}
                                                onLoading={false} 
                                                onPress={() => {
                                                    Actions.driverorderdeliveredscreen();
                                                }}
                                               
                                            />
                                        </View>
                                        {/* <View>
                                            <TextInput  style={[orderconfirmstyle.signSubmit]}
                                                    value="Check order status" 
                                                /> 
                                        </View> */}
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default DriverDashboard4Screen; 