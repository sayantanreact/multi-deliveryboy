import React, { Component } from "react";
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  FlatList,
  Keyboard,
  ScrollView,
} from "react-native";
import styles, { ThemeColors } from "../styles/main.style";
import loginpage from "../styles/login.style";
import pagestyles from "../styles/welcome.style";

import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/MaterialIcons";
import FlushMsg from "../utils/FlushMsg";
import { LoginService } from "../services";
import Loader from "../utils/Loader";
import { MainContainer, AuthInputBoxSec, Button } from "../components";
import { Images } from "../utils";
import Fonts, { fonts, fontSizes } from "../utils/Fonts";
import { CountrySelector } from "../components/CountrySelector";
import AsyncStorage from "@react-native-async-storage/async-storage";

// import { FlushMsg } from '../utils'

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      icEye: "visibility-off",
      showPassword: false,
    };
  }
  componentDidMount() { }

  loginBtn = () => {
    this.loginAction();
  };

  loginAction = () => {
    if (this.state.email == "") {
      FlushMsg.showError("Please enter your email");
    } else if (this.state.password == "") {
      FlushMsg.showError("Please enter your password");
    } else {
      Keyboard.dismiss();
      this.loginApi();
    }
  };

  loginApi = () => {
    this.loaderShowHide(true);
    var myData = {
      username: this.state.email,
      password: this.state.password,
    };
    LoginService.loginDriver(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          global.loginUserData = JSON.stringify(result.data);
          if (result.data.work_in_uk_proof == "" && result.data.venderID == "") {
            //if (result.data.work_in_uk_proof == "") {
            Actions.reset('driverprofile', {
              isPage: 'detail',
            });
          } else {
            AsyncStorage.setItem("@multiDRIVERData", global.loginUserData);
            AsyncStorage.getItem("@multiDRIVERData")
              .then((value) => {
                // //console.log('value:: ', value);
                //console.log('loginUserToken:: ', global.loginUserToken);
                global.loginUserData = value;
                Actions.reset("drawer");
              })
              .catch((error) => {
                //console.log(error);
                //alert('ERROR GETTING DATA FROM FACEBOOK')
              });
          }


        } else {
          FlushMsg.showError(result.message);
        }
        console.log("result:: ", result);
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this)
    );
  };

  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };

  changePwdType = () => {
    let newState;
    if (!this.state.showPassword) {
      newState = {
        icEye: "visibility",
        showPassword: true,
        password: this.state.password,
      };
    } else {
      newState = {
        icEye: "visibility-off",
        showPassword: false,
        password: this.state.password,
      };
    }
    this.setState(newState);
  };
  handlePassword = (password) => {
    let newState = {
      icEye: this.state.icEye,
      showPassword: this.state.showPassword,
      password: password,
    };
    this.setState(newState);
    this.props.callback(password);
  };

  renderHeaderShop = () => {
    return (
      <View>
        <View style={loginpage.mainView}>
          <View style={[styles.HeaderLeft, styles.HeaderLeftBackButton]}>
            <TouchableOpacity
              style={{}}
              activeOpacity={0.5}
              onPress={() => {
                Actions.pop();
              }}
            >
              <Image source={Images.leftArrow} />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={loginpage.headingtext}>Login</Text>
          </View>
        </View>
      </View>
    );
  };
  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <MainContainer>
          {/* <Loader loading={this.state.apiLoader} /> */}
          <View>
            <ScrollView
              keyboardShouldPersistTaps={"handled"}
              showsVerticalScrollIndicator={false}
            >
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  {this.renderHeaderShop()}
                  <View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: "100%",
                        }}
                        // icon={layer9_icon}
                        // name={'Email'}
                        lableText={"Email"}
                        keyboardType={"email-address"}
                        inputRef={(ref) => {
                          this.emailInput = ref;
                        }}
                        caretHidden={false}
                        onChangeText={(text) => this.setState({ email: text })}
                        value={this.state.email}
                        autoCapitalize="none"
                        returnKeyType="next"
                        caretHidden={false}
                        onSubmitEditing={() => this.passwordInput.focus()}
                        // error={firstNameError}
                        // imgStyle={{ marginLeft: wp(4) }}
                        // inputFieldStyle={{ marginLeft: wp(7) }}
                        maxLength={60}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: "100%",
                        }}
                        // icon={layer9_icon}
                        lableText={"Password"}
                        disableImg={true}
                        iconName={this.state.icEye}
                        inputRef={(ref) => {
                          this.passwordInput = ref;
                        }}
                        // refs={this.passwordInput}
                        secureTextEntry={this.state.showPassword ? false : true}
                        onChangeText={(text) =>
                          this.setState({ password: text })
                        }
                        value={this.state.password}
                        autoCapitalize="none"
                        returnKeyType="done"
                        onSubmitEditing={() => {
                          this.loginBtn();
                        }}
                        // value={firstName}
                        // error={firstNameError}
                        // imgStyle={{ marginLeft: wp(4) }}
                        // inputFieldStyle={{ marginLeft: wp(7) }}
                        iconPressAction={this.changePwdType}
                      />
                      {/* <Icon
                                                style={{
                                                    position: 'absolute',
                                                    top: '50%',
                                                    right: 0,
                                                    color: "#7C7C7C"
                                                }}
                                                name={this.state.icEye}
                                                size={25}
                                                onPress={this.changePwdType}
                                            /> */}
                    </View>
                  </View>

                  <View style={loginpage.forgotpass}>
                    <Text style={loginpage.forgotText}>Forgot Password?</Text>
                  </View>
                  <View style={{ marginTop: 20 }}>
                    <Button
                      text={"Login"}
                      text={"Login"}
                      onLoading={this.state.loading}
                      customStyles={
                        {
                          // mainContainer: styles.butonContainer
                        }
                      }
                      onPress={() => this.loginBtn()}
                    />
                  </View>
                  <View style={loginpage.bottomSection}>
                    <View>
                      <Text style={loginpage.Alreadytext}>
                        Don't have an account?
                      </Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => {
                        Actions.signupscreen();
                      }}
                    >
                      <Text style={loginpage.Siguptext}> Sign up</Text>
                    </TouchableOpacity>
                  </View>

                  {/* <View style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        backgroundColor: "white",
                                    }}>

                                        <CountrySelector visible={this.state.countrySelector} handleSelectedEvent={this.handleSelectedEvent} />
                                    </View> */}
                </View>
              </View>
            </ScrollView>
          </View>
        </MainContainer>
      </>
    );
  }
}

export default LoginScreen;
