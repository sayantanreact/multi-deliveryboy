import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  ActivityIndicator,
  CheckBox,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  FlatList,
  Keyboard,
  ScrollView,
} from 'react-native';
import { Modal } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import vendororderstyle from '../styles/vendororder.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import {
  AuthInputBoxSec,
  Button,
  TopHeader,
  ImageCustom,
} from '../components';
import {
  Images,
  Dimen,
  Fonts,
  Color,
  GlobalFunction,
  CommonStyles,
} from '../utils';
import Styles from '../utils/CommonStyles';
import OrderService from '../services/OrderService';
import FooterListPagination from '../components/FooterListPagination';
// import { FlushMsg } from '../utils'

class PastOrderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
      loginUserData: JSON.parse(global.loginUserData),
      apiLoding: false,
      loading: true,
      pastorderList: [],
      ended: false,
    };
  }
  componentDidMount() {
    // const { navigation } = this.props;
    // this.focusListener = navigation.addListener("didFocus", () => {
    //     // The screen is focused
    //     // Call any action
    //     this.currentOrderListApi()
    // });
    this.pastOrderListApi(true);
  }

  pastOrderListApi = (loadingApi) => {
    this.loaderShowHide(loadingApi);
    var myData = {
      driverID: this.state.loginUserData.driverID,
      //"customerID": "5"
    };
    console.log(myData);
    OrderService.pastOrderApi(myData).then(
      function (result) {
        console.log('Your data: ' + JSON.stringify(result));
        this.loaderShowHide(false);
        if (result.status) {
          if (result.data.length > 0) {
            this.setState(
              {
                pastorderList: [...this.state.pastorderList, ...result.data],
                //totalCount: result.data.totalcount,
                loading: false,
                ended: false,
                // imagePath: result.data?.path,
              },
              () => {
                console.log('pastorderList: ', this.state.pastorderList);
                // console.log('orderList: ', this.state.orderslistitem);
              },
            );
          } else {
            this.setState({
              pastorderList: [...this.state.pastorderList, ...result.data],
              //totalCount: result.data.totalcount,
              loading: false,
              ended: true,
              // imagePath: result.data?.path,
            });
          }
        } else {
          //FlushMsg.showError(result.message);
          this.setState({
            //totalCount: result.data.totalcount,
            loading: false,
            ended: true,
            // imagePath: result.data?.path,
          });
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };

  //   acceptOrderApi = (item, index) => {
  //     this.loaderShowHideApi(true);
  //     var myData = {
  //       orderID: item?.orderID,
  //       venderID: this.state.loginUserData.venderID,
  //       status: '13',
  //     };
  //     console.log(myData);
  //     OrderService.updateOrderStatus(myData).then(
  //       function (result) {
  //         console.log('Your data: ' + JSON.stringify(result));
  //         this.loaderShowHideApi(false);
  //         if (result.status) {
  //           FlushMsg.showSuccess(result.message);
  //           var array = [...this.state.currentorderList]; // make a separate copy of the array
  //           //array.splice(index, 1);
  //           array[index] = result.data;
  //           this.setState({
  //             currentorderList: array,
  //             ended: true,
  //           });
  //         } else {
  //           FlushMsg.showError(result.message);
  //         }
  //         console.log('result:: ', result);
  //       }.bind(this),
  //       function (result) {
  //         console.log('result:: ', result);
  //         //console.log('There was an error fetching the time');
  //         this.loaderShowHideApi(false);
  //         FlushMsg.showError(result.message);
  //       }.bind(this),
  //     );
  //   };

  //   callrejectOrderAlert(item, index) {
  //     Alert.alert(
  //       'REJECT ORDER!',
  //       'Are you sure you want to reject this order?',
  //       [
  //         {
  //           text: 'YES',
  //           onPress: () => {
  //             this.rejectOrderApi(item, index);
  //           },
  //         },
  //         { text: 'NO' },
  //       ],
  //     );
  //   }

  //   rejectOrderApi = (item, index) => {
  //     this.loaderShowHideApi(true);
  //     var myData = {
  //       orderID: item?.orderID,
  //       venderID: this.state.loginUserData.venderID,
  //       status: '12',
  //     };
  //     console.log(myData);
  //     OrderService.updateOrderStatus(myData).then(
  //       function (result) {
  //         console.log('Your data: ' + JSON.stringify(result));
  //         this.loaderShowHideApi(false);
  //         if (result.status) {
  //           FlushMsg.showSuccess(result.message);
  //           var array = [...this.state.currentorderList]; // make a separate copy of the array
  //           array.splice(index, 1);
  //           this.setState({
  //             currentorderList: array,
  //             ended: true,
  //           });
  //         } else {
  //           FlushMsg.showError(result.message);
  //         }
  //         console.log('result:: ', result);
  //       }.bind(this),
  //       function (result) {
  //         console.log('result:: ', result);
  //         //console.log('There was an error fetching the time');
  //         this.loaderShowHideApi(false);
  //         FlushMsg.showError(result.message);
  //       }.bind(this),
  //     );
  //   };

  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };

  loaderShowHideApi = (status) => {
    this.setState({
      apiLoding: status,
    });
  };

  onPressBtn = () => {
    FlushMsg.showSuccess('hiiiii');
  };

  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    const { loginUserData } = this.state
    return (
      <>
        <SafeAreaView
          style={{
            flex: 0,
            backgroundColor: ThemeColors.primaryColor,
          }}
        />
        <SafeAreaView>
          <Loader loading={this.state.apiLoding} />
          <View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  <TopHeader
                    title={'Past Orders'}
                    onPress={() => {
                      Actions.pop();
                    }}
                  />

                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.pastorderList}
                    keyExtractor={(item, index) => {
                      return 'key-' + index.toString();
                    }}
                    renderItem={({ item, index }) => {

                      return (
                        <TouchableOpacity
                          onPress={() => {
                            Actions.orderdetailscreen({
                              item: item,
                            });
                          }}
                        >
                          <View
                            style={[
                              {
                                borderTopWidth: 0.5,
                                borderColor: Color.lightBlack,
                              },
                            ]}
                          >
                            <View style={[CommonStyles.commonTBpadding]}>
                              <View style={vendororderstyle.orderproduct}>
                                <View
                                  style={[
                                    vendororderstyle.orderImg,
                                    { marginRight: 10 },
                                  ]}
                                >
                                  <ImageCustom
                                    imageUri={item?.pickup_address.image}
                                    customStyles={[
                                      {
                                        width: 65,
                                        height: 65,
                                        borderRadius: 5,
                                        overflow: 'hidden',
                                      },
                                    ]}
                                  />
                                </View>
                                <View style={vendororderstyle.ordercontent}>
                                  <View>
                                    <Text
                                      style={[
                                        vendororderstyle.orderheading1,
                                        styles.mb_5,
                                      ]}
                                    >
                                      {item?.store_type == 'G'
                                        ? item?.pickup_address.store_name
                                        : item?.pickup_address.restaurant_name}
                                    </Text>
                                  </View>
                                  <View>
                                    <Text
                                      style={[vendororderstyle.orderheading2]}
                                    >
                                      Order ID: {item?.order_number}{' '}
                                    </Text>
                                  </View>
                                  {
                                    (loginUserData.venderID == "") &&
                                    <View
                                    //style={vendororderstyle.orderprice}
                                    >
                                      <Text>You Earned: £{item?.delivery_charge} Gross, £{item?.driver_earning} Net</Text>
                                    </View>
                                  }
                                  {/* <View>
                                    <Text style={vendororderstyle.ordertext}>
                                      {GlobalFunction.formatDateTime(
                                        item?.order_date,
                                      )}
                                    </Text>
                                  </View> */}
                                </View>
                              </View>

                            </View>
                            <View>
                              <View>
                                <Text
                                  style={[
                                    vendororderstyle.ordertext,
                                    styles.mb_5,
                                  ]}
                                >
                                  Items
                                </Text>
                              </View>
                              {item.order_items.map((data) => {
                                return (
                                  <View>
                                    <Text
                                      style={[
                                        vendororderstyle.orderheading1,
                                        styles.mb_5,
                                      ]}
                                    >
                                      {data.quantity} * {data.item_name}
                                    </Text>
                                  </View>
                                );
                              })}
                            </View>
                            <View>
                              <View>
                                <Text
                                  style={[
                                    vendororderstyle.ordertext,
                                    styles.mb_5,
                                  ]}
                                >
                                  Order Status
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={[
                                    vendororderstyle.orderheading1,
                                    styles.mb_5,
                                  ]}
                                >
                                  {item?.order_status_name}
                                </Text>
                              </View>
                            </View>
                            <View>
                              <View>
                                <Text
                                  style={[
                                    vendororderstyle.ordertext,
                                    styles.mb_5,
                                  ]}
                                >
                                  Order Date
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={[
                                    vendororderstyle.orderheading1,
                                    styles.mb_5,
                                  ]}
                                >
                                  {GlobalFunction.formatDateTime(
                                    item?.order_date,
                                  )}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </TouchableOpacity>
                      );
                    }}
                    keyExtractor={(item) => item?._id}
                    //ListFooterComponent={this.renderFooter.bind(this)}
                    ListFooterComponent={
                      <FooterListPagination
                        listDataLength={this.state.pastorderList.length}
                        ended={this.state.ended}
                        removeItem={this.state.removeItem}
                        loading={this.state.loading}
                      />
                    }
                    keyExtractor={(item, index) => {
                      return 'key-' + index.toString();
                    }}
                    scrollEventThrottle={400}
                  />
                </View>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}
export default PastOrderScreen;
