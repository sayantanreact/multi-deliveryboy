import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  ActivityIndicator,
  CheckBox,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  FlatList,
  Keyboard,
  ScrollView,
} from 'react-native';
import { Modal } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import notificationstyle from '../styles/notification.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
import {
  AuthInputBoxSec,
  Button,
  MainContainer,
  TopHeader,
} from '../components';
import { Images, Dimen, Fonts, Color, GlobalFunction } from '../utils';
import Styles from '../utils/CommonStyles';
import { NotificationService } from '../services';
import FooterListPagination from '../components/FooterListPagination';

// import { FlushMsg } from '../utils'

class NotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
      loginUserData: JSON.parse(global.loginUserData),
      apiLoding: false,
      loading: false,
      notilist: [],
      ended: false,
    };
  }
  componentDidMount() {
    this.getNotificationApi();
  }
  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };
  loaderShowHideApi = (status) => {
    this.setState({
      apiLoding: status,
    });
  };
  loaderShowHideeApi = (status) => {
    this.setState({
      loaading: status,
    });
  };
  getNotificationApi = () => {
    this.loaderShowHide(true);
    var myData = {
      userID: this.state.loginUserData.driverID,
      key: '3',
    };
    console.log(myData);
    NotificationService.getNotification(myData).then(
      function (result) {
        console.log('Your data: ' + JSON.stringify(result));
        this.loaderShowHide(false);
        if (result.status) {
          if (result.data.length > 0) {
            this.setState(
              {
                notilist: [...this.state.notilist, ...result.data],
                // totalCount: result.data.totalcount,
                loading: false,
                ended: false,
              },
              () => {
                console.log('notilist: ', this.state.notilist);
              },
            );
          } else {
            this.setState({
              notilist: [...this.state.notilist, ...result.data],
              //   totalCount: result.data.totalcount,
              loading: false,
              ended: true,
            });
          }
        } else {
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };
  onPressBtn = () => {
    FlushMsg.showSuccess('hiiiii');
  };

  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <MainContainer>
          {/* <Loader loading={this.state.loading} /> */}
          <View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  <TopHeader
                    title={'Notification'}
                    onPress={() => {
                      Actions.pop();
                    }}
                  />
                  <View>
                    <FlatList
                      style={{
                        marginBottom: 70,
                      }}
                      showsVerticalScrollIndicator={false}
                      // data={reselleruserList}
                      data={this.state.notilist}
                      // ListHeaderComponent={this.renderHeaderShop.bind(this)}
                      keyExtractor={(item, index) => {
                        return 'key-' + index.toString();
                      }}
                      renderItem={({ item, index }) => {
                        return (
                          <View
                            style={[
                              notificationstyle.notificationlist,
                              styles.mb_15,
                              styles.commonPadding,
                              styles.brd5,
                              {
                                backgroundColor: Color.whiteColor,
                                marginTop: 20
                              },
                            ]}
                          >
                            <View
                              style={[notificationstyle.ntflex1, styles.mb_15]}
                            >
                              <View>
                                <View>
                                  <Text
                                    style={[
                                      notificationstyle.nthead1,
                                      { marginBottom: 5 },
                                    ]}
                                  >
                                    {item?.subject}
                                  </Text>
                                </View>
                              </View>

                            </View>
                            <TouchableOpacity
                              // onPress={() => {
                              //   Actions.paymentstatusscreen();
                              // }}
                              style={{
                                marginTop: 10
                              }}
                            >
                              <Text
                                style={[
                                  notificationstyle.nttext1,
                                  { color: Color.greenColor },
                                ]}
                              >
                                {item?.message}
                              </Text>
                            </TouchableOpacity>
                            <View style={{
                              marginTop: 15
                            }}>
                              <View>
                                <Text style={notificationstyle.nttext}>
                                  {GlobalFunction.formatDateTime(
                                    item?.create_date,
                                  )}
                                </Text>
                              </View>
                            </View>
                          </View>
                        );
                      }}
                      keyExtractor={(item) => item?.notificationID}
                      // ListFooterComponent={this.renderFooter.bind(this)}
                      ListFooterComponent={
                        <FooterListPagination
                          listDataLength={this.state.notilist.length}
                          ended={this.state.ended}
                          removeItem={this.state.removeItem}
                          loading={this.state.loading}
                        />
                      }
                      keyExtractor={(item, index) => {
                        return 'key-' + index.toString();
                      }}
                      scrollEventThrottle={400}
                    />
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </MainContainer>
      </>
    );
  }
}

export default NotificationScreen;
