import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { ThemeColors } from "../styles/main.style";
import pagestyles from "../styles/welcome.style";
import { Actions } from "react-native-router-flux";
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from "../utils/FlushMsg";
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from "../components";

// import { FlushMsg } from '../utils'

class WelcomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}

  onPressBtn = () => {
    FlushMsg.showSuccess("hiiiii");
  };
  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <SafeAreaView
          style={{
            flex: 0,
            backgroundColor: ThemeColors.primaryColor,
          }}
        />
        <SafeAreaView>
          {/* <Loader loading={this.state.apiLoader} /> */}
          <View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  <TouchableOpacity
                    style={pagestyles.BigButton}
                    activeOpacity={0.7}
                    onPress={() => {
                      Actions.reset("tabsLog");
                    }}
                  >
                    <Text style={pagestyles.BigButtonText}>Lets Start</Text>
                  </TouchableOpacity>
                  <Button
                    text={"Sign In"}
                    onLoading={false}
                    customStyles={
                      {
                        //mainContainer: styles.butonContainer
                      }
                    }
                    onPress={() => this.onPressBtn()}
                  />
                  <View>
                    <AuthInputBoxSec
                      // icon={layer9_icon}
                      name={"hello"}
                      // value={firstName}
                      // error={firstNameError}
                      // imgStyle={{ marginLeft: wp(4) }}
                      // inputFieldStyle={{ marginLeft: wp(7) }}
                      maxLength={60}
                    />
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default WelcomeScreen;
