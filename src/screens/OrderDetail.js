import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator, CheckBox,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import { Modal } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import orderdetailsstyle from '../styles/orderdetails.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button, MainContainer, TopHeader } from '../components'
import { Images, Dimen, Fonts, Color, GlobalFunction } from '../utils'
import Styles from '../utils/CommonStyles';
import { ProductService } from '../services';
import OrderService from '../services/OrderService';
import Moment from "moment-timezone";
// import { FlushMsg } from '../utils'

class OrderDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: true,
            orderdetailsList: {},
            apiLoader: false,
            apiLoding: false,
            loginUserData: JSON.parse(global.loginUserData),
            page: 0,
            totalCount: 0,
            loading: true,
            ended: false,
        };
    }
    componentDidMount() {
        this.orderDetalLsitApi(true)
    }
    orderDetalLsitApi = (loadingApi) => {
        this.loaderShowHide(loadingApi);
        var myData = {
            "orderID": this.props.item.orderID,
        };
        console.log(myData)
        OrderService.getOrderDetail(myData).then(
            function (result) {
                //Alert.alert(JSON.stringify(result))
                console.log('Your data: ' + JSON.stringify(result));
                this.loaderShowHide(false);
                if (result.status) {
                    if (result.data.length > 0) {
                        this.setState(
                            {
                                orderdetailsList: result.data,
                                totalCount: result.data.totalcount,
                                loading: false,
                                ended: false,

                                // imagePath: result.data?.path,
                            },
                            () => {
                                console.log('orderdetailsList: ', this.state.orderdetailsList);
                            },
                        )
                    } else {
                        this.setState({
                            orderdetailsList: result.data,
                            totalCount: result.data.totalcount,
                            loading: false,
                            ended: true,
                            // imagePath: result.data?.path,
                        })
                    }
                } else {
                    //FlushMsg.showError(result.message);
                }
                console.log('result:: ', result)
            }.bind(this),
            function (result) {
                console.log('result:: ', result)
                //console.log('There was an error fetching the time');
                this.loaderShowHide(false);
                FlushMsg.showError(result.message);
            }.bind(this)
        )

    };
    loaderShowHide = (status) => {
        this.setState({
            apiLoader: status,
        });
    };
    loaderShowHideApi = (status) => {
        this.setState({
            apiLoading: status,
        });
    };
    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }
    // reOrderApi = (item) => {
    //     this.loaderShowHideApi(true);
    //     var myData = {
    //         "orderID": item?.orderID
    //         //"customerID": "5"
    //     };
    //     console.log(myData)
    //     OrderService.reOrder(myData).then(
    //         function (result) {
    //             console.log('Your data: ' + JSON.stringify(result));
    //             this.loaderShowHideApi(false);
    //             if (result.status) {
    //                 Actions.tab_2();
    //             } else {
    //                 if (result.refresh == true) {
    //                     this.addToRefreshCartApi(item, result.message)
    //                 } else {
    //                     FlushMsg.showError(result.message);
    //                 }
    //             }
    //             console.log('result:: ', result)
    //         }.bind(this),
    //         function (result) {
    //             console.log('result:: ', result)
    //             this.loaderShowHideApi(false);
    //             FlushMsg.showError(result.message);
    //         }.bind(this)
    //     )

    // };

    // reOrderRefreshApi = (item) => {
    //     this.loaderShowHideApi(true);
    //     var myData = {
    //         "orderID": item?.orderID
    //         //"customerID": "5"
    //     };
    //     console.log(myData)
    //     OrderService.reOrderRefresh(myData).then(
    //         function (result) {
    //             console.log('Your data: ' + JSON.stringify(result));
    //             this.loaderShowHideApi(false);
    //             if (result.status) {
    //                 Actions.tab_2();
    //             } else {
    //                 if (result.refresh == true) {
    //                     this.addToRefreshCartApi(item, result.message)
    //                 } else {
    //                     FlushMsg.showError(result.message);
    //                 }
    //             }
    //             console.log('result:: ', result)
    //         }.bind(this),
    //         function (result) {
    //             console.log('result:: ', result)
    //             this.loaderShowHideApi(false);
    //             FlushMsg.showError(result.message);
    //         }.bind(this)
    //     )

    // };

    // addToRefreshCartApi(item, message) {
    //     Alert.alert('Items already in cart', message, [
    //         {
    //             text: 'YES',
    //             onPress: () => {
    //                 this.reOrderRefreshApi(item);
    //             },
    //         },
    //         { text: 'NO' },
    //     ]);
    // }

    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        const { orderdetailsList, loginUserData } = this.state;
        console.log('orderdetailsList.orderID:: ', orderdetailsList.orderID);
        return (
            <>
                <MainContainer>

                    {/* <Loader loading={this.state.apiLoading} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>
                                {
                                    (this.state.apiLoader) ?
                                        <Loader loading={this.state.apiLoader} /> :

                                        <View style={pagestyles.Container}>
                                            <TopHeader
                                                title={'Order Detail'}
                                                onPress={() => {
                                                    Actions.pop();
                                                }}
                                            />
                                            {/* {this.renderHeaderShop()} */}
                                            {/* {this.state.orderdetailsList.map(item => {
                                                return ( */}
                                            {
                                                (orderdetailsList.orderID != undefined) &&
                                                <View style={orderdetailsstyle.pastorderholder}>

                                                    <View style={[orderdetailsstyle.detailsTopBorder, { paddingBottom: 15, paddingTop: 15 }]}>
                                                        <View>
                                                            <Text style={orderdetailsstyle.ordertext}>
                                                                This order with {(orderdetailsList?.store_type == "R") ?
                                                                    orderdetailsList?.restaurant_name : orderdetailsList?.store_name} was {orderdetailsList?.order_status_name}
                                                            </Text>
                                                        </View>
                                                        <View>

                                                            {/* <Text>{item.order_number}</Text>; */}

                                                        </View>
                                                    </View>
                                                    <View style={[orderdetailsstyle.detailsTopBorder, { paddingBottom: 15, paddingTop: 15 }]}>
                                                        <View><Text style={orderdetailsstyle.orderheading1}>Your Order</Text></View>
                                                    </View>
                                                    <View style={[{ paddingBottom: 15, paddingTop: 0 }]}>
                                                        {
                                                            (orderdetailsList?.order_items) &&
                                                            orderdetailsList?.order_items.map((data, index) => {
                                                                return (
                                                                    <>
                                                                        <View style={[orderdetailsstyle.flex1, {
                                                                            marginBottom: 5
                                                                        }]}>
                                                                            {/* <View style={{
                                                                                backgroundColor: Color.whiteColor,
                                                                                borderWidth: 1, borderStyle: 'solid', borderColor: Color.orangeColor,
                                                                                width: 20, height: 20, textAlign: 'center',
                                                                                marginRight: 8, padding: 4
                                                                            }}><Image
                                                                                    source={Images.orangedot}
                                                                                /></View> */}
                                                                            <View><Text style={orderdetailsstyle.ordertext1}>{data?.quantity} * {data?.item_name}</Text></View>
                                                                        </View>
                                                                        {/* <View style={[orderdetailsstyle.flex1, styles.mb_30]}>
                                                                            <View style={[orderdetailsstyle.flex1, styles.mb_5]}>
                                                                                <View><Text style={orderdetailsstyle.ordertext}>{data?.quantity} * £{data?.price}</Text></View>
                                                                            </View> 
                                                                            
                                                                        </View>*/}
                                                                    </>
                                                                )
                                                            })
                                                        }

                                                        {/* <View style={[orderdetailsstyle.flex1, styles.mb_5]}>
                                                            <View><Text style={orderdetailsstyle.ordertext}>Item Total</Text></View>
                                                            <View><Text style={orderdetailsstyle.ordertext}>£{orderdetailsList.sub_total}</Text></View>
                                                        </View>
                                                        <View style={[orderdetailsstyle.flex1, styles.mb_5]}>
                                                            <View><Text style={orderdetailsstyle.ordertext}>Delivery Charge</Text></View>
                                                            <View><Text style={orderdetailsstyle.ordertext}>£{orderdetailsList.delivery_charge}</Text></View>
                                                        </View>
                                                        <View style={[orderdetailsstyle.flex1, styles.mt_5]}>
                                                            <View><Text style={orderdetailsstyle.orderheading1}>Grand Total</Text></View>
                                                            <View><Text style={orderdetailsstyle.orderheading1}>£{orderdetailsList.total_amount}</Text></View>
                                                        </View> */}
                                                        {
                                                            (loginUserData.venderID == "") &&
                                                            <View style={[orderdetailsstyle.flex1, styles.mt_5, {
                                                                marginTop: 15
                                                            }]}>
                                                                <View><Text style={orderdetailsstyle.orderheading1}>You Earned</Text></View>
                                                                <View><Text style={orderdetailsstyle.orderheading1}>
                                                                    {/* £{orderdetailsList.driver_earning} */}
                                                                    £{orderdetailsList?.delivery_charge} Gross, £{orderdetailsList?.driver_earning} Net
                                                                </Text></View>
                                                            </View>
                                                        }
                                                    </View>
                                                    <View style={[orderdetailsstyle.detailsTopBorder, { paddingBottom: 15, paddingTop: 15 }]}>
                                                        <View><Text style={[orderdetailsstyle.orderheading1, styles.mb_20]}>Order Details</Text></View>

                                                        {
                                                            (orderdetailsList?.spl_instruct_driver != "") &&
                                                            <>
                                                                <View><Text style={[orderdetailsstyle.ordertext1,]}>CUSTOMER INSTRUCTIONS</Text></View>
                                                                <View><Text style={[orderdetailsstyle.orderheading2, styles.mb_10]}>{orderdetailsList?.spl_instruct_driver}</Text></View>
                                                            </>
                                                        }

                                                        <View><Text style={[orderdetailsstyle.ordertext1,]}>ORDER NUMBER</Text></View>
                                                        <View><Text style={[orderdetailsstyle.orderheading2, styles.mb_10]}>{orderdetailsList.order_number}</Text></View>

                                                        <View><Text style={[orderdetailsstyle.ordertext1,]}>payment</Text></View>
                                                        <View><Text style={[orderdetailsstyle.orderheading2, styles.mb_10]}>{orderdetailsList.payment_method}</Text></View>

                                                        <View><Text style={[orderdetailsstyle.ordertext1,]}>date</Text></View>
                                                        <View><Text style={[orderdetailsstyle.orderheading2, styles.mb_10]}>{GlobalFunction.formatDateTime(orderdetailsList?.order_date)}</Text></View>

                                                        <View><Text style={[orderdetailsstyle.ordertext1,]}>phone NUMBER</Text></View>
                                                        <View><Text style={[orderdetailsstyle.orderheading2, styles.mb_10]}>{orderdetailsList.customer_phone}</Text></View>

                                                        <View><Text style={[orderdetailsstyle.ordertext1,]}>deliver to</Text></View>
                                                        <View><Text style={[orderdetailsstyle.orderheading2, styles.mb_10]}>{orderdetailsList.delivery_address}</Text></View>

                                                    </View>
                                                    <View style={[orderdetailsstyle.detailsTopBorder, { paddingBottom: 15, paddingTop: 15 }]}>
                                                        {
                                                            (global.userType == "1") ?
                                                                <View>
                                                                    <TouchableOpacity
                                                                        onPress={() => {
                                                                            GlobalFunction.dialCall(orderdetailsList?.phone_no)
                                                                        }}
                                                                    >
                                                                        <Text style={[{ color: Color.orangeColor, fontWeight: 'bold' }]}>
                                                                            Call {(orderdetailsList?.store_type == "R") ?
                                                                                orderdetailsList?.restaurant_name : orderdetailsList?.store_name} ({orderdetailsList?.phone_no})
                                                                        </Text>
                                                                    </TouchableOpacity>
                                                                </View> :
                                                                <View>
                                                                    <TouchableOpacity
                                                                        onPress={() => {
                                                                            GlobalFunction.dialCall(orderdetailsList?.customer_phone)
                                                                        }}
                                                                    >
                                                                        <Text style={[{ color: Color.orangeColor, fontWeight: 'bold' }]}>
                                                                            Call {orderdetailsList?.customer_name} ({orderdetailsList?.customer_phone})
                                                                        </Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                        }

                                                        {
                                                            (orderdetailsList?.order_status_name == "Cancelled" ||
                                                                orderdetailsList?.order_status_name == "Delivered" ||
                                                                orderdetailsList?.order_status_name == "Declined") &&
                                                            <View style={{ marginTop: 20 }}>
                                                                {/* <Button
                                                                    style={[orderdetailsstyle.signSubmit]}
                                                                    text={'REORDER'}
                                                                    onLoading={this.state.apiLoading}
                                                                    onPress={() => {
                                                                        this.reOrderApi(orderdetailsList)
                                                                    }}
                                                                /> */}
                                                            </View>
                                                        }

                                                    </View>
                                                </View>
                                            }
                                            {/* // )
                                            })} */}
                                        </View>
                                }
                            </View>


                        </ScrollView>
                    </View>
                </MainContainer>
            </>
        )
    }
}


export default OrderDetailsScreen;




