import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  Keyboard,
  ScrollView,
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import { Actions } from 'react-native-router-flux';
import FlushMsg from '../utils/FlushMsg';
import Loader from '../utils/Loader';
import { AuthInputBoxSec, Button, ImageCustom, TopHeader } from '../components';
import { Color, Fonts, Images } from '../utils';
import driverprofilestyle from '../styles/driverprofile.style';
import { RF } from '../utils/responsive';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { DriverService } from '../services';
import DatePicker from 'react-native-datepicker';
import HearAboutScreen from '../components/HearAbout';
import TransprtModeScreen from '../components/TransportMode';
import WorkLocationScreen from '../components/WorkLocationType';
import StateListScreen from '../components/StateList';
import CityListScreen from '../components/CityList';
import SearchPlaceScreen from '../components/SearchPlace';
import AsyncStorage from '@react-native-async-storage/async-storage';

// import DateTimePicker from '@react-native-community/datetimepicker';

const showActionSheetCameraPicker = ['Cancel', 'Camera', 'Album'];
const showActionSheetPicker = ['Cancel', 'Yes', 'No'];
const showActionSheetGenderPicker = ['Cancel', 'Male', 'Female'];

const CANCEL_INDEX = 0;

const postCodeRegex = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9 ]+)$/;
const postCodeUKRegex = /^(([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))\s?[0-9][A-Za-z]{2}))$/;

const nameRegex = /[^a-zA-Z ]/;

class DriverProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginUserData: JSON.parse(global.loginUserData),
      apiLoader: false,
      apiLoding: false,
      loading: false,
      cityListVisible: false,
      stateListVisible: false,
      worklocationVisible: false,
      transportModeVisible: false,
      hearAboutVisible: false,
      searchPlaceVisible: false,
      cityid: '',
      zoneid: '',
      state: '',
      city: '',
      worklocation: '',
      worklocationId: '',
      transprtmode: '',
      transprtmodeId: '',
      hearabout: '',
      hearaboutId: '',
      name: '',
      driverPic: '',
      workproofPic: '',
      addresprfPic: '',
      foodinsurancPic: '',
      drvingPic: '',
      email: '',
      mobileNo: '',
      birthdate: '',
      address: '',
      Postcode: '',
      house: '',
      unspentCriminal: '',
      emergencyname: '',
      releation: '',
      emergencymobileNo: '',
      countriesId: '',
      gender: '',
      isActiverecord: '',
      locality: '',
      shrtbio: '',
      cameraType: '',
      coutryID: '',
      // isPage: '',
    };
  }
  componentDidMount() {
    console.log('loginUserData', this.state.loginUserData);
    this.setDrivertData();
    // this.getheaderName();
  }
  // getheaderName = async () => {
  //   const details = await AsyncStorage.getItem('@multiProfileHeader');
  //   this.setState({
  //     isPage: details,
  //   });
  // };

  // async clearAsync(key) {
  //   try {
  //     await AsyncStorage.removeItem(key);
  //     return true;
  //   } catch (exception) {
  //     return false;
  //   }
  // }

  setDrivertData = () => {
    this.setState({
      // update: true,
      name: this.state.loginUserData.name,
      mobileNo: this.state.loginUserData.phone_no,
      email: this.state.loginUserData.email,
      shrtbio: this.state.loginUserData.short_bio,
      gender: this.state.loginUserData.gender,
      locality: this.state.loginUserData.locality,
      birthdate:
        this.state.loginUserData.date_of_birth == '0000-00-00'
          ? ''
          : this.state.loginUserData.date_of_birth,
      coutryID: this.state.loginUserData.countries_id,
      zoneid: this.state.loginUserData.zone_id,
      cityid: this.state.loginUserData.city_id,
      address: this.state.loginUserData.address,
      latitude: this.state.loginUserData.latitude,
      longitude: this.state.loginUserData.longitude,
      Postcode: this.state.loginUserData.zipcode,
      house: this.state.loginUserData.house_no,
      city: this.state.loginUserData.city_name,
      state: this.state.loginUserData.zone_name,
      transprtmode: (this.state.loginUserData.transport != undefined) ? this.state.loginUserData.transport : "",
      worklocation: (this.state.loginUserData.worklocation != undefined) ? this.state.loginUserData.worklocation : "",
      hearabout: (this.state.loginUserData.hearabout != undefined) ? this.state.loginUserData.hearabout : "",
      transprtmodeId: this.state.loginUserData.mode_of_transport_ID,
      worklocationId: this.state.loginUserData.work_location_ID,
      unspentCriminal: this.state.loginUserData.criminal_convictions == 1 ? 'Yes' : 'No',
      emergencyname: this.state.loginUserData.emergency_person_name,
      releation: this.state.loginUserData.emergency_person_relationship,
      emergencymobileNo: this.state.loginUserData.emergency_person_phone_no,
      hearaboutId: this.state.loginUserData.hear_about_us_ID,
      isActiverecord: this.state.loginUserData.criminal_convictions,
      profilePic: this.state.loginUserData.profile_pic,
      drivngFile: this.state.loginUserData.driving_licence_file,
      workFile: this.state.loginUserData.work_permit_file,
      adresFile: this.state.loginUserData.address_proof_file,
      foodFile: this.state.loginUserData.food_delivery_insurance_file,
      driverPic: this.state.loginUserData.profile_photo,
      workproofPic: this.state.loginUserData.work_in_uk_proof,
      addresprfPic: this.state.loginUserData.address_proof,
      foodinsurancPic: this.state.loginUserData.food_delivery_insurance_proof,
      drvingPic: this.state.loginUserData.driving_licence_proof,
    });
  };

  editUpdateDriverApi = () => {
    this.loaderShowHide(true);
    var myData = {
      name: this.state.name,
      phone_no: this.state.mobileNo,
      email: this.state.email,
      short_bio: this.state.shrtbio,
      gender: this.state.gender,
      date_of_birth: this.state.birthdate,
      countries_id: '232',
      address: this.state.address,
      // latitude: this.state.latitude,
      // longitude: this.state.longitude,
      zone_id: this.state.zoneid,
      city_id: this.state.cityid,
      zipcode: this.state.Postcode,
      house_no: this.state.house,
      mode_of_transport_ID: this.state.transprtmodeId,
      work_location_ID: this.state.worklocationId,
      criminal_convictions: this.state.isActiverecord,
      emergency_person_name: this.state.emergencyname,
      emergency_person_relationship: this.state.releation,
      emergency_person_phone_no: this.state.emergencymobileNo,
      hear_about_us_ID: this.state.hearaboutId,
      locality: this.state.locality,
      driverID: this.state.loginUserData.driverID,

      profile_pic: this.state.profilePic,
      driving_licence_file: this.state.drivngFile,
      work_permit_file: this.state.workFile,
      address_proof_file: this.state.adresFile,
      food_delivery_insurance_file: this.state.foodFile,
    };
    const imageData = [];
    if (this.state.driverPic.path != undefined) {
      const profile_photo = {
        serverFileName: 'profile_photo',
        uri: this.state.driverPic.path,
        fileName:
          Platform.OS == 'ios' ? this.state.driverPic.filename : 'image',
        type: this.state.driverPic.mime,
      };
      imageData.push(profile_photo);
    }
    if (this.state.workproofPic.path != undefined) {
      const work_in_uk_proof = {
        serverFileName: 'work_in_uk_proof',
        uri: this.state.workproofPic.path,
        fileName:
          Platform.OS == 'ios' ? this.state.workproofPic.filename : 'image',
        type: this.state.workproofPic.mime,
      };
      imageData.push(work_in_uk_proof);
    }
    if (this.state.drvingPic.path != undefined) {
      const driving_licence_proof = {
        serverFileName: 'driving_licence_proof',
        uri: this.state.drvingPic.path,
        fileName:
          Platform.OS == 'ios' ? this.state.drvingPic.filename : 'image',
        type: this.state.drvingPic.mime,
      };
      imageData.push(driving_licence_proof);
    }
    if (this.state.addresprfPic.path != undefined) {
      const address_proof = {
        serverFileName: 'address_proof',
        uri: this.state.addresprfPic.path,
        fileName:
          Platform.OS == 'ios' ? this.state.addresprfPic.filename : 'image',
        type: this.state.addresprfPic.mime,
      };
      imageData.push(address_proof);
    }
    if (this.state.foodinsurancPic.path != undefined) {
      const food_delivery_insurance_proof = {
        serverFileName: 'food_delivery_insurance_proof',
        uri: this.state.foodinsurancPic.path,
        fileName:
          Platform.OS == 'ios' ? this.state.foodinsurancPic.filename : 'image',
        type: this.state.foodinsurancPic.mime,
      };
      imageData.push(food_delivery_insurance_proof);
    }

    console.log('imageData:: ', imageData);
    console.log('myname ::', myData);
    DriverService.updateDriveApi(myData, imageData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          console.log('result.message:: ', result.message);
          if (this.props.isPage == 'detail') {
            FlushMsg.showSuccess('Detail Successfully Submited. Please wait for Admin Approval.');
          } else {
            FlushMsg.showSuccess(result.message);
          }

          if (result.data.otp_id != undefined) {
            Actions.otpscreen({
              data: result.data,
              isPage: 'editProfileDriver',
              storeUserData: this.storeUserData.bind(this),
            });
          } else {

            if (this.props.isPage == 'detail') {
              Actions.reset('signinscreen')
            } else {
              this.storeUserData(result.data);
            }
          }
        } else {
          FlushMsg.showError(result.message);
        }
        console.log('result:: ', result);
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };
  storeUserData = (data) => {
    console.log('datadatadatadata:: ', data);
    global.loginUserData = JSON.stringify(data);
    AsyncStorage.setItem('@multiDRIVERData', global.loginUserData);
    this.setState(
      {
        loginUserData: JSON.parse(global.loginUserData),
      },
      () => {
        Actions.tab_1();

      },
    );
  };

  showActionSheetCamPicker = (suman) => {
    this.ActionSheetCameraPicker.show();
    console.log('sam', suman);
  };
  handlePressCameraPickerType = (buttonIndex) => {
    if (buttonIndex !== 0) {
      if (buttonIndex === 1) {
        this.chooseImageFromCamera();
      } else if (buttonIndex === 2) {
        this.chooseImageFromPhotoAlbum();
      }
    }
  };
  chooseImageFromCamera = () => {
    const { cameraType } = this.state;
    // this.state.camera = true;
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        console.log('Select Image', image);
        this.setState(
          {
            [cameraType]: image,
          },
          () => {
            console.log('this.state.driverPic:: ', this.state.driverPic);
          },
        );
      })
      .catch((err) => {
        console.log('catch' + err.toString());
      });
  };
  chooseImageFromPhotoAlbum = () => {
    const { cameraType } = this.state;
    console.log('stateName', cameraType);

    // this.state.album = true;
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        console.log('Select Image', image);
        this.setState(
          {
            [cameraType]: image,
          },
          () => {
            console.log('this.state.driverPic:: ', this.state.driverPic);
          },
        );
      })
      .catch((err) => {
        console.log('catch' + err.toString());
      });
  };

  saveAction = () => {
    if (this.state.name == '') {
      FlushMsg.showError('Please Enter Your Full Name');
    } else if (!nameRegex.test(this.state.name) === false) {
      FlushMsg.showError('Name cannot contain number/special characters');
    } else if (this.state.email == '') {
      FlushMsg.showError('Please Enter Your Email');
    } else if (this.state.mobileNo == '') {
      FlushMsg.showError('Please Enter Your phone Number');
    } else if (this.state.mobileNo.length < 11) {
      FlushMsg.showError('Mobile Number must be of 11 Digit');
    } else if (!this.state.mobileNo.startsWith(0)) {
      FlushMsg.showError('Please Add 0 before phone Number');
    } else if (this.state.address == '') {
      FlushMsg.showError('Please Enter Your Address');
    } else if (this.state.address != '' && this.state.Postcode == '') {
      FlushMsg.showError("It appears that the address you entered is either incomplete or incorrect. Please enter the full address");
    } else if (postCodeUKRegex.test(this.state.Postcode) === false) {
      FlushMsg.showError("It appears that the address you entered is either incomplete or incorrect. Please enter the full address");
    } else if (this.state.house == '') {
      FlushMsg.showError('Please Enter Your House / Flat No');
    } else if (this.state.state == '') {
      FlushMsg.showError('Please select your County');
    }
    // else if (this.state.city == '') {
    //   FlushMsg.showError('Please select your City');
    // } 
    else if (this.state.Postcode == '') {
      FlushMsg.showError('Please Enter your Postcode');
    } else if (this.state.Postcode.length < 2) {
      FlushMsg.showError('Postcode must be greater than 1 Digit');
    } else if (this.state.Postcode.length > 8) {
      FlushMsg.showError('Postcode cannot be less than 8 digits');
    } else if (postCodeRegex.test(this.state.Postcode) === false) {
      FlushMsg.showError('Postcode must contains alphanumeric characters');
    }
    // else if (this.state.locality == '') {
    //   FlushMsg.showError('Please Enter Your Locality');
    // }
    else if (this.state.gender == '') {
      FlushMsg.showError('Please Select Your Gender');
    } else if (this.state.birthdate == '') {
      FlushMsg.showError('Please Select your Date of Birth');
    } else if (this.state.transprtmode == '') {
      FlushMsg.showError('Please select Mode of Transport');
    } else if (this.state.worklocation == '') {
      FlushMsg.showError('Please select your Work Location');
    } else if (this.state.workproofPic == '') {
      FlushMsg.showError('Please Add your Work Proof Photo');
    } else if (this.state.addresprfPic == '') {
      FlushMsg.showError('Please Add your Address Proof Photo');
    } else if (this.state.foodinsurancPic == '') {
      FlushMsg.showError('Please Add your Food Insurance Proof Photo');
    } else if (this.state.drvingPic == '') {
      FlushMsg.showError('Please Add your Driving Licence Photo');
    } else if (this.state.unspentCriminal == '') {
      FlushMsg.showError('Please make a selection Criminal Convictions is');
    } else if (this.state.emergencyname == '') {
      FlushMsg.showError('Please Enter Your Emergency Person Name');
    } else if (!nameRegex.test(this.state.emergencyname) === false) {
      FlushMsg.showError(
        'Person Name cannot contain number/special characters',
      );
    } else if (this.state.releation == '') {
      FlushMsg.showError('Please Enter your Emergency Person Relationship');
    } else if (!nameRegex.test(this.state.releation) === false) {
      FlushMsg.showError(
        'Relationship cannot contain number/special characters',
      );
    } else if (this.state.emergencymobileNo == '') {
      FlushMsg.showError('Please Enter your Emergency Person Mobile No');
    } else if (!this.state.emergencymobileNo.startsWith(0)) {
      FlushMsg.showError('Please Add 0 before  Emergency Person Mobile No');
    } else if (this.state.hearabout == '') {
      FlushMsg.showError('Please Select Hear About us');
    } else {
      Keyboard.dismiss();
      this.editUpdateDriverApi();
    }
  };
  loaderShowHide = (status) => {
    this.setState({
      apiLoader: status,
    });
  };
  loaderShowHideApi = (status) => {
    this.setState({
      apiLoding: status,
    });
  };

  openGooglePlace = () => {
    this.setState({
      searchPlaceVisible: true,
    });
  };

  closeGooglePlace = () => {
    this.setState({
      searchPlaceVisible: false,
    });
  };
  selectGooglePlace = (info) => {
    console.log('info:: ', info);
    console.log('info:: ', info?.data?.description);
    console.log('info:: ', info?.latitude);
    console.log('info:: ', info?.longitude);
    let details = info?.details
    let locationDetails = ''
    let sep = ''
    let postal = ''
    for (var i = 0; i < details.address_components.length; i++) {
      console.log('details.address_components[i].long_name:: ', details.address_components[i].long_name)
      locationDetails = locationDetails + sep + details.address_components[i].long_name
      sep = ', '
      for (var j = 0; j < details.address_components[i].types.length; j++) {

        if (details.address_components[i].types[j] == "postal_code") {
          // document.getElementById('postal_code').innerHTML = details.address_components[i].long_name;
          console.log('details.address_components[i].long_name:: ', details.address_components[i].long_name)
          postal = details.address_components[i].long_name
        }
      }
    }
    this.setState({
      address: info?.data?.description,
      searchPlaceVisible: false,
      latitude: info?.latitude,
      longitude: info?.longitude,
      locationDetails: locationDetails,
      Postcode: postal
    });
  };

  showActionSheetGenPicker = () => {
    this.ActionSheetGenderPicker.show();
  };
  handlePressGenderType = (buttonIndex) => {
    var gender;
    // alert(buttonIndex)
    if (buttonIndex !== 0) {
      if (buttonIndex === 1) {
        gender = 'Male';
      } else if (buttonIndex === 2) {
        gender = 'Female';
      }
      this.setState({
        selected: buttonIndex,
        gender: gender,
      });
    }
  };

  handlePressPickerType = (buttonIndex) => {
    var isActiverecord, unspentCriminal;
    if (buttonIndex !== 0) {
      if (buttonIndex === 1) {
        unspentCriminal = 'Yes';
        isActiverecord = 1;
      } else if (buttonIndex === 2) {
        unspentCriminal = 'No';
        isActiverecord = 0;
      }
      this.setState({
        selected: buttonIndex,
        isActiverecord: isActiverecord,
        unspentCriminal: unspentCriminal,
      });
    }
  };

  showActionSheetPicker = () => {
    this.ActionSheetPicker.show();
  };

  hearAboutListAction = (info) => {
    console.log('info info:: ', info);
    if (info.showMsg === true) {
      if (info.item != null) {
        // this.state.countryId = info.item.id;
        this.state.hearabout = info.item.name;
        this.state.hearaboutId = info.item.hear_about_us_ID;

        this.setState(
          {
            hearAboutVisible: false,
            listData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItem: {},
            selectIndex: -1,
            hearabout: info.item.name,
            hearaboutId: info.item.hear_about_us_ID,
          },
          () => { },
        );
      } else {
        this.setState(
          {
            hearAboutVisible: false,
            listData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItem: {},
            selectIndex: -1,
            hearabout: '',
            hearaboutId: '',
          },
          () => { },
        );
      }
    } else {
      this.showHidehearAboutList(false);
    }
  };
  renderhearAboutList = () => {
    const { selectItem, selectIndex } = this.state;
    return (
      <View style={pagestyles.modalViewDial}>
        <HearAboutScreen
          selectItem={selectItem}
          selectIndex={selectIndex}
          closeModal={this.hearAboutListAction}
        />
      </View>
    );
  };
  showHidehearAboutList = (visible, item, index) => {
    this.setState({
      hearAboutVisible: visible,
      selectItem: visible ? item : {},
      selectIndex: visible ? index : -1,
    });
  };

  closeModalCityListAction = (info) => {
    console.log('info info:: ', info);
    if (info.showMsg === true) {
      if (info.item != null) {
        // this.state.countryId = info.item.id;
        this.state.city = info.item.city_name;
        this.state.cityid = info.item.city_id;

        this.setState(
          {
            cityListVisible: false,
            listData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItem: {},
            selectIndex: -1,
          },
          () => { },
        );
      } else {
        this.state.city = 'City';
        this.setState(
          {
            cityListVisible: false,
            listData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItem: {},
            selectIndex: -1,
          },
          () => { },
        );
      }
    } else {
      this.showHideCityList(false);
    }
  };
  renderCityTypeList = () => {
    const { selectItem, selectIndex } = this.state;
    return (
      <View style={pagestyles.modalViewDial}>
        <CityListScreen
          selectItem={selectItem}
          selectIndex={selectIndex}
          zoneID={this.state.zoneid}
          closeModal={this.closeModalCityListAction}
        />
      </View>
    );
  };
  showHideCityList = (visible, item, index) => {
    this.setState({
      cityListVisible: visible,
      selectItem: visible ? item : {},
      selectIndex: visible ? index : -1,
    });
  };

  closeModalstateListAction = (info) => {
    console.log('info info:: ', info);
    if (info.showMsg === true) {
      if (info.item != null) {
        this.setState(
          {
            stateListVisible: false,
            statelistData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItemm: {},
            selecttIndex: -1,
            state: info.item.zone_name,
            zoneid: info.item.zone_id,
          },
          () => { },
        );
      } else {
        this.setState(
          {
            stateListVisible: false,
            statelistData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItemm: {},
            selecttIndex: -1,
            state: '',
            zoneid: 0,
          },
          () => { },
        );
      }
    } else {
      this.showHideStateList(false);
    }
  };
  renderStateTypeList = () => {
    const { selectItemm, selecttIndex } = this.state;
    return (
      <View style={pagestyles.modalViewDial}>
        <StateListScreen
          selectItemm={selectItemm}
          selecttIndex={selecttIndex}
          countryID={'232'}
          closeModal={this.closeModalstateListAction}
        />
      </View>
    );
  };
  showHideStateList = (visible, item, index) => {
    this.setState({
      stateListVisible: visible,
      selectItemm: visible ? item : {},
      selecttIndex: visible ? index : -1,
    });
  };
  closeModalTransModeListAction = (info) => {
    console.log('info info:: ', info);
    if (info.showMsg === true) {
      if (info.item != null) {
        this.setState(
          {
            transportModeVisible: false,
            statelistData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItemm: {},
            selecttIndex: -1,
            transprtmode: info.item.name,
            transprtmodeId: info.item.mode_of_transport_ID,
          },
          () => { },
        );
      } else {
        this.setState(
          {
            transportModeVisible: false,
            statelistData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItemm: {},
            selecttIndex: -1,
            transprtmode: '',
            transprtmodeId: '',
          },
          () => { },
        );
      }
    } else {
      this.showHidetransModeList(false);
    }
  };
  rendertransmodeList = () => {
    const { selectItemm, selecttIndex } = this.state;
    return (
      <View style={pagestyles.modalViewDial}>
        <TransprtModeScreen
          selectItemm={selectItemm}
          selecttIndex={selecttIndex}
          closeModal={this.closeModalTransModeListAction}
        />
      </View>
    );
  };
  showHidetransModeList = (visible, item, index) => {
    this.setState({
      transportModeVisible: visible,
      selectItemm: visible ? item : {},
      selecttIndex: visible ? index : -1,
    });
  };
  closeModalWorkLocationListAction = (info) => {
    console.log('info info:: ', info);
    if (info.showMsg === true) {
      if (info.item != null) {
        this.setState(
          {
            worklocationVisible: false,
            statelistData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItemm: {},
            selecttIndex: -1,
            worklocation: info.item.name,
            worklocationId: info.item.work_location_ID,
          },
          () => { },
        );
      } else {
        this.setState(
          {
            worklocationVisible: false,
            statelistData: [],
            page: 1,
            loading: false,
            ended: false,
            totalCount: 0,
            selectItemm: {},
            selecttIndex: -1,
            worklocation: '',
            worklocationId: '',
          },
          () => { },
        );
      }
    } else {
      this.showHideWorkLocationList(false);
    }
  };
  renderworLocypeList = () => {
    const { selectItemm, selecttIndex } = this.state;
    return (
      <View style={pagestyles.modalViewDial}>
        <WorkLocationScreen
          selectItemm={selectItemm}
          selecttIndex={selecttIndex}
          closeModal={this.closeModalWorkLocationListAction}
        />
      </View>
    );
  };
  showHideWorkLocationList = (visible, item, index) => {
    this.setState({
      worklocationVisible: visible,
      selectItemm: visible ? item : {},
      selecttIndex: visible ? index : -1,
    });
  };

  onPressBtn = () => { };

  getPreviousTwoYear = () => {
    const d = new Date();
    d.setFullYear(d.getFullYear() - 18);

    console.log(d.toDateString());
    return d;
  };

  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    const {
      cityListVisible,
      stateListVisible,
      worklocationVisible,
      transportModeVisible,
      hearAboutVisible,
      date,
    } = this.state;
    console.log(this.state.isActiverecord);
    console.log(this.state.gender);
    console.log('state', this.state);
    console.log('detail', this.props);

    return (
      <>
        <SafeAreaView
          style={{
            flex: 0,
            backgroundColor: ThemeColors.primaryColor,
          }}
        />
        <SafeAreaView>
          <Loader loading={this.state.apiLoader} />
          <View>
            <ScrollView
              style={{
                paddingBottom: 150,
              }}
              keyboardShouldPersistTaps={'handled'}
              showsVerticalScrollIndicator={false}
            >
              {cityListVisible && this.renderCityTypeList()}
              {stateListVisible && this.renderStateTypeList()}
              {worklocationVisible && this.renderworLocypeList()}
              {transportModeVisible && this.rendertransmodeList()}
              {hearAboutVisible && this.renderhearAboutList()}
              <View style={pagestyles.Wrapper}>
                <View style={pagestyles.Container}>
                  <TopHeader
                    title={
                      this.props.isPage == 'detail'
                        ? 'Addtional Details'
                        : 'Driver Profile'
                    }
                    onPress={() => {
                      {
                        this.props.isPage == 'detail'
                          ? Actions.reset('signinscreen')
                          : Actions.pop();
                      }
                    }}
                  />

                  {/* {this.renderHeaderShop()} */}

                  <View>
                    <View style={styles.commonTBpadding}>
                      <View style={styles.TextCenter}>
                        <View
                          style={{
                            width: 100,
                            height: 100,
                            borderWidth: 10,
                            borderColor: Color.primaryColor,
                            borderRadius: 120,
                            justifyContent: 'center',
                          }}
                        >
                          {/* <ImageCustom
                            imageUri={
                              this.state.driverPic.path
                                ? this.state.driverPic.path
                                : this.state.loginUserData.profile_photo
                            }
                            // imageUri={this.state.driverPic.path}
                            avatar={Images.avatar}
                            customStyles={[
                              {
                                width: 80,
                                height: 80,
                                borderRadius: 200,
                                overflow: "hidden",
                              },
                            ]}
                          /> */}
                          <Image
                            source={(this.state.driverPic.path)
                              ? { uri: this.state.driverPic.path } :
                              (this.state.loginUserData.profile_pic != "") ?
                                { uri: this.state.loginUserData.profile_photo } : Images.avatar
                            }
                            //source={Images.avatar}
                            defaultSource={Images.avatar}
                            style={{
                              width: 80,
                              height: 80,
                              borderRadius: 200,
                              // overflow: 'hidden',
                              resizeMode: 'cover'
                            }}
                          />

                          <TouchableOpacity
                            style={{
                              marginTop: 4,
                              position: 'absolute',
                              bottom: -5,
                              right: -10,
                              width: 32,
                              height: 32,
                              borderRadius: 60,
                              backgroundColor: ThemeColors.whiteColor,
                              alignItems: 'center',
                            }}
                            activeOpacity={0.7}
                            onPress={() => {
                              this.setState(
                                {
                                  cameraType: 'driverPic',
                                },
                                () => {
                                  this.showActionSheetCamPicker('driverPic');
                                },
                              );
                            }}
                          >
                            <Image
                              style={{ width: 32, height: 32 }}
                              source={Images.cameraicon}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={driverprofilestyle.profiledit}>
                        {/* <TouchableOpacity
                        // onPress={() => {
                        //     Actions.drivereditprofilescreen();
                        // }}
                        >
                          <Text style={{ color: Color.orangeColor }}>Edit</Text>
                        </TouchableOpacity> */}
                      </View>
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        lableText={'Full Name'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.nameInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ name: text })}
                        value={this.state.name}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.emailInput.focus();
                        }}
                        editable={this.props.isPage == 'detail' ? false : true}
                        maxLength={30}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        lableText={'Email'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.emailInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ email: text })}
                        value={this.state.email}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.phoneInput.focus();
                        }}
                        maxLength={50}
                        editable={this.props.isPage == 'detail' ? false : true}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        inputLayout={{
                          flexDirection: 'row',
                        }}
                        inputFieldStyle={{
                          width: '100%',
                          // marginTop:1
                        }}
                        lableText={'Phone Number'}
                        keyboardType={'number-pad'}
                        inputRef={(ref) => {
                          this.phoneInput = ref;
                        }}
                        // showCode={true}
                        onChangeText={(text) =>
                          this.setState({ mobileNo: text })
                        }
                        value={this.state.mobileNo}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.addressInput.focus();
                        }}
                        maxLength={11}
                        editable={this.props.isPage == 'detail' ? false : true}
                      />
                    </View>

                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        inputFieldStyle={{ height: 50 }}
                        lableText={'Address'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.addressInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ address: text })
                        }
                        value={this.state.address}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.shrtbioInput.focus();
                        }}
                        maxLength={100}
                        multiline={true}
                        numberOfLines={2}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          this.openGooglePlace();
                        }}
                        activeOpacity={0.7}
                        style={{
                          //backgroundColor: 'red',
                          position: 'absolute',
                          height: 65,
                          width: '100%',
                          top: 25,
                        }}
                      />
                      {this.state.searchPlaceVisible && (
                        <SearchPlaceScreen
                          closeGooglePlace={this.closeGooglePlace.bind(this)}
                          selectGooglePlace={this.selectGooglePlace.bind(this)}
                        />
                      )}
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%'
                        }}
                        isMandatoy={false}
                        lableText={'House / Flat No'}
                        keyboardType={'default'}
                        inputRef={(ref) => { this.houseInput = ref }}
                        onChangeText={text => this.setState({ house: text })}
                        value={this.state.house}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          //this.phoneInput.focus()
                        }}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        editable={false}
                        lableText={'County'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.stateInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ state: text })}
                        value={this.state.state}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          //this.phoneInput.focus()
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          this.showHideStateList(true);
                        }}
                        activeOpacity={0.7}
                        style={{
                          //backgroundColor: 'red',
                          position: 'absolute',
                          height: 65,
                          width: '100%',
                          top: 25,
                        }}
                      />
                    </View>

                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        editable={false}
                        lableText={'City'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.CityInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ city: text })}
                        value={this.state.city}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          //this.phoneInput.focus()
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          this.state.zoneid != 0
                            ? this.showHideCityList(true)
                            : FlushMsg.showError(
                              'Please select state to continue',
                            );
                        }}
                        activeOpacity={0.7}
                        style={{
                          //backgroundColor: 'red',
                          position: 'absolute',
                          height: 65,
                          width: '100%',
                          top: 25,
                        }}
                      />
                    </View>

                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        editable={false}
                        lableText={'Postcode'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.PostcodeInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ Postcode: text })
                        }
                        value={this.state.Postcode}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.datebirthInput.focus();
                        }}
                        maxLength={8}
                      />
                    </View>

                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        lableText={'locality'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.localityInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ locality: text })
                        }
                        value={this.state.locality}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.phoneInput.focus();
                        }}
                        maxLength={30}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        lableText={'Short Bio'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.shrtbioInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ shrtbio: text })
                        }
                        value={this.state.shrtbio}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.localityInput.focus();
                        }}
                        maxLength={100}
                      />
                    </View>


                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        editable={false}
                        lableText={'Gender'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.genderInput = ref;
                        }}
                        onChangeText={(text) => this.setState({ gender: text })}
                        value={this.state.gender}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          //this.phoneInput.focus()
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          this.showActionSheetGenPicker();
                        }}
                        activeOpacity={0.7}
                        style={{
                          //backgroundColor: 'red',
                          position: 'absolute',
                          height: 65,
                          width: '100%',
                          top: 25,
                        }}
                      />
                    </View>



                    <View
                      style={{
                        borderBottomWidth: 0.5,
                        borderBottomColor: 'lightgrey',
                        marginBottom: 10,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: Fonts.regular,
                          fontSize: 14,
                          marginBottom: 5,
                          marginTop: 15,
                          color: Color.greyColor,
                        }}
                      >
                        Date of Birth
                      </Text>
                      <DatePicker
                        style={{ width: '100%', marginBottom: 15 }}
                        date={this.state.birthdate} //initial date from state
                        mode="date" //The enum of date, date time and time
                        //androidMode="spinner"
                        placeholder="Select Date"
                        format="YYYY-MM-DD"
                        minDate={
                          new Date(
                            new Date().setFullYear(
                              new Date().getFullYear() - 100,
                            ),
                          )
                        }
                        maxDate={this.getPreviousTwoYear()}
                        // maxDate={
                        //   new Date()
                        //   // new Date().setFullYear(
                        //   //   new Date().getFullYear() + 100,
                        //   // ),
                        // }
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        showIcon={false}
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            // marginLeft: 36,
                            // marginTop:20
                          },
                          // ... You can check the source to find the other keys.
                        }}
                        // customStyles={{
                        //   dateIcon: {
                        //     height: 32,
                        //     position: 'absolute',
                        //     left: 0,
                        //     top: 0,
                        //     marginLeft: 0,
                        //   },
                        //   dateInput: {
                        //     height: 60,
                        //     alignItems: 'flex-start',
                        //     justifyContent: 'center',
                        //     color: ThemeColors.blackColor,
                        //     borderColor: '#99a0ae',
                        //     borderWidth: 1,
                        //     borderRadius: 30,
                        //     fontSize: 16,
                        //     paddingLeft: 20,
                        //     paddingRight: 20,
                        //   },
                        //   placeholderText: {
                        //     color: ThemeColors.primaryColor,
                        //     fontSize: 16,
                        //   },
                        // }}
                        onDateChange={(date) => {
                          console.log('chked Date::', date);
                          this.setState({ birthdate: date }, () => {
                            console.log('startDate ', this.state.birthdate);
                          });
                        }}
                      />
                      {/* <DatePicker
                        style={{ width: 200, marginBottom: 20, marginTop: 20 }}
                        date={this.state.birthdate}
                        mode="date"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        minDate="1950-06-01"
                        maxDate="2021-05-01"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0,
                          },
                          dateInput: {
                            // marginLeft: 36,
                            // marginTop:20
                          },
                          // ... You can check the source to find the other keys.
                        }}
                        onDateChange={(birthdate) => {
                          this.setState({ birthdate: birthdate });
                        }}
                      /> */}
                      {/* <AuthInputBoxSec
                                                mainContainer={{
                                                    width: '100%'
                                                }}
                                                lableText={'Date of Birth'}
                                                keyboardType={'default'}
                                                inputRef={(ref) => { this.datebirthInput = ref }}
                                                onChangeText={text => this.setState({ birthdate: text })}
                                                value={this.state.birthdate}
                                                autoCapitalize="none"
                                                returnKeyType="next"
                                                onSubmitEditing={() => {
                                                    this.phoneInput.focus()
                                                }}
                                                maxLength={10}
                                            /> */}
                    </View>

                    <TouchableOpacity
                      style={{}}
                      onPress={() => {
                        this.showHidetransModeList(true);
                      }}
                      activeOpacity={0.7}
                    >
                      <Text
                        style={{
                          fontFamily: Fonts.regular,
                          fontSize: 14,
                          marginBottom: 5,
                          marginTop: 15,
                          color: Color.greyColor,
                        }}
                      >
                        Mode of Transport
                      </Text>
                      <TextInput
                        style={{
                          width: '100%',
                          height: 40,
                          paddingVertical: 5,
                          fontSize: RF(14),
                          color: 'black',
                          marginBottom: 10,
                          paddingVertical: 5,
                          borderBottomWidth: 0.5,
                          borderBottomColor: 'lightgrey',
                          fontFamily: Fonts.regular,
                        }}
                        // lableText={'City'}
                        keyboardType={'default'}
                        editable={false}
                        inputRef={(ref) => {
                          this.phoneInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ transprtmode: text })
                        }
                        value={this.state.transprtmode}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.postalInput.focus();
                        }}
                      />
                      <Image
                        source={Images.downarrow1}
                        style={{
                          position: 'absolute',
                          right: 6,
                          top: 20,
                        }}
                      />
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{}}
                      onPress={() => {
                        this.showHideWorkLocationList(true);
                      }}
                      activeOpacity={0.7}
                    >
                      <Text
                        style={{
                          fontFamily: Fonts.regular,
                          fontSize: 14,
                          marginBottom: 5,
                          marginTop: 15,
                          color: Color.greyColor,
                        }}
                      >
                        Where do you like to work?
                      </Text>
                      <TextInput
                        style={{
                          width: '100%',
                          height: 40,
                          paddingVertical: 5,
                          fontSize: RF(14),
                          color: 'black',
                          marginBottom: 10,
                          paddingVertical: 5,
                          borderBottomWidth: 0.5,
                          borderBottomColor: 'lightgrey',
                          fontFamily: Fonts.regular,
                        }}
                        // lableText={'City'}
                        keyboardType={'default'}
                        editable={false}
                        inputRef={(ref) => {
                          this.phoneInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ worklocation: text })
                        }
                        value={this.state.worklocation}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.postalInput.focus();
                        }}
                      />
                      <Image
                        source={Images.downarrow1}
                        style={{
                          position: 'absolute',
                          right: 6,
                          top: 20,
                        }}
                      />
                    </TouchableOpacity>

                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: '400',
                            },
                          ]}
                        >
                          Proof that you can legally work in the UK{' '}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.showActionSheetCamPicker('workproofPic');
                            this.setState({
                              cameraType: 'workproofPic',
                            });
                          }}
                          //  onPress={this.showActionSheetCamPicker('workproofPic')}
                          style={[
                            driverprofilestyle.driverinput2holder,
                            styles.commonTBpadding,
                          ]}
                        >
                          <Text style={driverprofilestyle.inputfile}>
                            Choose File
                          </Text>
                        </TouchableOpacity>
                        <View
                          style={{ position: 'absolute', right: 0, bottom: 0 }}
                        >
                          <ImageCustom
                            imageUri={
                              this.state.workproofPic.path
                                ? this.state.workproofPic.path
                                : this.state.loginUserData.work_in_uk_proof
                            }
                            // imageUri={this.state.workproofPic.path}
                            customStyles={[
                              { height: 68, width: 112, borderRadius: 10 },
                            ]}
                          />
                          {/* <Image source={Images.avatar} style={{ height: 68, width: 112, borderRadius: 10 }} /> */}
                        </View>
                      </View>
                    </View>
                    <View style={{}}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: '400',
                            },
                          ]}
                        >
                          Proof of your Address
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.showActionSheetCamPicker('addresprfPic');
                            this.setState({
                              cameraType: 'addresprfPic',
                            });
                          }}
                          //  onPress={this.showActionSheetCamPicker('addresprfPic')}
                          style={[
                            driverprofilestyle.driverinput2holder,
                            styles.commonTBpadding,
                          ]}
                        >
                          <Text style={driverprofilestyle.inputfile}>
                            Choose File
                          </Text>
                        </TouchableOpacity>
                        <View
                          style={{ position: 'absolute', right: 0, bottom: 0 }}
                        >
                          <ImageCustom
                            imageUri={
                              this.state.addresprfPic.path
                                ? this.state.addresprfPic.path
                                : this.state.loginUserData.address_proof
                            }
                            // imageUri={this.state.addresprfPic.path}
                            customStyles={[
                              { height: 68, width: 112, borderRadius: 10 },
                            ]}
                          />
                          {/* <Image source={Images.avatar} style={{ height: 68, width: 112, borderRadius: 10 }} /> */}
                        </View>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: '400',
                            },
                          ]}
                        >
                          Your driving licence
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.showActionSheetCamPicker('drvingPic');
                            this.setState({
                              cameraType: 'drvingPic',
                            });
                          }}
                          //  onPress={this.showActionSheetCamPicker('foodinsurancPic')}
                          style={[
                            driverprofilestyle.driverinput2holder,
                            styles.commonTBpadding,
                          ]}
                        >
                          <Text style={driverprofilestyle.inputfile}>
                            Choose File
                          </Text>
                        </TouchableOpacity>
                        <View
                          style={{ position: 'absolute', right: 0, bottom: 0 }}
                        >
                          <ImageCustom
                            imageUri={
                              this.state.drvingPic.path
                                ? this.state.drvingPic.path
                                : this.state.loginUserData.driving_licence_proof
                            }
                            // imageUri={this.state.drvingPic.path}
                            customStyles={[
                              { height: 68, width: 112, borderRadius: 10 },
                            ]}
                          />
                          {/* <Image source={Images.avatar} style={{ height: 68, width: 112, borderRadius: 10 }} /> */}
                        </View>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: '400',
                            },
                          ]}
                        >
                          Proof of food delivery insurance{' '}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.showActionSheetCamPicker('foodinsurancPic');
                            this.setState({
                              cameraType: 'foodinsurancPic',
                            });
                          }}
                          //  onPress={this.showActionSheetCamPicker('drvingPic')}
                          style={[
                            driverprofilestyle.driverinput2holder,
                            styles.commonTBpadding,
                          ]}
                        >
                          <Text style={driverprofilestyle.inputfile}>
                            Choose File
                          </Text>
                        </TouchableOpacity>
                        <View
                          style={{ position: 'absolute', right: 0, bottom: 0 }}
                        >
                          <ImageCustom
                            imageUri={
                              this.state.foodinsurancPic.path
                                ? this.state.foodinsurancPic.path
                                : this.state.loginUserData
                                  .food_delivery_insurance_proof
                            }
                            // imageUri={this.state.foodinsurancPic.path}
                            customStyles={[
                              { height: 68, width: 112, borderRadius: 10 },
                            ]}
                          />
                          {/* <Image source={this.state.suman.path} style={{ height: 68, width: 112, borderRadius: 10 }} /> */}
                        </View>
                      </View>
                    </View>
                    <View style={styles.commonTBpadding}>
                      <View
                        style={{
                          borderBottomWidth: 0.5,
                          borderBottomColor: 'lightgrey',
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.showActionSheetPicker();
                          }}
                          activeOpacity={0.7}
                        >
                          <Text
                            style={[
                              driverprofilestyle.labeltext,
                              {
                                paddingBottom: 20,
                                fontFamily: Fonts.semiBold,
                                fontWeight: '400',
                              },
                            ]}
                          >
                            Do you have any unspent criminal convictions
                          </Text>

                          <Text style={styles.TextBlack}>
                            {this.state.unspentCriminal}
                          </Text>
                          <Image
                            source={Images.downarrow1}
                            style={{
                              position: 'absolute',
                              right: 6,
                              top: 6,
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                      <View style={{ marginTop: 5 }}>
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              paddingBottom: 10,
                              fontFamily: Fonts.semiBold,
                              fontWeight: '400',
                            },
                          ]}
                        >
                          “We will require you to complete a criminal check
                          later in the process.”
                        </Text>
                      </View>
                    </View>
                    <View
                      style={[styles.commonTBpadding, { position: 'relative' }]}
                    >
                      <View style={driverprofilestyle.line2}>
                        <Text style={{ lineHeight: 4 }}>&nbsp;</Text>
                      </View>
                      <View
                        style={{
                          display: 'flex',
                          justifyContent: 'center',
                          flexDirection: 'row',
                        }}
                      >
                        <Text
                          style={[
                            driverprofilestyle.labeltext,
                            {
                              fontSize: 16,
                              color: Color.orangeColor,
                              textAlign: 'center',
                              backgroundColor: Color.greyApp,
                            },
                          ]}
                        >
                          Emergency contact{' '}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        lableText={'Full Name'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.nameInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ emergencyname: text })
                        }
                        value={this.state.emergencyname}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.releationInput.focus();
                        }}
                        maxLength={30}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        lableText={'Relationship'}
                        keyboardType={'default'}
                        inputRef={(ref) => {
                          this.releationInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ releation: text })
                        }
                        value={this.state.releation}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.mobInput.focus();
                        }}
                        maxLength={20}
                      />
                    </View>
                    <View>
                      <AuthInputBoxSec
                        mainContainer={{
                          width: '100%',
                        }}
                        inputLayout={{
                          flexDirection: 'row',
                        }}
                        inputFieldStyle={{
                          width: '100%',
                          // marginTop:1
                        }}
                        // showCode={true}
                        lableText={'Phone Number'}
                        keyboardType={'number-pad'}
                        inputRef={(ref) => {
                          this.mobInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ emergencymobileNo: text })
                        }
                        value={this.state.emergencymobileNo}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.phoneInput.focus();
                        }}
                        maxLength={11}
                      />
                    </View>
                    <TouchableOpacity
                      onPress={() => {
                        this.showHidehearAboutList(true);
                      }}
                      activeOpacity={0.7}
                    >
                      <Text
                        style={{
                          fontFamily: Fonts.regular,
                          fontSize: 14,
                          marginBottom: 5,
                          marginTop: 15,
                          color: Color.greyColor,
                        }}
                      >
                        How did you hear about us?
                      </Text>
                      <TextInput
                        style={{
                          width: '100%',
                          height: 40,
                          paddingVertical: 5,
                          fontSize: RF(14),
                          color: 'black',
                          marginBottom: 10,
                          paddingVertical: 5,
                          borderBottomWidth: 0.5,
                          borderBottomColor: 'lightgrey',
                          fontFamily: Fonts.regular,
                        }}
                        // lableText={'City'}
                        keyboardType={'default'}
                        editable={false}
                        inputRef={(ref) => {
                          this.phoneInput = ref;
                        }}
                        onChangeText={(text) =>
                          this.setState({ hearabout: text })
                        }
                        value={this.state.hearabout}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          this.postalInput.focus();
                        }}
                      />
                      <Image
                        source={Images.downarrow1}
                        style={{
                          position: 'absolute',
                          right: 6,
                          top: 20,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View>
                    <View
                      style={{ marginBottom: 50 }}
                    // style={{
                    //   position: 'absolute',
                    //   bottom: Platform.OS == 'ios' ? 40 : 0,
                    //   left: 0,
                    //   // backgroundColor: 'red',
                    //   width: '100%',
                    //   paddingLeft: 10,
                    //   paddingRight: 10,
                    // }}
                    >
                      <Button
                        text={
                          this.props.isPage == 'detail'
                            ? 'Submit'
                            : 'Update Profile'
                        }
                        onLoading={false}
                        onPress={() => this.saveAction()}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>

            <ActionSheet
              ref={(o) => {
                this.ActionSheetPicker = o;
              }}
              options={showActionSheetPicker}
              cancelButtonIndex={CANCEL_INDEX}
              onPress={(index) => {
                this.handlePressPickerType(index);
              }}
              tintColor={ThemeColors.primaryColor}
            />
            <ActionSheet
              ref={(o) => {
                this.ActionSheetCameraPicker = o;
              }}
              options={showActionSheetCameraPicker}
              cancelButtonIndex={CANCEL_INDEX}
              onPress={(index) => {
                this.handlePressCameraPickerType(index);
              }}
              tintColor={ThemeColors.primaryColor}
            />
            <ActionSheet
              ref={(o) => {
                this.ActionSheetGenderPicker = o;
              }}
              options={showActionSheetGenderPicker}
              cancelButtonIndex={CANCEL_INDEX}
              onPress={(index) => {
                this.handlePressGenderType(index);
              }}
              tintColor={ThemeColors.primaryColor}
            />
          </View>
        </SafeAreaView>
      </>
    );
  }
}

export default DriverProfileScreen;
