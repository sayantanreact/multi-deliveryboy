import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator, CheckBox, ImageBackground,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import { Modal } from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import driverorderhistorystyle from '../styles/driverorderhistory.style';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button } from '../components'
import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';

// import { FlushMsg } from '../utils'

class DriverOrderHistoryScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: true
        }
    }
    componentDidMount() {

    }

    onPressBtn = () => {
        FlushMsg.showSuccess("hiiiii")
    }

    renderHeaderShop = () => {
        return (
            <View>
                <View style={styles.Header}>
                    <View style={styles.HeaderLeft}>
                        <TouchableOpacity
                            style={{}}
                            activeOpacity={0.5}

                            onPress={() => {
                                Actions.pop();
                            }}
                        >
                            <Image
                                source={Images.leftArrow}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.HeaderMiddle}>
                        <Text style={Styles.Heading}>Order History</Text>
                    </View>
                    <View style={styles.HeaderRight}>
                        <Text>&nbsp; </Text>
                    </View>
                </View>
            </View>
        );
    };
    /**
    * render() this the main function which used to display different view
    * and contain all view related information.
    */
    render() {
        return (
            <>
                <SafeAreaView style={{
                    flex: 0,
                    backgroundColor: ThemeColors.primaryColor,
                }} />
                <SafeAreaView>
                    {/* <Loader loading={this.state.apiLoader} /> */}
                    <View>
                        <ScrollView showsVerticalScrollIndicator={false}>

                            <View style={pagestyles.Wrapper}>

                                <View style={pagestyles.Container}>
                                    {this.renderHeaderShop()}
                                    <View>
                                        <View style={[driverorderhistorystyle.notificationlist, styles.mb_15, styles.commonPadding, styles.brd5, {
                                            backgroundColor: Color.whiteColor, marginBottom: 20
                                        }]}>
                                            <View style={[driverorderhistorystyle.ntflex1, styles.mb_15]}>
                                                <View>
                                                    <View><Text style={[driverorderhistorystyle.nthead1,]}>Order # 15632</Text></View>
                                                    <View><Text style={driverorderhistorystyle.nttext}>10 July 2021   |   5.30 pm</Text></View>
                                                </View>
                                                <View>
                                                    <View><Text style={[driverorderhistorystyle.nttext, { textAlign: 'right' }]}>2 Items</Text></View>
                                                    <View><Text style={[driverorderhistorystyle.nttext, { textAlign: 'right' }]}>Amount : £120</Text></View>
                                                </View>
                                            </View>
                                            <View style={driverorderhistorystyle.ntdetails}>
                                                <View style={{ width: "70%" }}>
                                                    <View><Text style={[driverorderhistorystyle.nthead1, {}]}>DANIEL REX</Text></View>
                                                    <View><Text style={driverorderhistorystyle.nttext1}>Delivery Address : THE BUSINESS CENTRE, 61 WELLFIELD ROAD, UK CF24 3DG</Text></View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={[driverorderhistorystyle.notificationlist, styles.mb_15, styles.commonPadding, styles.brd5, {
                                            backgroundColor: Color.whiteColor, marginBottom: 20
                                        }]}>
                                            <View style={[driverorderhistorystyle.ntflex1, styles.mb_15]}>
                                                <View>
                                                    <View><Text style={[driverorderhistorystyle.nthead1,]}>Order # 15632</Text></View>
                                                    <View><Text style={driverorderhistorystyle.nttext1}>10 July 2021   |   5.30 pm</Text></View>
                                                </View>
                                                <View>
                                                    <View><Text style={[driverorderhistorystyle.nttext, { marginBottom: 5, textAlign: 'right' }]}>2 Items</Text></View>
                                                    <View><Text style={[driverorderhistorystyle.nttext, { textAlign: 'right' }]}>Amount : £120</Text></View>
                                                </View>
                                            </View>
                                            <View style={driverorderhistorystyle.ntdetails}>
                                                <View style={{ width: "70%" }}>
                                                    <View><Text style={[driverorderhistorystyle.nthead1]}>DANIEL REX</Text></View>
                                                    <View><Text style={driverorderhistorystyle.nttext1}>Delivery Address : THE BUSINESS CENTRE, 61 WELLFIELD ROAD, UK CF24 3DG</Text></View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={[driverorderhistorystyle.notificationlist, styles.mb_15, styles.commonPadding, styles.brd5, {
                                            backgroundColor: Color.whiteColor, marginBottom: 20
                                        }]}>
                                            <View style={[driverorderhistorystyle.ntflex1, styles.mb_15]}>
                                                <View>
                                                    <View><Text style={[driverorderhistorystyle.nthead1,]}>Order # 15632</Text></View>
                                                    <View><Text style={driverorderhistorystyle.nttext}>10 July 2021   |   5.30 pm</Text></View>
                                                </View>
                                                <View>
                                                    <View><Text style={[driverorderhistorystyle.nttext, { marginBottom: 5, textAlign: 'right' }]}>2 Items</Text></View>
                                                    <View><Text style={[driverorderhistorystyle.nttext, { textAlign: 'right' }]}>Amount : £120</Text></View>
                                                </View>
                                            </View>
                                            <View style={driverorderhistorystyle.ntdetails}>
                                                <View style={{ width: "70%" }}>
                                                    <View><Text style={[driverorderhistorystyle.nthead1]}>DANIEL REX</Text></View>
                                                    <View><Text style={driverorderhistorystyle.nttext1}>Delivery Address : THE BUSINESS CENTRE, 61 WELLFIELD ROAD, UK CF24 3DG</Text></View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>


                        </ScrollView>
                    </View>
                </SafeAreaView >
            </>
        )
    }
}


export default DriverOrderHistoryScreen;
