import React, { Component } from "react";
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  Switch,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import styles from "../styles/main.style";
import pagestyles from "../styles/welcome.style";
// import pagestyles from '../styles/home.style';
import loginpage from "../styles/login.style";
import driverdashboardstyle from "../styles/driverdashboard.style";
import { Actions } from "react-native-router-flux";
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from "../utils/FlushMsg";
import { LoginService } from "../services";
import { OrderService } from "../services";
import Loader from "../utils/Loader";
//import { Constants, Location, Permissions } from 'expo';
import { connect } from "react-redux";
import {
  addToCart,
  selectStadium,
  getSelectedStadiumData,
  selectStadiumDetailsData,
  getSelectedStadiumDetailsData,
} from "../actions/cartActions";
import { EventRegister } from "react-native-event-listeners";
// import GetLocation from 'react-native-get-location'
import {
  MainContainer,
  Button,
  TopHeader,
  ImageCustom, DeclineResonScreen
} from "../components";
import { Color, Images, Dimen, Fonts, GlobalFunction } from "../utils";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Geolocation from "@react-native-community/geolocation";
import Geocoder from "react-native-geocoding";
import Permissions, {
  PERMISSIONS,
  openSettings,
} from "react-native-permissions";
import DatePicker from "react-native-datepicker";
import { parse } from "@babel/core";

class ReportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loginUserData: JSON.parse(global.loginUserData),
      reportData: { "accepted_order_count": "0", "past_order_count": "0", "rejected_order_count": "0", "total_earning": 0 },
      cancel_reason: '',
      isShowReasonBox: false,
      activeStatus:
        JSON.parse(global.loginUserData).login_status == "1" ? true : false,
      cDate: new Date(),
      fDate: '',
      tDate: ''
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({
        loginUserData: JSON.parse(global.loginUserData),
      });
      // The screen is focused
      // Call any action
      this.getCurrentOrder();

    });

  }

  componentWillUnmount() {
  }

  callGetReport = () => {
    if (this.state.fDate != "" && this.state.tDate != "") {
      this.getCurrentOrder()
    } else {
      FlushMsg.showError('Please select From and To date to get your report.');
    }
  }

  getCurrentOrder = () => {
    this.loaderShowHide(true);
    var myData = {
      "driverID": this.state.loginUserData.driverID,
      "tdate": this.state.tDate,
      "fdate": this.state.fDate,
    };
    OrderService.getReport(myData).then(
      function (result) {
        this.loaderShowHide(false);
        if (result.status) {
          this.setState({
            reportData: result?.data,
          });
        } else {
          FlushMsg.showError(result.message);
        }
        console.log("result:: ", result);
      }.bind(this),
      function (result) {
        console.log("result:: ", result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this)
    );


  };

  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };


  renderHeaderShop = () => {
    return (
      <View>
        <View style={loginpage.mainView}>
          <View style={[styles.HeaderLeft, styles.HeaderLeftBackButton]}>
            <TouchableOpacity
              style={{}}
              activeOpacity={0.5}
              onPress={() => {
                Actions.pop();
              }}
            >
              <Image source={Images.leftArrow} />
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={[
                loginpage.headingtext,
                {
                  fontSize: 18,
                  marginTop: 4,
                },
              ]}
            >
              Home
            </Text>
          </View>
        </View>
      </View>
    );
  };

  openReasonBox = () => {
    this.setState({
      isShowReasonBox: true,
    });
  };

  closeReasonBox = () => {
    this.setState({
      isShowReasonBox: false,
    });
  };

  submitReasonBox = (info) => {
    console.log("info:: ", info);
    this.setState(
      {
        isShowReasonBox: false,
        cancel_reason: info?.cancel_reason
      },
      () => {
        this.acceptOrder("2");
      }
    );
  };

  getPreviousTwoYear = () => {
    const d = new Date();
    d.setFullYear(d.getFullYear() - 18);

    console.log(d.toDateString());
    return d;
  };

  render() {
    const { reportData, loginUserData } = this.state;
    console.log(
      "this.state.loginUserData.profile_photo:: ",
      this.state.loginUserData.profile_photo
    );
    return (
      <>
        <MainContainer>
          <Loader loading={this.state.loading} />
          {
            (this.state.isShowReasonBox) && <DeclineResonScreen
              closeReasonBox={this.closeReasonBox.bind(this)}
              submitReasonBox={this.submitReasonBox.bind(this)}
            />
          }
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={pagestyles.Wrapper}>
              <View style={pagestyles.Container}>
                <TopHeader title={"My Report"}
                  onPress={() => {
                    Actions.pop();
                  }}
                />
              </View>

              <View style={pagestyles.Container}>

                <View>
                  <View>
                    <View style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around'
                    }}>
                      <View
                        style={{
                          // borderBottomWidth: 0.5,
                          // borderBottomColor: "lightgrey",
                          marginBottom: 5,
                          // backgroundColor: 'red',
                          width: '45%'
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: Fonts.regular,
                            fontSize: 14,
                            marginBottom: 5,
                            marginTop: 15,
                            color: Color.greyColor,
                          }}
                        >
                          From Date
                        </Text>
                        <DatePicker
                          style={{ width: "100%", marginBottom: 5 }}
                          date={this.state.fDate} //initial date from state
                          mode="date" //The enum of date, date time and time
                          //androidMode="spinner"
                          placeholder="Select Date"
                          format="YYYY-MM-DD"
                          minDate={
                            new Date(
                              new Date().setFullYear(
                                new Date().getFullYear() - 100
                              )
                            )
                          }
                          maxDate={
                            new Date()
                            //this.getPreviousTwoYear()
                          }
                          // maxDate={
                          //   new Date()
                          //   // new Date().setFullYear(
                          //   //   new Date().getFullYear() + 100,
                          //   // ),
                          // }
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          customStyles={{
                            dateIcon: {
                              position: "absolute",
                              right: 0,
                              top: 4,
                              marginLeft: 0,
                            },
                            dateInput: {
                              // marginLeft: 36,
                              // marginTop:20
                            },
                            // ... You can check the source to find the other keys.
                          }}
                          // customStyles={{
                          //   dateIcon: {
                          //     height: 32,
                          //     position: 'absolute',
                          //     left: 0,
                          //     top: 0,
                          //     marginLeft: 0,
                          //   },
                          //   dateInput: {
                          //     height: 60,
                          //     alignItems: 'flex-start',
                          //     justifyContent: 'center',
                          //     color: ThemeColors.blackColor,
                          //     borderColor: '#99a0ae',
                          //     borderWidth: 1,
                          //     borderRadius: 30,
                          //     fontSize: 16,
                          //     paddingLeft: 20,
                          //     paddingRight: 20,
                          //   },
                          //   placeholderText: {
                          //     color: ThemeColors.primaryColor,
                          //     fontSize: 16,
                          //   },
                          // }}
                          onDateChange={(date) => {
                            console.log("chked Date::", date);
                            this.setState({ fDate: date }, () => {
                              console.log("fDate ", this.state.fDate);
                            });
                          }}
                        />

                      </View>
                      <View
                        style={{
                          // borderBottomWidth: 0.5,
                          // borderBottomColor: "lightgrey",
                          marginBottom: 5,
                          // backgroundColor: 'blue',
                          width: '45%'
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: Fonts.regular,
                            fontSize: 14,
                            marginBottom: 5,
                            marginTop: 15,
                            color: Color.greyColor,
                          }}
                        >
                          To Date
                        </Text>
                        <DatePicker
                          style={{ width: "100%", marginBottom: 5 }}
                          date={this.state.tDate} //initial date from state
                          mode="date" //The enum of date, date time and time
                          //androidMode="spinner"
                          placeholder="Select Date"
                          format="YYYY-MM-DD"
                          minDate={
                            (this.state.fDate != "") ?
                              new Date(this.state.fDate) :
                              new Date(
                                new Date().setFullYear(
                                  new Date().getFullYear() - 100
                                )
                              )
                          }
                          maxDate={
                            new Date()
                            //this.getPreviousTwoYear()
                          }
                          // maxDate={
                          //   new Date()
                          //   // new Date().setFullYear(
                          //   //   new Date().getFullYear() + 100,
                          //   // ),
                          // }
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          customStyles={{
                            dateIcon: {
                              position: "absolute",
                              right: 0,
                              top: 4,
                              marginLeft: 0,
                            },
                            dateInput: {
                              // marginLeft: 36,
                              // marginTop:20
                            },
                            // ... You can check the source to find the other keys.
                          }}
                          // customStyles={{
                          //   dateIcon: {
                          //     height: 32,
                          //     position: 'absolute',
                          //     left: 0,
                          //     top: 0,
                          //     marginLeft: 0,
                          //   },
                          //   dateInput: {
                          //     height: 60,
                          //     alignItems: 'flex-start',
                          //     justifyContent: 'center',
                          //     color: ThemeColors.blackColor,
                          //     borderColor: '#99a0ae',
                          //     borderWidth: 1,
                          //     borderRadius: 30,
                          //     fontSize: 16,
                          //     paddingLeft: 20,
                          //     paddingRight: 20,
                          //   },
                          //   placeholderText: {
                          //     color: ThemeColors.primaryColor,
                          //     fontSize: 16,
                          //   },
                          // }}
                          onDateChange={(date) => {
                            console.log("chked Date::", date);
                            this.setState({ tDate: date }, () => {
                              console.log("tDate ", this.state.tDate);
                            });
                          }}
                        />

                      </View>
                    </View>
                    <Button
                      style={[driverdashboardstyle.signSubmit]}
                      text={"Get Report"}
                      onLoading={false}
                      onPress={() => {
                        // Actions.driverdashboard3screen()
                        this.callGetReport();
                      }}
                    />
                  </View>
                </View>
                <View style={{
                  marginTop: 15,
                  padding: 15,
                  backgroundColor: Color.whiteColor,
                  borderRadius: 10
                }}>
                  <Text style={{
                    fontSize: 16,
                    marginBottom: 10
                  }}>Report of {(this.state.fDate != "" && this.state.tDate != "") ?
                    GlobalFunction.formatDate(this.state.fDate) + ' to ' + GlobalFunction.formatDate(this.state.tDate)
                    : GlobalFunction.formatDate(this.state.cDate)}</Text>
                  <View>
                    <View style={{
                      flexDirection: 'row',
                      padding: 10,
                      paddingLeft: 0
                    }}>
                      <Text style={{
                        fontSize: 18,
                        fontWeight: 'bold'
                      }}>Total Order: </Text>
                      <Text style={{
                        fontSize: 18,
                      }}>{reportData?.past_order_count}</Text>
                    </View>
                    <View style={{
                      flexDirection: 'row',
                      padding: 10,
                      paddingLeft: 0
                    }}>
                      <Text style={{
                        fontSize: 18,
                        fontWeight: 'bold'
                      }}>Completed Order: </Text>
                      <Text style={{
                        fontSize: 18,
                      }}>{reportData?.accepted_order_count}</Text>
                    </View>
                    <View style={{
                      flexDirection: 'row',
                      padding: 10,
                      paddingLeft: 0
                    }}>
                      <Text style={{
                        fontSize: 18,
                        fontWeight: 'bold'
                      }}>Declined Order: </Text>
                      <Text style={{
                        fontSize: 18,
                      }}>{reportData?.rejected_order_count}</Text>
                    </View>
                    <View style={{
                      flexDirection: 'row',
                      padding: 10,
                      paddingLeft: 0
                    }}>
                      <Text style={{
                        fontSize: 18,
                        fontWeight: 'bold'
                      }}>Total Earned: </Text>
                      <Text style={{
                        fontSize: 18,
                      }}>£{parseFloat(reportData?.total_earning).toFixed(2)}</Text>
                    </View>

                  </View>
                </View>

              </View>
            </View>
          </ScrollView>
        </MainContainer>
      </>
    );
  }
}

// export default ReportScreen;

const mapStateToProps = (state) => {
  console.log("statestatestate::: ", state);
  return {
    items: state.carts.items,
    addedItems: state.carts.addedItems,
    totalCartPrice: state.carts.totalCartPrice,
    totalCartQty: state.carts.totalCartQty,
    selectedStadium: state.carts.selectStadium,
    selectStadiumDetails: state.carts.selectStadiumDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (data) => {
      dispatch(addToCart(data));
    },
    selectStadium: (data) => {
      dispatch(selectStadium(data));
    },
    // removeToCart: (data) => { dispatch(removeToCart(data)) },
    // getCartData: (data) => { dispatch(getCartData(data)) }, // ()=>{dispatch({type: 'GET_CART'})},
    getSelectedStadiumData: (data) => {
      dispatch(getSelectedStadiumData(data));
    },
    selectStadiumDetailsData: (data) => {
      dispatch(selectStadiumDetailsData(data));
    },
    getSelectedStadiumDetailsData: (data) => {
      dispatch(getSelectedStadiumDetailsData(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportScreen);
