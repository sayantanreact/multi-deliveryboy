import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert, ImageBackground,
    ActivityIndicator, Modal, Animated, Easing,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView, PermissionsAndroid
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/home.style';
import { Actions } from 'react-native-router-flux';
import { Images, Dimen, Fonts, Color } from '../utils'
import AsyncStorage from "@react-native-async-storage/async-storage";

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            spinAnim: new Animated.Value(0),
            bounceValue: new Animated.Value(300),
            bounceValueImageView: new Animated.Value(-300),
        }
        this.animatedValue = new Animated.Value(180);
        this.currentValue = 180;

        this.animatedValue.addListener(({ value }) => {
            this.currentValue = value;
        });
    }


    componentDidMount() {
        setTimeout(() => {
            this.imageViewAnimation()
        }, 500);
    }

    imageViewAnimation = () => {
        Animated.spring(
            this.state.bounceValueImageView,
            {
                toValue: 1,
                velocity: 3,
                tension: 2,
                friction: 5,
                useNativeDriver: true
            }
        ).start(() => {
            this.roatedImage()
        });
    }

    roatedImage = () => {
        Animated.timing(
            this.state.spinAnim,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: true
            }
        ).start(() => {
            //this.bottomAnimation()
            // Actions.reset("signinscreen")
            this.gotoScreen()
        });
    }

    flipAnimation = () => {
        console.log("this.currentValue:: ", this.currentValue)
        if (this.currentValue >= 90) {
            Animated.spring(this.animatedValue, {
                toValue: 0,
                tension: 10,
                friction: 8,
                useNativeDriver: false,
            }).start(() => {
                //this.flipAnimation()
            });
        } else {
            Animated.spring(this.animatedValue, {
                toValue: 180,
                tension: 10,
                friction: 8,
                useNativeDriver: false,
            }).start(() => {
                //this.flipAnimation()
            });
        }
    };

    bottomAnimation = () => {
        console.log("bottomAnimation")
        Animated.spring(
            this.state.bounceValue,
            {
                toValue: 1,
                // velocity: 3,
                // tension: 2,
                // friction: 8,
                useNativeDriver: true,
                easing: Easing.bounce,
            }
        ).start(() => {
            this.gotoScreen()
        });
    }

    gotoScreen = () => {
        AsyncStorage.getItem("@multiDRIVERData")
            .then((value) => {
                //console.log('value:: ',value);
                if (value !== null) {
                    // value previously stored
                    console.log("value:: ", value);
                    //let v = value.replace("\\", "");
                    global.loginUserData = value;
                    Actions.reset('drawer')

                } else {
                    //console.log('value:: ',value);
                    Actions.reset("signinscreen")
                }
                //console.log('this.state:: ',this.state);
            })
            .catch((error) => {
                //console.log('error:: ', error);
                //alert('ERROR GETTING DATA FROM FACEBOOK')
                Actions.reset("signinscreen")
            });
    }

    render() {
        const spin = this.state.spinAnim.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });
        const setInterpolate = this.animatedValue.interpolate({
            inputRange: [0, 180],
            outputRange: ['180deg', '360deg'],
        });

        const rotateYAnimatedStyle = {
            transform: [{ rotateY: setInterpolate }],
        };
        return (
            <>
                <View style={{
                    width: Dimen.width,
                    height: Dimen.height,
                    //backgroundColor: 'red'
                }}>
                    <ImageBackground
                        source={Images.splashImage}
                        style={{
                            width: Dimen.width,
                            height: Dimen.height
                        }}
                    >

                        <Animated.View style={{
                            marginTop: 100,
                            width: Dimen.width,
                            alignItems: 'center',
                            transform: [{ translateY: this.state.bounceValueImageView }]
                        }}>
                            {/* <Image
                                source={Images.logo}
                            //style={{ width: 124, height: 124 }} 
                            /> */}
                            <Animated.Image
                                style={{
                                    // height: 100,
                                    //  width: 100, 
                                    transform: [{
                                        rotate: spin
                                        // rotateY: setInterpolate
                                    }]
                                }}
                                source={Images.logo} />
                        </Animated.View>
                        {/* <Animated.View style={{
                            marginTop: 350,
                            width: Dimen.width,
                            alignItems: 'center',
                            transform: [{ translateY: this.state.bounceValue }]
                        }}>
                            <Text style={{
                                fontFamily: Fonts.regular,
                                fontSize: 40,
                                color: '#FF7900'
                            }}>Groceries</Text>
                            <Text style={{
                                fontFamily: Fonts.regular,
                                fontSize: 40,
                                color: '#FF7900'
                            }}>&</Text>
                            <Text style={{
                                fontFamily: Fonts.regular,
                                fontSize: 40,
                                color: '#FF7900'
                            }}>Food</Text>
                            <Text style={{
                                fontFamily: Fonts.regular,
                                fontSize: 40,
                                color: '#FFF'
                            }}>On Demand</Text>
                        </Animated.View> */}
                    </ImageBackground>
                </View>
            </>
        )
    }
}


export default SplashScreen;

