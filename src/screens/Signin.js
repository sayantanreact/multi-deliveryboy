import React, { Component } from 'react';
import {
  Platform, Text, View, Image, Alert,
  ActivityIndicator, ImageBackground,
  TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';
import pagestyles from '../styles/welcome.style';
import signinstyles from '../styles/signin.style';
import loginpage from '../styles/login.style';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
//import Permissions from 'react-native-permissions';
// import Permissions, { PERMISSIONS, openSettings } from 'react-native-permissions';
// import Geocoder from 'react-native-geocoding';
// import Geolocation from '@react-native-community/geolocation';
// // import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import FlushMsg from '../utils/FlushMsg';
import { Apis } from '../utils/Apis';
import Loader from '../utils/Loader';
//import { Constants, Location, Permissions } from 'expo';
import { AuthInputBoxSec, Button, MainContainer } from '../components'
// import { Images, Dimen, Fonts, Color } from '../utils'
import Styles from '../utils/CommonStyles';
import { Images, Color, Fonts } from '../utils';
import { LoginService } from '../services';

// import { FlushMsg } from '../utils'

class SigninScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      phoneNo: '',
    };
  }
  componentDidMount() { }
  loginOnPressBtn = () => {
    this.loginAction();
  };
  loaderShowHide = (status) => {
    this.setState({
      loading: status,
    });
  };
  loginAction = () => {
    if (this.state.phoneNo == '') {
      FlushMsg.showError('Please enter your phone no');
    } else if (!this.state.phoneNo.startsWith(0)) {
      FlushMsg.showError('Please Add 0 before phone Number');
    } else {
      Keyboard.dismiss();
      this.loginApi();
    }
  };
  loginApi = () => {
    this.loaderShowHide(true);
    var myData = {
      username: this.state.phoneNo,
    };
    LoginService.loginDriver(myData).then(
      function (result) {
        this.loaderShowHide(false);
        console.log('result:: ', result);
        if (result.status) {
          if (result.data.work_in_uk_proof == "" && result.data.venderID == "") {
            global.loginUserData = JSON.stringify(result.data);
            Actions.reset('driverprofile', {
              isPage: 'detail',
            });
          } else {
            Actions.otpscreen({
              data: result.data,
              isPage: 'login',
            });
          }
        } else {
          FlushMsg.showError(result.message);
          if (result.data.registered == false) {
            Actions.signupscreen({
              data: result.data,
              phoneNo: this.state.phoneNo,
              isPage: 'login',
            });
          }
        }
      }.bind(this),
      function (result) {
        console.log('result:: ', result);
        //console.log('There was an error fetching the time');
        this.loaderShowHide(false);
        FlushMsg.showError(result.message);
      }.bind(this),
    );
  };

  renderHeaderShop = () => {
    return (
      <View>
        <View style={styles.Header}>
          <View style={styles.HeaderLeft}>
            <TouchableOpacity style={{}} activeOpacity={0.5}>
              <Image source={Images.leftArrow} />
            </TouchableOpacity>
          </View>
          <View style={styles.HeaderMiddle}>
            <Text style={Styles.Heading}>&nbsp;</Text>
          </View>
          <View style={styles.HeaderRight}>
            <Text>&nbsp; </Text>
          </View>
        </View>
      </View>
    );
  };
  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */
  render() {
    return (
      <>
        <MainContainer>
          {/* <Loader loading={this.state.apiLoader} /> */}
          <View>
            <ScrollView
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps={'handled'}
            >
              <View style={signinstyles.signinMain}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 30,
                  }}
                >
                  <Image source={Images.deliveryBoy} />
                </View>
                <View
                  style={[
                    signinstyles.signinWrapper,
                    signinstyles.boxWithShadow,
                  ]}
                >
                  <View>
                    {/* <TouchableOpacity
                                            style={signinstyles.signinNo1}
                                            activeOpacity={0.5}
                                        >
                                            <Image
                                                style={signinstyles.mail1}
                                                source={Images.email}
                                            />
                                            <Text style={[signinstyles.countryNo, styles.TextCenter]}>
                                                Continue with Email
                                            </Text>
                                        </TouchableOpacity> */}

                    <Button
                      customStyles={{
                        mainContainer: {
                          backgroundColor: Color.whiteColor,
                        },
                        buttonText: {
                          color: Color.blackColor,
                          fontSize: 16,
                          fontFamily: Fonts.regular,
                        },
                        img: signinstyles.mail1,
                      }}
                      text={'Continue with Email'}
                      image={Images.email}
                      onLoading={false}
                      onPress={() => {
                        Actions.loginscreen();
                      }}
                    />
                  </View>

                  {/* <View>
                                            <TextInput  style={[signinstyles.signSubmit]}
                                                    value="Send OTP" 
                                                /> 
                                        </View> */}
                  <View style={signinstyles.orholder}>
                    <Text style={signinstyles.or}>OR</Text>
                  </View>

                  <View>
                    <View style={signinstyles.signinNo}>
                      {/* <Text style={signinstyles.countryCode}>+0</Text> */}
                      <TextInput
                        style={signinstyles.countryNo}
                        placeholder="Phone Number"
                        placeholderTextColor={Color.greyColor}
                        maxLength={11}
                        keyboardType={'number-pad'}
                        onChangeText={(text) => this.setState({ phoneNo: text })}
                        value={this.state.phoneNo}
                        autoCapitalize="none"
                        returnKeyType="done"
                        onSubmitEditing={() => {
                          this.loginOnPressBtn();
                        }}
                      />
                    </View>
                    <View>
                      <Button
                        customStyles={{
                          // mainContainer: {
                          //     backgroundColor: Color.whiteColor,
                          // },
                          buttonText: {
                            color: Color.whiteColor,
                            fontSize: 18,
                            fontFamily: Fonts.semibold,
                          },
                          //img: signinstyles.mail1
                        }}
                        text={'Send OTP'}
                        onLoading={this.state.loading}
                        onPress={() => {
                          this.loginOnPressBtn();
                        }}
                      />
                    </View>
                  </View>

                  <View style={loginpage.conditiontextView}>
                    <Text style={loginpage.insideText}>
                      By continuing, you agree to our
                      <Text
                        onPress={() => {
                          // Actions.privacyscreen();
                          Actions.cmsscreen({
                            title: 'terms-conditions',
                            heading: 'Terms & Conditions',
                          });
                        }}
                        style={loginpage.termText}
                      >
                        {' '}
                        Terms & Conditions{' '}
                      </Text>
                      and
                      <Text
                        onPress={() => {
                          // Actions.privacyscreen();
                          Actions.cmsscreen({
                            title: 'privacy-policy',
                            heading: 'Privacy Policy',
                          });
                        }}
                        style={loginpage.policyText}
                      >
                        {' '}
                        Privacy Policy.
                      </Text>
                    </Text>
                  </View>

                  {/* <View style={loginpage.conditiontextView}>
                    <Text style={loginpage.insideText}>
                      By continuing, you agree to our
                    </Text>
                    <Text
                      onPress={() => {
                        // Actions.termsconditionscreen();
                        Actions.cmsscreen({
                          title: 'terms-conditions',
                          heading: 'Terms & Conditions',
                        });
                      }}
                      style={loginpage.termText}
                    >
                      Terms of
                    </Text>
                  </View>
                  <View style={loginpage.secView}>
                    <Text style={loginpage.serviceText}>Service</Text>
                    <Text style={loginpage.andText}>and</Text>
                    <Text
                      onPress={() => {
                        // this.setState({
                        //   privacypolicyVisible: true,
                        // });
                        Actions.cmsscreen({
                          title: 'privacy-policy',
                          heading: 'Privacy Policy',
                        });
                      }}
                      style={loginpage.policyText}
                    >
                      Privacy Policy.
                    </Text>
                  </View> */}

                  {/* <View >
                                            <Text style={styles.termText}> By continuing, you agree to our Terms of Service and Privacy Policy. </Text>
                                        </View> */}
                </View>
              </View>
            </ScrollView>
          </View>
        </MainContainer>
      </>
    );
  }
}


export default SigninScreen;
