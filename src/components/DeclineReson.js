/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
import React, { Component } from "react";
import {
  View, Modal, Image, Text, Keyboard,
  TouchableOpacity, Dimensions, TextInput
} from "react-native";
import FlushMsg from "../utils/FlushMsg";
import styles, { ThemeColors } from "../styles/main.style";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
//import { Constants, Location, Permissions } from 'expo';
import { Fonts, Color } from "../utils";
let ScreenHeight = Dimensions.get("window").height;
import { MainContainer, AuthInputBoxSec, Button } from "./";

class DeclineResonScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      listData: [],
      isVisible: true,
      cancel_reason: '',
      error: false
      // loginUserData: JSON.parse(global.loginUserData),
    };
  }

  componentDidMount() {
    // this.locationRef.focus()
  }

  closeModalAction = () => {
    this.props.closeReasonBox({
      ivrListVisible: false,
    });
  };

  loginAction = () => {
    if (this.state.cancel_reason == "") {
      FlushMsg.showError("Please enter a reason for decline order");
      this.setState({
        error: true
      })
    } else {
      Keyboard.dismiss();
      this.props.submitReasonBox({
        cancel_reason: this.state.cancel_reason,
      });
    }
  };

  /**
   * render() this the main function which used to display different view
   * and contain all view related information.
   */

  render() {
    return (
      <Modal
        propagateSwipe={50}
        animationType="fade"
        transparent={true}
        visible={true}
        coverScreen={true}
        onRequestClose={() => { }}
      >
        {/* <ScrollView
                    scrollEnabled={enableScrollViewScroll}
                    showsVerticalScrollIndicator={false}> */}
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <View
            style={{
              margin: 0,
              backgroundColor: ThemeColors.whiteColor,
              borderRadius: 6,
              padding: 15,
              paddingTop: 40,
              width: "100%",
              height: ScreenHeight,
              flex: 1,
            }}
          >
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                position: "relative",
                right: 0,
                top: 0,
                zIndex: 1,
                justifyContent: "center",
                alignContent: "center",
                textAlign: "center",
              }}
              onPress={() => {
                this.closeModalAction(false);
              }}
            >
              <Image
                source={require("../assets/images/close.png")}
                style={{ width: 30, height: 30 }}
              />
            </TouchableOpacity>

            <View
              style={{
                flex: 1,
                backgroundColor: "#fff",
                padding: 10,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{
                textAlign: 'left',
                width: '100%',
                color: Color.greyColor,
                marginTop: 20
              }}>
                Please enter a reason for decline order
              </Text>
              <View
                style={{
                  color: "#0137FF",
                  borderColor: ThemeColors.darkgreyColor,
                  borderWidth: 1.5,
                  // borderRadius: 22,
                  marginBottom: 10,
                  backgroundColor: ThemeColors.whiteColor,
                  paddingLeft: 15,
                  paddingRight: 15,
                  marginTop: 20,
                  width: '100%'
                }}
              >
                <TextInput
                  style={{
                    width: '100%',
                    height: 150,
                    justifyContent: 'flex-start',
                    textAlignVertical: 'top',
                    color: '#000',
                    borderBottomWidth: 0,
                  }}
                  // placeholder={'YOUR REVIEW'}
                  underlineColorAndroid="transparent"
                  onChangeText={text => {
                    this.setState({
                      cancel_reason: text,
                      error: false
                    })
                  }
                  }
                  returnKeyType="done"
                  //numberOfLines={10}
                  multiline={true}
                  //numberOfLines={4}
                  placeholderTextColor="#000"
                  onSubmitEditing={() => {
                    this.loginAction()
                  }}
                />

              </View>
              {
                (this.state.error) &&
                <Text style={{
                  color: 'red',
                  marginTop: 10,
                  textAlign: 'left',
                  width: '100%',
                }}>Please enter the reson</Text>
              }
              <View style={{ width: '100%', marginTop: 20 }}>
                <Button
                  text={"Submit & Decline"}
                  onLoading={this.state.loading}
                  customStyles={
                    {
                      // mainContainer: styles.butonContainer
                    }
                  }
                  onPress={() => this.loginAction()}
                />
              </View>
            </View>
          </View>
        </View>
        {/* </ScrollView> */}
      </Modal>
    );
  }
}

export default DeclineResonScreen;
