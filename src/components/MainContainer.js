import React, { Component } from 'react';
import {
    Platform, Text, View, Image, Alert,
    ActivityIndicator,
    TouchableOpacity, TextInput, SafeAreaView, FlatList, Keyboard, ScrollView
} from 'react-native';
import styles, { ThemeColors } from '../styles/main.style';

export default function MainContainer(props) {
    return (
        <>
            <SafeAreaView style={{
                flex: 0,
                backgroundColor: ThemeColors.primaryColor,
            }} />
            <SafeAreaView>
                { props.children }
            </SafeAreaView>
        </>
    )
}