import React from 'react';
import {
    TextInput,
    View,
    Image,

    StyleSheet,
    Dimensions,
    Text,
    ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { hp, wp } from '../utils/responsive';
import { RF } from '../utils/responsive';
import Fonts, { fonts, fontSizes } from '../utils/Fonts';
import { Color } from '../utils';
const { height, width } = Dimensions.get('window');

const AuthInputBoxSec = ({ lableText, lblTxtInfo, icon, iconName, iconPressAction, name, disableImg, address, mainContainer,
    inputLayout,
    imgView,
    imgStyle,
    inputFieldStyle,
    refs,
    inputRef,
    showCode, iconColor, isMandatoy = false,
    ...props
}) => {

    //console.log(props.onSubmitEditing)
    return (

        <>
            <View style={[styles.mainContainer, mainContainer]}>

                {
                    (lableText) ?
                        <Text style={{
                            fontFamily: Fonts.regular,
                            fontSize: 14,
                            marginBottom: 5,
                            marginTop: 15,
                            color: Color.greyColor
                        }}>
                            {
                                (isMandatoy) && <Text style={{
                                    color: 'red'
                                }}>*</Text>
                            }{lableText} {(lblTxtInfo) ? '(' + lblTxtInfo + ')' : ''}</Text> :
                        null
                }
                <View
                    style={[
                        styles.inputLayout,
                        {
                            width: disableImg ? '90%' : '100%',
                        },
                        inputLayout,
                    ]}>
                    {
                        (showCode) ?
                            <Text style={{
                                // position: 'absolute',
                                // top: '50%',
                                // left: 0,
                                marginTop: 11.3,
                                marginRight: 10,
                                color: 'black', //"#7C7C7C",
                                fontFamily: Fonts.regular,
                                fontSize: 13
                            }}>+0</Text> : null
                    }

                    <TextInput
                        style={[styles.inputFieldStyle, inputFieldStyle]}
                        //ref={input => refs && props.refs(input)}
                        // ref={(input) => { refs = input; }}
                        // ref={refs}
                        // ref={(r) => { refs && refs(r) }}
                        ref={(r) => { inputRef && inputRef(r) }}
                        //onSubmitEditing = {props.onSubmitEditing}
                        placeholder={name}
                        placeholderTextColor="grey"
                        // editable={editable}
                        autoCapitalize='none'
                        {...props}
                        value={props.value}
                    />

                    {disableImg ? (
                        // <View style={[styles.imgView, imgView]}>
                        //     <Image
                        //         style={[styles.img, imgStyle]}
                        //         resizeMode="contain"
                        //         source={icon}
                        //     />
                        // </View>
                        <Icon
                            style={{
                                position: 'absolute',
                                top: 6,
                                right: -35,
                                color: (iconColor) ? iconColor : "#7C7C7C"
                            }}
                            name={iconName}
                            size={25}
                            onPress={iconPressAction}
                        />
                    ) : null}

                    {/* <TouchableOpacity style={[styles.textStyle, textStyle]} {...props}>
            {loading ?
              <ActivityIndicator color="white" {...props} size={'small'} />
              :
              <Text style={{ color: THEME_WHITE }}>{buttin}</Text>
            }
          </TouchableOpacity> */}


                </View>
            </View>
            {/* {error ? (
                <View style={[styles.errorLayout, errorLayout]}>
                    <Text
                        ellipsizeMode="tail"
                        numberOfLines={3}
                        style={[styles.errorTxt]}>
                        {error}
                    </Text>
                </View>
            ) : null} */}
        </>
    )
}
// };
AuthInputBoxSec.defaultProps = { mainContainer: {} }

const styles = StyleSheet.create({
    textStyle: {
        backgroundColor: "gray",
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContainer: {
        width: "90%",
        //paddingLeft: 10,
        //paddingRight: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        paddingVertical: 5,
        justifyContent: "center",
        alignSelf: "center",
        marginBottom: 10
    },
    imgView: {
        width: '15%',
        alignItems: 'center',
    },
    img: {
        height: 30,
        width: 30,
    },
    inputLayout: {
        width: '100%',
        // flexDirection: 'row',
        // justifyContent: 'space-between',
    },
    inputFieldStyle: {
        width: "100%",
        height: 40,
        paddingVertical: 5,
        fontSize: RF(14),
        color: Color.blackColor,
        fontFamily: Fonts.regular,
        // backgroundColor: 'red'
    },
    errorLayout: {
        backgroundColor: "red",
        marginVertical: 6,
        borderRadius: 5,
        padding: 5,
    },
    errorTxt: {
        color: "white",
        fontWeight: 'bold',
        fontSize: RF(12),
    },
});
export default AuthInputBoxSec