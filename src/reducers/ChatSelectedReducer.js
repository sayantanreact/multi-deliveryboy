import { SELECT_CHAT } from "../actions/types";

export default (state = null, action) => {
    console.log(action);
    switch (action.type) {
        case SELECT_CHAT:
            return action.payload;
        default:
            return state;
    } 
};