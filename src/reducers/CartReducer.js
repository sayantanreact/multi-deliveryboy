// import Item1 from '../../images/item1.jpg'
// import Item2 from '../../images/item2.jpg'
// import Item3 from '../../images/item3.jpg'
// import Item4 from '../../images/item4.jpg'
// import Item5 from '../../images/item5.jpg'
// import Item6 from '../../images/item6.jpg'
import { SET_SELECTED_STADIUM_DETAILS, GET_SELECT_STADIUM_DETAILS, SET_SELECTED_STADIUM, GET_SELECT_STADIUM, GET_CART, ADD_TO_CART, REMOVE_TO_CART, SUB_QUANTITY, ADD_QUANTITY, ADD_SHIPPING } from '../actions/action-types/cart-actions'


const initState = {
    addedItems: [],
    totalCartPrice: 0,
    totalCartQty: 0,
    deliveryCharge: 0,
    deliveryKm: 0,
    selectStadium: {},
    selectStadiumDetails: {}
}
const CartReducer = (state = initState, action) => {

    // console.log('CartReducerstatestate:: ', state);
    // console.log('CartReduceractionaction:: ', action);

    //INSIDE HOME COMPONENT
    if (action.type === GET_CART) {
        console.log('GET_CARTGET_CART:: ', state);
        return {
            ...state,
            // addedItems: [...state.addedItems, cartData],
            // totalCartPrice: newTotal
            // addedItems: [...state.addedItems],
            // totalCartPrice: state.totalCartPrice,
            // totalCartQty: state.totalCartQty
        }
    }
    if (action.type === ADD_TO_CART) {
        //check if the action id exists in the addedItems
        console.log('ADD_TO_CART.state:: ', state);
        console.log('state.addedItems1:: ', state.addedItems);
        console.log('action.data:: ', action.data);
        const cartData = (action.data.cart_list != undefined) ? action.data.cart_list : [] //action.data.cart_list

        //calculating the total
        let newTotal = action.data.cart_total //parseFloat(state.total) + parseFloat(cartData.price)
        let newTotalQty = action.data.total_qty
        let deliveryCharge = action.data.delivery_charges
        let deliveryKm = action.data.delivery_km
        console.log('newtotalCartPrice:: ', newTotal);
        console.log('state.addedItems:: ', state.addedItems);
        return {
            ...state,
            addedItems: cartData, //[...state.addedItems, cartData],
            totalCartPrice: newTotal,
            totalCartQty: newTotalQty,
            deliveryCharge: deliveryCharge,
            deliveryKm: deliveryKm
        }
    }
    if (action.type === REMOVE_TO_CART) {
        //check if the action id exists in the addedItems
        console.log('state.addedItems1:: ', state.addedItems);
        console.log('action.data:: ', action.data);
        const cartData = (action.data.cart_list != undefined) ? action.data.cart_list : []

        //calculating the total
        let newTotal = action.data.cart_total //parseFloat(state.total) + parseFloat(cartData.price)
        let newTotalQty = action.data.total_qty
        let deliveryCharge = action.data.delivery_charges
        let deliveryKm = action.data.delivery_km
        console.log('newtotalCartPrice:: ', newTotal);
        console.log('state.addedItems:: ', state.addedItems);
        return {
            ...state,
            addedItems: cartData, //[...state.addedItems, cartData],
            totalCartPrice: newTotal,
            totalCartQty: newTotalQty,
            deliveryCharge: deliveryCharge,
            deliveryKm: deliveryKm
        }
    }
    if (action.type === GET_SELECT_STADIUM) {
        console.log('GET_SELECT_STADIUM:: ', state);
        return {
            ...state,
        }
    }
    if (action.type === SET_SELECTED_STADIUM) {
        console.log('action.data:: ', action.data);
        return {
            ...state,
            selectStadium: action.data, 
        }
    }

    if (action.type === GET_SELECT_STADIUM_DETAILS) {
        console.log('GET_SELECT_STADIUM_DETAILS:: ', state);
        return {
            ...state,
        }
    }
    if (action.type === SET_SELECTED_STADIUM_DETAILS) {
        console.log('action.data:: ', action.data);
        return {
            ...state,
            selectStadiumDetails: action.data, 
        }
    }
    
    return state

}

export default CartReducer
