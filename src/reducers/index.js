import {combineReducers} from 'redux';
//import DateReducer from './DateReducer';
//import DateSelectionReducer from './DateSelectionReducer';
import ChatReducer from './ChatReducer';
import LoginUserDataReducer from "./LoginUserDataReducer";
import CartReducer from './CartReducer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['selectStadium']
  };

export default combineReducers({
    //dates: DateReducer,
    //state: (state = {}) => state,
    // LoginUserData: LoginUserDataReducer,
    // chats: ChatReducer,
    carts: CartReducer, //persistReducer(persistConfig, CartReducer) //CartReducer
    //selectedChatId: ChatSelectedReducer
});