import { Apis } from '../utils/Apis';

const errorData = {
  status: false,
  data: {},
  message: 'Something went wrong, please try again later.',
};


var updateDriveApi = function (myData, fileData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = 'driver/edit';
      let paramsData = JSON.stringify(myData);
      Apis.callFormdataApis(api, fileData, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};

const DriverService = {
  updateDriveApi,
};

export default DriverService;
