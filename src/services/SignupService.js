import { Apis } from '../utils/Apis';

const errorData = {
  status: false,
  data: {},
  message: 'Something went wrong, please try again later.',
};

var signupDriver = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/register';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};



const SignupService = {
  signupDriver,
};

export default SignupService;
