import LoginService from './LoginService';
import SignupService from './SignupService';
import OrderService from './OrderService';
import StateCityService from './StateCityService';
import DriverService from './DriverService';
import NotificationService from './NotificationService';
import WebService from './WebService';




export {
  LoginService,
  SignupService,
  OrderService,
  StateCityService,
  DriverService,
  NotificationService,
  WebService,
};