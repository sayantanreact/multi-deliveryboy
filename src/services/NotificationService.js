import { Apis } from '../utils/Apis';

const errorData = {
  status: false,
  data: {},
  message: 'Something went wrong, please try again later.',
};

var getNotification = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = 'common/user_push_notiication';
      let paramsData = JSON.stringify(myData);

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};

const NotificationService = {
  getNotification,
};

export default NotificationService;
