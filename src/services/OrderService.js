import { Apis } from '../utils/Apis';

const errorData = {
  "status": false,
  "data": {},
  "message": "Something went wrong, please try again later."
}

var getCurrentOrder = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/current_order';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}

var changeCurrentOrderStatus = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/current_order_status';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}

var updateOrderStatus = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/update_order_status';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}
var pastOrderApi = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/order_history';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};
var getOrderDetail = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'order/get_order_details';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};
var getReport = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/get_report_screen';
      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};
const OrderService = {
  getCurrentOrder,
  changeCurrentOrderStatus,
  updateOrderStatus, getReport,
  pastOrderApi,
  getOrderDetail,
};

export default OrderService;