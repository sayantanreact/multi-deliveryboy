import { Apis } from '../utils/Apis';

const errorData = {
  status: false,
  data: {},
  message: 'Something went wrong, please try again later.',
};

var webPagesApi = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'common/get_web_page';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result);
        }.bind(this),
        function () {
          onFail(errorData);
        }.bind(this),
      );
    } catch (e) {
      onFail(errorData);
    }
  });
};

const WebService = {
  webPagesApi,
};

export default WebService;
