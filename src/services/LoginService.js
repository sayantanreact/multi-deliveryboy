import { Apis } from '../utils/Apis';

const errorData = {
  "status": false,
  "data": {},
  "message": "Something went wrong, please try again later."
}


var loginDriver = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let paramsData = JSON.stringify(myData);
      let api = 'driver/login';

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}




var verifyOtpDriver = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = (myData.isPage == 'signup') ? 'driver/verify_register_otp' :
        (myData.isPage == 'login') ? 'driver/verify_login_otp' : 'driver/verify_edit_otp';
      let paramsData = JSON.stringify(myData);

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}


var resendOtpDriver = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = 'driver/resend_otp';
      let paramsData = JSON.stringify(myData);

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}


var activeInactivestatus = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = 'driver/active_status';
      let paramsData = JSON.stringify(myData);

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}

var updateDeviceToken = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = 'driver/update_device_token';
      let paramsData = JSON.stringify(myData);

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}

var updateDriverLocation = function (myData) {
  return new Promise(function (onSuccess, onFail) {
    try {
      let api = 'driver/update_driver_location';
      let paramsData = JSON.stringify(myData);

      Apis.callApis(api, paramsData).then(
        function (result) {
          onSuccess(result)
        }.bind(this),
        function () {
          onFail(errorData)
        }.bind(this),
      );

    } catch (e) {
      onFail(errorData);
    }
  });
}

const LoginService = {
  loginDriver,
  resendOtpDriver,
  verifyOtpDriver,
  activeInactivestatus, updateDeviceToken, updateDriverLocation
};

export default LoginService;