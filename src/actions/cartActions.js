
import { 
    SET_SELECTED_STADIUM_DETAILS, GET_SELECT_STADIUM_DETAILS,
    SET_SELECTED_STADIUM, GET_SELECT_STADIUM, 
    GET_CART, ADD_TO_CART, REMOVE_TO_CART, 
    SUB_QUANTITY, ADD_QUANTITY, ADD_SHIPPING } from './action-types/cart-actions'

//add cart action
export const getCartData = (data) => {
    return {
        type: GET_CART,
        data
    }
}

//add cart action
export const addToCart = (data) => {
    return {
        type: ADD_TO_CART,
        data
    }
}
//remove item action
export const removeToCart = (data) => {
    return {
        type: REMOVE_TO_CART,
        data
    }
}
//subtract qt action
export const subtractQuantity = (id) => {
    return {
        type: SUB_QUANTITY,
        id
    }
}
//add qt action
export const addQuantity = (id) => {
    return {
        type: ADD_QUANTITY,
        id
    }
}

//add cart action
export const getSelectedStadiumData = (data) => {
    return {
        type: GET_SELECT_STADIUM,
        data
    }
}

//add select stadium
export const selectStadium = (data) => {
    return {
        type: SET_SELECTED_STADIUM,
        data
    }
}

//get stadium details action
export const getSelectedStadiumDetailsData = (data) => {
    return {
        type: GET_SELECT_STADIUM_DETAILS,
        data
    }
}

//add stadium details stadium
export const selectStadiumDetailsData = (data) => {
    return {
        type: SET_SELECTED_STADIUM_DETAILS,
        data
    }
}
