
//Types should be in const to avoid typos and duplication since it's a string and could be easily miss spelled
export const GET_CART = 'GET_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_TO_CART = 'REMOVE_TO_CART';
export const SUB_QUANTITY = 'SUB_QUANTITY';
export const ADD_QUANTITY = 'ADD_QUANTITY';
export const ADD_SHIPPING = 'ADD_SHIPPING';
export const GET_SELECT_STADIUM = 'GET_SELECT_STADIUM';
export const SET_SELECTED_STADIUM = 'SET_SELECTED_STADIUM';
export const GET_SELECT_STADIUM_DETAILS = 'GET_SELECT_STADIUM_DETAILS';
export const SET_SELECTED_STADIUM_DETAILS = 'SET_SELECTED_STADIUM_DETAILS';