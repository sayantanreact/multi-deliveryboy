import { RF } from "./responsive"

export const fontSizes={
   'ONE':RF(14),
   'TWO':RF(15),
   'THREE':RF(16),
   'FOUR':RF(17),
   'FIVE':RF(18),
}

export const fontFamilies={
    'ROBO_LT':"Roboto-Light",
    'ROBO_MD':"Roboto-Medium",
    'ROBO_RG':"Roboto-Regular",
    'ROBO_TH':"Roboto-Thin",
    'MICRO_BLD':"Microsoft-YaHei-Bold-Font",
}