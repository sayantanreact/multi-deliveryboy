const images = {
  splashImage: require("./../assets/images/SplashScreen.png"),
  menuActive1: require("./../assets/images/menu-icon1_active.png"),
  logo: require("./../assets/images/logo.png"),
  deliveryBoy: require("./../assets/images/deliveryBoy.png"),
  menuActive1: require("./../assets/images/menu-icon1_active.png"),
  menu1: require("./../assets/images/menu-icon1.png"),
  menuActive2: require("./../assets/images/menu-icon2_active.png"),
  menu2: require("./../assets/images/menu-icon2.png"),
  menuActive3: require("./../assets/images/menu-icon3_active.png"),
  menu3: require("./../assets/images/menu-icon3.png"),
  menuActive4: require("./../assets/images/menu-icon4_active.png"),
  menu4: require("./../assets/images/menu-icon4.png"),
  menuActive5: require("./../assets/images/menu-icon5_active.png"),
  menu5: require("./../assets/images/menu-icon5.png"),
  closeIcon: require("./../assets/images/close-icon_green.png"),
  backIcon: require("./../assets/images/Frame.png"),
  leftArrow: require("./../assets/images/leftarr.png"),
  email: require("./../assets/images/email.png"),
  apple: require("./../assets/images/apple.png"),
  facebook: require("./../assets/images/facebook.png"),
  google: require("./../assets/images/google.png"),
  vendor: require("./../assets/images/vendor.png"),
  signinbg: require("./../assets/images/signinbg.png"),
  filter: require("./../assets/images/filter.png"),
  firewater: require("./../assets/images/firewater.png"),
  lil: require("./../assets/images/lil.png"),
  heart: require("./../assets/images/heart.png"),
  orderimg: require("./../assets/images/orderimg.png"),
  orderimg2: require("./../assets/images/orderimg2.png"),
  checkbox: require("./../assets/images/checkbox.png"),
  clickcheckbox: require("./../assets/images/clickcheckbox.png"),
  orderico1: require("./../assets/images/orderico1.png"),
  avatar: require("./../assets/images/avatar.png"),
  homebg: require("./../assets/images/home-bg.png"),
  clock: require("./../assets/images/clock.png"),
  downarrow1: require("./../assets/images/clorarrow.png"),
  orderdelivered: require("./../assets/images/orderdelivered.png"),
  homemenu: require("./../assets/images/homemenu.png"),
  cameraicon: require("./../assets/images/cameraicon.png"),
  homedrawer: require("./../assets/images/homedrawer.png"),
  rightarrow: require("./../assets/images/rightarrow.png"),
  logout: require("./../assets/images/logout.png"),
  termsico: require("./../assets/images/termsico.png"),
};

export default images;
