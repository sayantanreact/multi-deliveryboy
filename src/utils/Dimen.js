import { Dimensions } from 'react-native';

const Dimen = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    hasNotch: Dimensions.get('window').height >= 812
}

export default Dimen