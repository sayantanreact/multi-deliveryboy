import { View, StyleSheet } from 'react-native';
import Color from './Color';
import Fonts from './Fonts';

const Styles = StyleSheet.create({
    Header: {
        flexDirection: 'row',
        justifyContent: "space-between",
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15,
        alignItems: "center",
    },
    HeaderLeft: {
        width: "20%",
    },
    HeaderMiddle: {
        width: "60%",
        alignItems: "center",

    },
    HeaderText: {
        fontSize: 40,
    },
    HeaderRight: {
        width: "20%",
        alignItems: "flex-end",
    },
    HeaderLeftBackButton: {
        position: 'absolute',
        left: 0,
        top: 6
    },
    Heading: {
        fontFamily: Fonts.Bold,
        fontSize: 26,
        color: Color.blackColor,
        marginBottom: 0,
    },
    subText: {
        fontFamily: Fonts.regular,
        fontSize: 14,
        color: Color.greyColor,
    },
    cardViewStyle: {
        backgroundColor: Color.whiteColor,
        shadowColor: '#000000',
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        shadowOffset: { height: 2, width: 0 },
        padding: 15,
        borderRadius: 15,
        margin: 5,
        marginTop: 10,
    },
    shadowStyle: {
        shadowColor: '#83A2CB',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 0,
        elevation: 0
    },
    butonContainer: {
        backgroundColor: "#06CD56",
        fontWeight: "600",
        fontSize: 18,

    },
    buttonPrimary: {
        backgroundColor: Color.primaryColor,
        height: 46,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 30,
        paddingRight: 30,
    },
    buttonPrimaryText: {
        color: Color.whiteColor,
        fontSize: 18,
        fontWeight: 'bold',
    },
    buttonSecondary: {
        backgroundColor: Color.darkgreyColor,
        height: 46,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 30,
        paddingRight: 30,
    },
    buttonSecondaryText: {
        color: Color.whiteColor,
        fontSize: 18,
        fontWeight: 'bold',
    },
    buttonOutline: {
        borderWidth: 1,
        borderColor: Color.primaryColor,
        height: 46,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    buttonOutlineText: {
        color: Color.primaryColor,
        fontSize: 18,
        fontWeight: 'bold',
    },
    TextCenter: {
        alignItems: 'center',
    },
    TextCenter1: {
        textAlign: 'center',
    },
    TextWhite: {
        color: Color.whiteColor,
    },
    TextBlack: {
        color: Color.blackColor,
    },
    TextGreen: {
        color: Color.greenColor,
    },
    TextMuted: {
        color: Color.darkgreyColor,
    },
    TextPrimary: {
        color: Color.primaryColor,
    },
    TextSecondary: {
        color: Color.secondaryColor,
    },
    TextBlue: {
        color: Color.blueColor,
    },
    TextBold: {
        fontWeight: 'bold',
    },
    formControl: {
        height: 44,
        color: Color.secondaryColor,
        marginBottom: 15,
        backgroundColor: Color.whiteColor,
        borderColor: Color.lightgreyColor,
        borderWidth: 1,
        borderRadius: 4,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
    },
    FormControltextArea: {
        color: Color.secondaryColor,
        borderColor: Color.blueColor,
        borderWidth: 1,
        borderRadius: 22,
        marginBottom: 10,
        backgroundColor: Color.whiteColor,
        paddingLeft: 15,
        paddingRight: 15,
    },
    Row: {
        flexDirection: 'row',
    },
    Imgfluid: {
        width: '100%',
        height: '100%',
    },
    mt_15: {
        marginTop: 15,
    },
    pt_15: {
        paddingTop: 15,
    },
    pb_10: {
        paddingBottom: 10,
    },
    pb_15: {
        paddingBottom: 15,
    },
    mt_30: {
        marginTop: 30,
    },
    mb_5: {
        marginBottom: 5,
    },
    mb_10: {
        marginBottom: 10,
    },
    mb_20: {
        marginBottom: 20,
    },
    mb_30: {
        marginBottom: 30,
    },
    mb_50: {
        marginBottom: 50,
    },
    mb_70: {
        marginBottom: 70,
    },
    pt_30: {
        paddingTop: 30,
    },
    commonLRpadding: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    commonTBpadding: {
        paddingTop: 20,
        paddingBottom: 20,
    },
    commonTBpadding1: {
        paddingTop: 12,
        paddingBottom: 12,
    },
    commonPadding: {
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    brd10: {
        borderRadius: 10,
    },
    brd5: {
        borderRadius: 5,
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderStyle: "solid",
        borderColor: Color.offwhiteColor,
    },
    modalMain: {
        backgroundColor: Color.blackColor,
        height: "100%",
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    orderModalContentHolder: {
        backgroundColor: Color.whiteColor,
        width: '100%',
    },
    modalBtnHolder: {
        display: 'flex',
        flexDirection: 'row'
    },
    modalBtn: {
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: Color.greyColor,
        backgroundColor: Color.whiteColor,
        textAlign: 'center',
        alignItems: 'center',
    },
    modalBtn1: {
        backgroundColor: Color.orangeColor,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: Color.orangeColor,
    },
    topHeaderMain: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-between',
        marginBottom: 20
    },
    topHeaderBackButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingTop: (Platform.OS == "android") ? 0 : 3
    },
    topHeadingtext: {
        textTransform: 'capitalize',
        fontSize: 20,
        //fontWeight: "600",
        //marginRight: "27%",
        textAlign: "center",
        fontFamily: Fonts.semibold
    },
    topHeadingRight: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

});
export default Styles