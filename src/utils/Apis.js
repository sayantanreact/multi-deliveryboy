import {
  Platform
} from 'react-native';

const devUrl = 'https://multistore.wordsystech.com/api/';
const liveUrl = 'https://www.multimultistores.com/api/'; //'https://beta.multimultistores.com/api/';

const baseUrl = liveUrl //devUrl;

/* api call for raw data */
var callApis = function (apiName, myData) {
  var apiUrl = baseUrl + apiName;
  //var apiUrl = 'http://myvtd.site/ci/likemyapp/webservice/api/api/' + apiName;
  console.log('apiUrl:: ', apiUrl);
  //console.log('apiName:: ', apiName);
  console.log('myData:: ', myData);
  //alert(JSON.stringify(myData));
  return new Promise(function (onSuccess, onFail) {
    fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-API-KEY': 'ee4454DSFDSDSF556566'
      },
      body: myData,
      // JSON.stringify({
      //   "appcall":"1",
      //   "start":"0",
      //   "perpage":"10",
      //   "searchField":"yoga_type_name",
      //   "searchString":"b"
      // }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //return responseJson.movies;
        console.log('responseJson:: ', responseJson);
        onSuccess(responseJson);
      })
      .catch((error) => {
        console.log(error);
        onFail(error);
      });
  });
}

const createFormData = (photo, body) => {
  console.log('bodybody:: ', body);
  const data = new FormData();
  if (photo != null) {
    console.log('photophoto:: ', photo);
    // console.log('photo.fileName:: ', photo.fileName);
    //console.log('photo.length:: ', photo.length);
    if (photo.length > 0) {
      for (var i = 0; i < photo.length; i++) {
        data.append(photo[i].serverFileName, {
          name: photo[i].fileName,
          type: photo[i].type,
          uri: Platform.OS === "android" ? photo[i].uri : photo[i].uri.replace("file://", "")
        });
      }

      console.log("FormData data:::",data)

    }

    // data.append(photo.serverFileName, {
    //   name: photo.fileName,
    //   type: photo.type,
    //   uri: Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
    // });

    //console.log("FormData data:::",data)
    
  }

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });
  //data.append("apiCall", "1");
  // console.log('data data:: ', data);
  return data;
};

var callFormdataApis = function (apiName, fileData, myData) {
  var apiUrl = baseUrl + apiName;
  if (apiName == "http://myvtd.site/ci/testApi/api.php?function=testFileupload") {
    apiUrl = apiName;
  }

  console.log('apiUrl:: ', apiUrl);
  //console.log('apiName:: ', apiName);
  console.log('myData:: ', myData);
  //console.log('myData.length:: ', myData.length);
  console.log('fileData ', fileData);

  //alert(JSON.stringify(myData));
  return new Promise(function (onSuccess, onFail) {
    fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json', //'multipart/form-data',
        'Content-Type': 'multipart/form-data',
        'X-API-KEY': 'ee4454DSFDSDSF556566'
        //'Authorization': global.loginUserTokenType + ' ' + global.loginUserAccessToken //'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMzMmJiN2MxZTA0MjBhNWZhZWNkMGVjZDc4ZjIyYmZjMDUzYTA1NjUxOWE1YjhiOTIyYWUwYjFkYTI1MWUyMjdiYmIyOGNlNGQzZTRlNzdmIn0.eyJhdWQiOiIxIiwianRpIjoiYzMyYmI3YzFlMDQyMGE1ZmFlY2QwZWNkNzhmMjJiZmMwNTNhMDU2NTE5YTViOGI5MjJhZTBiMWRhMjUxZTIyN2JiYjI4Y2U0ZDNlNGU3N2YiLCJpYXQiOjE1OTYwOTkxNjQsIm5iZiI6MTU5NjA5OTE2NCwiZXhwIjoxNjI3NjM1MTY0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.jOa5a03e8LiMUSPUM54AtAu_YEWegzYb9_cYZaO1IiQtVxLtUdkx6Xmh5Fa1VSeD9dks1GmF3owPuuhnE2G-67D4b2SbPNuSiBftb4rISyQCmgDBV9PS1pQoZkjYcueHcduM9jiHhsxMI5cXHcpgiJg4t2xL0i_c1nT38-QCXqbq7DSZNmoDs-dkdoXjCzz2zLA20hklGwTmXB3MO8nCGVSxSH5sqOg_yYYkNMVoJtwQreCsb4LkMWL9P__x95X99MN1SthO6DmXun5GIPoBW1ndy-TA3TtD9B31NSbwM21TkKCbuDZxQILeleeaGzKE4W526mmm5LMfkWviuNdjUe8sM49eFKa3aEe4m4bAdnHGjRabfwR2u3NMUWdn6fU9oHStqzzOXekwSA2kRGqQsDXuWDzXXJyPccPvr_HhFsDC6cIze_HJIhlhw8MHiTBt3bC3EMXVj_VazvyjG7iCjQj18JuNFwwpuyxdZh8jDRy-TVKNPJH6bdPTPQ2yv2zpwPrV3gKMhGKgGKEKGNwRJgFiLchBCEHbcsuQqGidw26dTZabF712-C1jBkxl3Gz6CHfcqAkAXHHc6S76V0BJYd-sEXCJqyv46aDltYbqXOJekXnxDi9n2oYZydcDmbSBwoPI9IcNcZ_C--pITsfMiiBjOhy-L0eEVqm4l2fhoTY'
      },
      body: createFormData(fileData, JSON.parse(myData))

      // JSON.stringify({
      //   "appcall":"1",
      //   "start":"0",
      //   "perpage":"10",
      //   "searchField":"yoga_type_name",
      //   "searchString":"b"
      // }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //return responseJson.movies;
        console.log('responseJson:: ', responseJson);
        onSuccess(responseJson);
      })
      .catch((error) => {
        console.log('error::' , error);
        onFail(error);
      });
  });
}

var callApi = function (apiName) {
  var apiUrl = baseUrl + apiName;
  console.log('yes');

  console.log('apiUrl:: ', apiUrl);
  //console.log('apiName:: ', apiName);
  // console.log('myData:: ', myData);
  //alert(JSON.stringify(myData));
  return new Promise(function (onSuccess, onFail) {
    fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json', //'multipart/form-data',
        'Content-Type': 'multipart/form-data',
        'Authorization': global.loginUserTokenType + ' ' + global.loginUserAccessToken //'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMzMmJiN2MxZTA0MjBhNWZhZWNkMGVjZDc4ZjIyYmZjMDUzYTA1NjUxOWE1YjhiOTIyYWUwYjFkYTI1MWUyMjdiYmIyOGNlNGQzZTRlNzdmIn0.eyJhdWQiOiIxIiwianRpIjoiYzMyYmI3YzFlMDQyMGE1ZmFlY2QwZWNkNzhmMjJiZmMwNTNhMDU2NTE5YTViOGI5MjJhZTBiMWRhMjUxZTIyN2JiYjI4Y2U0ZDNlNGU3N2YiLCJpYXQiOjE1OTYwOTkxNjQsIm5iZiI6MTU5NjA5OTE2NCwiZXhwIjoxNjI3NjM1MTY0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.jOa5a03e8LiMUSPUM54AtAu_YEWegzYb9_cYZaO1IiQtVxLtUdkx6Xmh5Fa1VSeD9dks1GmF3owPuuhnE2G-67D4b2SbPNuSiBftb4rISyQCmgDBV9PS1pQoZkjYcueHcduM9jiHhsxMI5cXHcpgiJg4t2xL0i_c1nT38-QCXqbq7DSZNmoDs-dkdoXjCzz2zLA20hklGwTmXB3MO8nCGVSxSH5sqOg_yYYkNMVoJtwQreCsb4LkMWL9P__x95X99MN1SthO6DmXun5GIPoBW1ndy-TA3TtD9B31NSbwM21TkKCbuDZxQILeleeaGzKE4W526mmm5LMfkWviuNdjUe8sM49eFKa3aEe4m4bAdnHGjRabfwR2u3NMUWdn6fU9oHStqzzOXekwSA2kRGqQsDXuWDzXXJyPccPvr_HhFsDC6cIze_HJIhlhw8MHiTBt3bC3EMXVj_VazvyjG7iCjQj18JuNFwwpuyxdZh8jDRy-TVKNPJH6bdPTPQ2yv2zpwPrV3gKMhGKgGKEKGNwRJgFiLchBCEHbcsuQqGidw26dTZabF712-C1jBkxl3Gz6CHfcqAkAXHHc6S76V0BJYd-sEXCJqyv46aDltYbqXOJekXnxDi9n2oYZydcDmbSBwoPI9IcNcZ_C--pITsfMiiBjOhy-L0eEVqm4l2fhoTY'
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //return responseJson.movies;
        console.log('responseJson:: ', responseJson);
        onSuccess(responseJson);
      })
      .catch((error) => {
        console.log(error);
        onFail(error);
      });
  });

}
var callGetApis = function (apiName) {
  var apiUrl = baseUrl + apiName;

  console.log('apiUrl:: ', apiUrl);

  //alert(JSON.stringify(myData));
  return new Promise(function (onSuccess, onFail) {
    fetch(apiUrl, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-API-KEY': 'ee4454DSFDSDSF556566',
        //'Content-Type': 'multipart/form-data',
        //'Authorization': global.loginUserTokenType + ' ' + global.loginUserAccessToken //'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMzMmJiN2MxZTA0MjBhNWZhZWNkMGVjZDc4ZjIyYmZjMDUzYTA1NjUxOWE1YjhiOTIyYWUwYjFkYTI1MWUyMjdiYmIyOGNlNGQzZTRlNzdmIn0.eyJhdWQiOiIxIiwianRpIjoiYzMyYmI3YzFlMDQyMGE1ZmFlY2QwZWNkNzhmMjJiZmMwNTNhMDU2NTE5YTViOGI5MjJhZTBiMWRhMjUxZTIyN2JiYjI4Y2U0ZDNlNGU3N2YiLCJpYXQiOjE1OTYwOTkxNjQsIm5iZiI6MTU5NjA5OTE2NCwiZXhwIjoxNjI3NjM1MTY0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.jOa5a03e8LiMUSPUM54AtAu_YEWegzYb9_cYZaO1IiQtVxLtUdkx6Xmh5Fa1VSeD9dks1GmF3owPuuhnE2G-67D4b2SbPNuSiBftb4rISyQCmgDBV9PS1pQoZkjYcueHcduM9jiHhsxMI5cXHcpgiJg4t2xL0i_c1nT38-QCXqbq7DSZNmoDs-dkdoXjCzz2zLA20hklGwTmXB3MO8nCGVSxSH5sqOg_yYYkNMVoJtwQreCsb4LkMWL9P__x95X99MN1SthO6DmXun5GIPoBW1ndy-TA3TtD9B31NSbwM21TkKCbuDZxQILeleeaGzKE4W526mmm5LMfkWviuNdjUe8sM49eFKa3aEe4m4bAdnHGjRabfwR2u3NMUWdn6fU9oHStqzzOXekwSA2kRGqQsDXuWDzXXJyPccPvr_HhFsDC6cIze_HJIhlhw8MHiTBt3bC3EMXVj_VazvyjG7iCjQj18JuNFwwpuyxdZh8jDRy-TVKNPJH6bdPTPQ2yv2zpwPrV3gKMhGKgGKEKGNwRJgFiLchBCEHbcsuQqGidw26dTZabF712-C1jBkxl3Gz6CHfcqAkAXHHc6S76V0BJYd-sEXCJqyv46aDltYbqXOJekXnxDi9n2oYZydcDmbSBwoPI9IcNcZ_C--pITsfMiiBjOhy-L0eEVqm4l2fhoTY'
      },
      //body: createFormData(fileData, JSON.parse(myData))
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //return responseJson.movies;
        console.log('responseJson:: ', responseJson);
        onSuccess(responseJson);
      })
      .catch((error) => {
        console.log('error::', error);
        onFail(error);
      });
  });
};
export const Apis = {
  callApi,
  callApis,
  callFormdataApis,
  callGetApis
};
