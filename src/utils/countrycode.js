// const root_path = `../Assets/countryimages`;
export const COUNTRY_LIST = [
        {
            // "name": "Afghanistan",
            "dialCode": "+93",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Albania",
            "dialCode": "+355",
            // "icon": require(`${root_path}/al.png`)
        },
        {
            // "name": "Algeria",
            "dialCode": "+213",
            // "icon": require(`${root_path}/dz.png`)
        },
        {
            // "name": "American Samoa",
            "dialCode": "+1684",
            // "icon": require(`${root_path}/as.png`)
        },
        {
            // "name": "Andorra",
            "dialCode": "+376",
            // "icon": require(`${root_path}/ad.png`)
        },
        {
            // "name": "Angola",
            "dialCode": "+244",
            // "icon": require(`${root_path}/ao.png`)
        },
        {
            // "name": "Anguilla",
            "dialCode": "+1264",
            // "icon": require(`${root_path}/ai.png`)
        },
        {
            // "name": "Antigua and Barbuda",
            "dialCode": "+1268",
            // "icon": require(`${root_path}/ag.png`)
        },
        {
            // "name": "Argentina",
            "dialCode": "+54",
            // "icon": require(`${root_path}/ar.png`)
        },
        {
            // "name": "Armenia",
            "dialCode": "+374",
            // "icon": require(`${root_path}/am.png`)
        },
        {
            // "name": "Aruba",
            "dialCode": "+297",
            // "icon": require(`${root_path}/aw.png`)
        },
        {
            // "name": "Australia",
            "dialCode": "+61",
            // "icon": require(`${root_path}/au.png`)
        },
        {
            // "name": "Austria",
            "dialCode": "+43",
            // "icon": require(`${root_path}/at.png`)
        },
        {
            // "name": "Azerbaijan",
            "dialCode": "+994",
            // "icon": require(`${root_path}/az.png`)
        },
        {
            // "name": "Bahamas",
            "dialCode": "+1242",
            // "icon": require(`${root_path}/bs.png`)
        },
        {
            // "name": "Bahrain‬‎",
            "dialCode": "+973",
            // "icon": require(`${root_path}/bh.png`)
        },
        {
            // "name": "Bangladesh",
            "dialCode": "+880",
            // "icon": require(`${root_path}/bd.png`)
        },
        {
            // "name": "Barbados",
            "dialCode": "+1246",
            // "icon": require(`${root_path}/bb.png`)
        },
        {
            // "name": "Belarus",
            "dialCode": "+375",
            // "icon": require(`${root_path}/by.png`)
        },
        {
            // "name": "Belgium",
            "dialCode": "+32",
            // "icon": require(`${root_path}/be.png`)
        },
        {
            // "name": "Belize",
            "dialCode": "+501",
            // "icon": require(`${root_path}/bz.png`)
        },
        {
            // "name": "Benin",
            "dialCode": "+229",
            // "icon": require(`${root_path}/bj.png`)
        },
        {
            // "name": "Bermuda",
            "dialCode": "+1441",
            // "icon": require(`${root_path}/bm.png`)
        },
        {
            // "name": "Bhutan",
            "dialCode": "+975",
            // "icon": require(`${root_path}/bt.png`)
        },
        {
            // "name": "Bolivia",
            "dialCode": "+591",
            // "icon": require(`${root_path}/bo.png`)
        },
        {
            // "name": "Bosnia and Herzegovina",
            "dialCode": "+387",
            // "icon": require(`${root_path}/ba.png`)
        },
        {
            // "name": "Botswana",
            "dialCode": "+267",
            // "icon": require(`${root_path}/bw.png`)
        },
        {
            // "name": "Brazil",
            "dialCode": "+55",
            // "icon": require(`${root_path}/br.png`)
        },
        {
            // "name": "British Indian Ocean Territory",
            "dialCode": "+246",
            // "icon": require(`${root_path}/io.png`)
        },
        {
            // "name": "British Virgin Islands",
            "dialCode": "+1284",
            // "icon": require(`${root_path}/vg.png`)
        },
        {
            // "name": "Brunei",
            "dialCode": "+673",
            // "icon": require(`${root_path}/bn.png`)
        },
        {
            // "name": "Bulgaria",
            "dialCode": "+359",
            // "icon": require(`${root_path}/bg.png`)
        },
        {
            // "name": "Burkina Faso",
            "dialCode": "+226",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Burundi",
            "dialCode": "+257",
            // "icon": require(`${root_path}/bi.png`)
        },
        {
            // "name": "Cambodia",
            "dialCode": "+855",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Cameroon",
            "dialCode": "+237",
            // "icon": require(`${root_path}/cm.png`)
        },
        {
            // "name": "Canada",
            "dialCode": "+1",
            // "icon": require(`${root_path}/ca.png`)
        },
        {
            // "name": "Cape Verde",
            "dialCode": "+238",
            // "icon": require(`${root_path}/cv.png`)
        },
        {
            // "name": "Caribbean Netherlands",
            "dialCode": "+599",
            // "icon": require(`${root_path}/bq.png`)
        },
        {
            // "name": "Cayman Islands",
            "dialCode": "+1345",
            // "icon": require(`${root_path}/ky.png`)
        },
        {
            // "name": "Central African Republic",
            "dialCode": "+236",
            // "icon": require(`${root_path}/cf.png`)
        },
        {
            // "name": "Chad (Tchad)",
            "dialCode": "+235",
            // "icon": require(`${root_path}/td.png`)
        },
        {
            // "name": "Chile",
            "dialCode": "+56",
            // "icon": require(`${root_path}/cl.png`)
        },
        {
            // "name": "China",
            "dialCode": "+86",
            // "icon": require(`${root_path}/cn.png`)
        },
        {
            // "name": "Christmas Island",
            "dialCode": "+61",
            // "icon": require(`${root_path}/cx.png`)
        },
        {
            // "name": "Cocos Islands",
            "dialCode": "+61",
            // "icon": require(`${root_path}/cc.png`)
        },
        {
            // "name": "Colombia",
            "dialCode": "+57",
            // "icon": require(`${root_path}/co.png`)
        },
        {
            // "name": "Comoros‬‎",
            "dialCode": "+269",
            // "icon": require(`${root_path}/km.png`)
        },
        {
            // "name": "Congo",
            "dialCode": "+243",
            // "icon": require(`${root_path}/cd.png`)
        },
        {
            // "name": "Congo",
            "dialCode": "+242",
            // "icon": require(`${root_path}/cg.png`)
        },
        {
            // "name": "Cook Islands",
            "dialCode": "+682",
            // "icon": require(`${root_path}/ck.png`)
        },
        {
            // "name": "Costa Rica",
            "dialCode": "+506",
            // "icon": require(`${root_path}/cr.png`)
        },
        {
            // "name": "Croatia",
            "dialCode": "+385",
            // "icon": require(`${root_path}/hr.png`)
        },
        {
            // "name": "Cuba",
            "dialCode": "+53",
            // "icon": require(`${root_path}/cu.png`)

        },
        {
            // "name": "Curaçao",
            "dialCode": "+599",
            // "icon": require(`${root_path}/cw.png`)
        },
        {
            // "name": "Cyprus",
            "dialCode": "+357",
            // "icon": require(`${root_path}/cy.png`)
        },
        {
            // "name": "Czech Republic",
            "dialCode": "+420",
            // "icon": require(`${root_path}/cz.png`)
        },
        {
            // "name": "Denmark",
            "dialCode": "+45",
            // "icon": require(`${root_path}/dk.png`)
        },
        {
            // "name": "Djibouti",
            "dialCode": "+253",
            // "icon": require(`${root_path}/dj.png`)
        },
        {
            // "name": "Dominica",
            "dialCode": "+1767",
            // "icon": require(`${root_path}/dm.png`)
        },
        {
            // "name": "Dominican Republic",
            "dialCode": "+1",
            // "icon": require(`${root_path}/do.png`)
        },
        {
            // "name": "Ecuador",
            "dialCode": "+593",
            // "icon": require(`${root_path}/ec.png`)
        },
        {
            // "name": "Egypt‬‎",
            "dialCode": "+20",
            // "icon": require(`${root_path}/eg.png`)
        },
        {
            // "name": "El Salvador",
            "dialCode": "+503",
            // "icon": require(`${root_path}/sv.png`)
        },
        {
            // "name": "Equatorial Guinea",
            "dialCode": "+240",
            // "icon": require(`${root_path}/gq.png`)
        },
        {
            // "name": "Eritrea",
            "dialCode": "+291",
            // "icon": require(`${root_path}/er.png`)
        },
        {
            // "name": "Estonia",
            "dialCode": "+372",
            // "icon": require(`${root_path}/ee.png`)
        },
        {
            // "name": "Ethiopia",
            "dialCode": "+251",
            // "icon": require(`${root_path}/et.png`)
        },
        {
            // "name": "Falkland Islands",
            "dialCode": "+500",
            // "icon": require(`${root_path}/fk.png`)
        },
        {
            // "name": "Faroe Islands",
            "dialCode": "+298",
            // "icon": require(`${root_path}/fo.png`)
        },
        {
            // "name": "Fiji",
            "dialCode": "+679",
            // "icon": require(`${root_path}/fj.png`)
        },
        {
            // "name": "Finland",
            "dialCode": "+358",
            // "icon": require(`${root_path}/fi.png`)
        },
        {
            // "name": "France",
            "dialCode": "+33",
            // "icon": require(`${root_path}/fr.png`)
        },
        {
            // "name": "French Guiana",
            "dialCode": "+594",
            // "icon": require(`${root_path}/gf.png`)
        },
        {
            // "name": "French Polynesia",
            "dialCode": "+689",
            // "icon": require(`${root_path}/pf.png`)
        },
        {
            // "name": "Gabon",
            "dialCode": "+241",
            // "icon": require(`${root_path}/ga.png`)
        },
        {
            // "name": "Gambia",
            "dialCode": "+220",
            // "icon": require(`${root_path}/gm.png`)
        },
        {
            // "name": "Georgia",
            "dialCode": "+995",
            // "icon": require(`${root_path}/ge.png`)
        },
        {
            // "name": "Germany",
            "dialCode": "+49",
            // "icon": require(`${root_path}/de.png`)
        },
        {
            // "name": "Ghana",
            "dialCode": "+233",
            // "icon": require(`${root_path}/gh.png`)
        },
        {
            // "name": "Gibraltar",
            "dialCode": "+350",
            // "icon": require(`${root_path}/gi.png`)
        },
        {
            // "name": "Greece",
            "dialCode": "+30",
            // "icon": require(`${root_path}/gr.png`)
        },
        {
            // "name": "Greenland",
            "dialCode": "+299",
            // "icon": require(`${root_path}/gr.png`)
        },
        {
            // "name": "Grenada",
            "dialCode": "+1473",
            // "icon": require(`${root_path}/gd.png`)
        },
        {
            // "name": "Guadeloupe",
            "dialCode": "+590",
            // "icon": require(`${root_path}/gp.png`)
        },
        {
            // "name": "Guam",
            "dialCode": "+1671",
            // "icon": require(`${root_path}/gu.png`)
        },
        {
            // "name": "Guatemala",
            "dialCode": "+502",
            // "icon": require(`${root_path}/gt.png`)
        },
        {
            // "name": "Guernsey",
            "dialCode": "+44",
            // "icon": require(`${root_path}/gg.png`)
        },
        {
            // "name": "Guinea",
            "dialCode": "+224",
            // "icon": require(`${root_path}/gn.png`)
        },
        {
            // "name": "Guinea-Bissau",
            "dialCode": "+245",
            // "icon": require(`${root_path}/gw.png`)
        },
        {
            // "name": "Guyana",
            "dialCode": "+592",
            // "icon": require(`${root_path}/gy.png`)
        },
        {
            // "name": "Haiti",
            "dialCode": "+509",
            // "icon": require(`${root_path}/ht.png`)
        },
        {
            // "name": "Honduras",
            "dialCode": "+504",
            // "icon": require(`${root_path}/hn.png`)
        },
        {
            // "name": "Hong Kong",
            "dialCode": "+852",
            // "icon": require(`${root_path}/hk.png`)
        },
        {
            // "name": "Hungary",
            "dialCode": "+36",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Iceland",
            "dialCode": "+354",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "India",
            "dialCode": "+91",
            // "icon": require(`${root_path}/in.png`)
        },
        {
            // "name": "Indonesia",
            "dialCode": "+62",
            // "icon": require(`${root_path}/id.png`)
        },
        {
            // "name": "Iran‬‎",
            "dialCode": "+98",
            // "icon": require(`${root_path}/ir.png`)
        },
        {
            // "name": "Iraq‬‎",
            "dialCode": "+964",
            // "icon": require(`${root_path}/iq.png`)
        },
        {
            // "name": "Ireland",
            "dialCode": "+353",
            // "icon": require(`${root_path}/ie.png`)
        },
        {
            // "name": "Isle of Man",
            "dialCode": "+44",
            // "icon": require(`${root_path}/im.png`)
        },
        {
            // "name": "Israel‬‎",
            "dialCode": "+972",
            // "icon": require(`${root_path}/il.png`)
        },
        {
            // "name": "Italy",
            "dialCode": "+39",
            // "icon": require(`${root_path}/it.png`)
        },
        {
            // "name": "Jamaica",
            "dialCode": "+1876",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Japan",
            "dialCode": "+81",
            // "icon": require(`${root_path}/jp.png`)
        },
        {
            // "name": "Jersey",
            "dialCode": "+44",
            // "icon": require(`${root_path}/ge.png`)
        },
        {
            // "name": "Jordan‬‎",
            "dialCode": "+962",
            // "icon": require(`${root_path}/jo.png`)
        },
        {
            // "name": "Kazakhstan",
            "dialCode": "+7",
            // "icon": require(`${root_path}/kz.png`)
        },
        {
            // "name": "Kenya",
            "dialCode": "+254",
            // "icon": require(`${root_path}/ke.png`)
        },
        {
            // "name": "Kiribati",
            "dialCode": "+686",
            // "icon": require(`${root_path}/ki.png`)
        },
        {
            // "name": "Kuwait‬‎",
            "dialCode": "+965",
            // "icon": require(`${root_path}/kw.png`)
        },
        {
            // "name": "Kyrgyzstan",
            "dialCode": "+996",
            // "icon": require(`${root_path}/kg.png`)
        },
        {
            // "name": "Laos",
            "dialCode": "+856",
            // "icon": require(`${root_path}/la.png`)
        },
        {
            // "name": "Latvia",
            "dialCode": "+371",
            // "icon": require(`${root_path}/lv.png`)
        },
        {
            // "name": "Lebanon",
            "dialCode": "+961",
            // "icon": require(`${root_path}/lb.png`)
        },
        {
            // "name": "Lesotho",
            "dialCode": "+266",
            // "icon": require(`${root_path}/ls.png`)
        },
        {
            // "name": "Liberia",
            "dialCode": "+231",
            // "icon": require(`${root_path}/lr.png`)
        },
        {
            // "name": "Libya",
            "dialCode": "+218",
            // "icon": require(`${root_path}/ly.png`)
        },
        {
            // "name": "Liechtenstein",
            "dialCode": "+423",
            // "icon": require(`${root_path}/li.png`)
        },
        {
            // "name": "Lithuania",
            "dialCode": "+370",
            // "icon": require(`${root_path}/lt.png`)
        },
        {
            // "name": "Luxembourg",
            "dialCode": "+352",
            // "icon": require(`${root_path}/lu.png`)
        },
        {
            // "name": "Macau",
            "dialCode": "+853",
            // "icon": require(`${root_path}/mo.png`)
        },
        {
            // "name": "Macedonia",
            "dialCode": "+389",
            // "icon": require(`${root_path}/mk.png`)
        },
        {
            // "name": "Madagascar",
            "dialCode": "+261",
            // "icon": require(`${root_path}/mg.png`)
        },
        {
            // "name": "Malawi",
            "dialCode": "+265",
            // "icon": require(`${root_path}/mw.png`)
        },
        {
            // "name": "Malaysia",
            "dialCode": "+60",
            // "icon": require(`${root_path}/my.png`)
        },
        {
            // "name": "Maldives",
            "dialCode": "+960",
            // "icon": require(`${root_path}/mv.png`)
        },
        {
            // "name": "Mali",
            "dialCode": "+223",
            // "icon": require(`${root_path}/ml.png`)
        },
        {
            // "name": "Malta",
            "dialCode": "+356",
            // "icon": require(`${root_path}/mt.png`)
        },
        {
            // "name": "Marshall Islands",
            "dialCode": "+692",
            // "icon": require(`${root_path}/mh.png`)
        },
        {
            // "name": "Martinique",
            "dialCode": "+596",
            // "icon": require(`${root_path}/mq.png`)
        },
        {
            // "name": "Mauritania",
            "dialCode": "+222",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Mauritius",
            "dialCode": "+230",
            // "icon": require(`${root_path}/mu.png`)
        },
        {
            // "name": "Mayotte",
            "dialCode": "+262",
            // "icon": require(`${root_path}/yt.png`)

        },
        {
            // "name": "Mexico",
            "dialCode": "+52",
            // "icon": require(`${root_path}/mx.png`)

        },
        {
            // "name": "Micronesia",
            "dialCode": "+691",
            // "icon": require(`${root_path}/fm.png`)

        },
        {
            // "name": "Moldova",
            "dialCode": "+373",
            // "icon": require(`${root_path}/md.png`)

        },
        {
            // "name": "Monaco",
            "dialCode": "+377",
            // "icon": require(`${root_path}/mc.png`)

        },
        {
            // "name": "Mongolia",
            "dialCode": "+976",
            // "icon": require(`${root_path}/mn.png`)

        },
        {
            // "name": "Montenegro",
            "dialCode": "+382",
            // "icon": require(`${root_path}/me.png`)

        },
        {
            // "name": "Montserrat",
            "dialCode": "+1664",
            // "icon": require(`${root_path}/ms.png`)

        },
        {
            // "name": "Morocco",
            "dialCode": "+212",
            // "icon": require(`${root_path}/ma.png`)

        },
        {
            // "name": "Mozambique",
            "dialCode": "+258",
            // "icon": require(`${root_path}/mz.png`)

        },
        {
            // "name": "Myanmar",
            "dialCode": "+95",
            // "icon": require(`${root_path}/mm.png`)

        },
        {
            // "name": "Namibia",
            "dialCode": "+264",
            // "icon": require(`${root_path}/na.png`)

        },
        {
            // "name": "Nauru",
            "dialCode": "+674",
            // "icon": require(`${root_path}/af.png`)

        },
        {
            // "name": "Nepal",
            "dialCode": "+977",
            // "icon": require(`${root_path}/np.png`)

        },
        {
            // "name": "Netherlands",
            "dialCode": "+31",
            // "icon": require(`${root_path}/nl.png`)
        },
        {
            // "name": "New Caledonia",
            "dialCode": "+687",
            // "icon": require(`${root_path}/nc.png`)
        },
        {
            // "name": "New Zealand",
            "dialCode": "+64",
            // "icon": require(`${root_path}/nz.png`)
        },
        {
            // "name": "Nicaragua",
            "dialCode": "+505",
            // "icon": require(`${root_path}/ni.png`)
        },
        {
            // "name": "Niger",
            "dialCode": "+227",
            // "icon": require(`${root_path}/ne.png`)
        },
        {
            // "name": "Nigeria",
            "dialCode": "+234",
            // "icon": require(`${root_path}/ng.png`)
        },
        {
            // "name": "Niue",
            "dialCode": "+683",
            // "icon": require(`${root_path}/nu.png`)
        },
        {
            // "name": "Norfolk Island",
            "dialCode": "+672",
            // "icon": require(`${root_path}/nf.png`)
        },
        {
            // "name": "North Korea",
            "dialCode": "+850",
            // "icon": require(`${root_path}/kp.png`)
        },
        {
            // "name": "Northern Mariana Islands",
            "dialCode": "+1670",
            // "icon": require(`${root_path}/mp.png`)
        },
        {
            // "name": "Norway",
            "dialCode": "+47",
            // "icon": require(`${root_path}/no.png`)
        },
        {
            // "name": "Oman",
            "dialCode": "+968",
            // "icon": require(`${root_path}/om.png`)
        },
        {
            // "name": "Pakistan",
            "dialCode": "+92",
            // "icon": require(`${root_path}/pk.png`)
        },
        {
            // "name": "Palau",
            "dialCode": "+680",
            // "icon": require(`${root_path}/pw.png`)
        },
        {
            // "name": "Palestine",
            "dialCode": "+970",
            // "icon": require(`${root_path}/ps.png`)
        },
        {
            // "name": "Panama",
            "dialCode": "+507",
            // "icon": require(`${root_path}/pa.png`)
        },
        {
            // "name": "Papua New Guinea",
            "dialCode": "+675",
            // "icon": require(`${root_path}/pg.png`)
        },
        {
            // "name": "Paraguay",
            "dialCode": "+595",
            // "icon": require(`${root_path}/py.png`)
        },
        {
            // "name": "Peru",
            "dialCode": "+51",
            // "icon": require(`${root_path}/pe.png`)
        },
        {
            // "name": "Philippines",
            "dialCode": "+63",
            // "icon": require(`${root_path}/ph.png`)
        },
        {
            // "name": "Poland",
            "dialCode": "+48",
            // "icon": require(`${root_path}/pl.png`)


        },
        {
            // "name": "Portugal",
            "dialCode": "+351",
            // "icon": require(`${root_path}/pt.png`)
        },
        {
            // "name": "Puerto Rico",
            "dialCode": "+1",
            // "icon": require(`${root_path}/pr.png`)
        },
        {
            // "name": "Qatar",
            "dialCode": "+974",
            // "icon": require(`${root_path}/qa.png`)
        },
        {
            // "name": "Réunion",
            "dialCode": "+262",
            // "icon": require(`${root_path}/re.png`)
        },
        {
            // "name": "Romania",
            "dialCode": "+40",
            // "icon": require(`${root_path}/ro.png`)
        },
        {
            // "name": "Russia",
            "dialCode": "+7",
            // "icon": require(`${root_path}/ru.png`)
        },
        {
            // "name": "Rwanda",
            "dialCode": "+250",
            // "icon": require(`${root_path}/rw.png`)
        },
        {
            // "name": "Saint Barthélemy",
            "dialCode": "+590",
            // "icon": require(`${root_path}/bl.png`)
        },
        {
            // "name": "Saint Helena",
            "dialCode": "+290",
            // "icon": require(`${root_path}/sh.png`)
        },
        {
            // "name": "Saint Kitts and Nevis",
            "dialCode": "+1869",
            // "icon": require(`${root_path}/kn.png`)
        },
        {
            // "name": "Saint Lucia",
            "dialCode": "+1758",
            // "icon": require(`${root_path}/lc.png`)
        },
        {
            // "name": "Saint Martin",
            "dialCode": "+590",
            // "icon": require(`${root_path}/mf.png`)
        },
        {
            // "name": "Saint Pierre and Miquelon",
            "dialCode": "+508",
            // "icon": require(`${root_path}/pm.png`)
        },
        {
            // "name": "Saint Vincent and the Grenadines",
            "dialCode": "+1784",
            // "icon": require(`${root_path}/vc.png`)
        },
        {
            // "name": "Samoa",
            "dialCode": "+685",
            // "icon": require(`${root_path}/ws.png`)
        },
        {
            // "name": "San Marino",
            "dialCode": "+378",
            // "icon": require(`${root_path}/sm.png`)
        },
        {
            // "name": "São Tomé and Príncipe",
            "dialCode": "+239",
            // "icon": require(`${root_path}/st.png`)
        },
        {
            // "name": "Saudi Arabia",
            "dialCode": "+966",
            // "icon": require(`${root_path}/sa.png`)
        },
        {
            // "name": "Senegal",
            "dialCode": "+221",
            // "icon": require(`${root_path}/sn.png`)
        },
        {
            // "name": "Serbia",
            "dialCode": "+381",
            // "icon": require(`${root_path}/rs.png`)
        },
        {
            // "name": "Seychelles",
            "dialCode": "+248",
            // "icon": require(`${root_path}/sc.png`)
        },
        {
            // "name": "Sierra Leone",
            "dialCode": "+232",
            // "icon": require(`${root_path}/sl.png`)
        },
        {
            // "name": "Singapore",
            "dialCode": "+65",
            // "icon": require(`${root_path}/sg.png`)
        },
        {
            // "name": "Sint Maarten",
            "dialCode": "+1721",
            // "icon": require(`${root_path}/sx.png`)
        },
        {
            // "name": "Slovakia",
            "dialCode": "+421",
            // "icon": require(`${root_path}/sk.png`)
        },
        {
            // "name": "Slovenia",
            "dialCode": "+386",
            // "icon": require(`${root_path}/si.png`)
        },
        {
            // "name": "Solomon Islands",
            "dialCode": "+677",
            // "icon": require(`${root_path}/sb.png`)
        },
        {
            // "name": "Somalia",
            "dialCode": "+252",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "South Africa",
            "dialCode": "+27",
            // "icon": require(`${root_path}/za.png`)
        },
        {
            // "name": "South Korea",
            "dialCode": "+82",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "South Sudan",
            "dialCode": "+211",
            // "icon": require(`${root_path}/ss.png`)
        },
        {
            // "name": "Spain",
            "dialCode": "+34",
            // "icon": require(`${root_path}/es.png`)
        },
        {
            // "name": "Sri Lanka",
            "dialCode": "+94",
            // "icon": require(`${root_path}/lk.png`)
        },
        {
            // "name": "Sudan",
            "dialCode": "+249",
            // "icon": require(`${root_path}/sd.png`)
        },
        {
            // "name": "Suriname",
            "dialCode": "+597",
            // "icon": require(`${root_path}/sr.png`)
        },
        {
            // "name": "Svalbard and Jan Mayen",
            "iso2": "sj",
            "dialCode": "+47",
            // "icon": require(`${root_path}/sj.png`)
        },
        {
            // "name": "Swaziland",
            "dialCode": "+268",
            // "icon": require(`${root_path}/sz.png`)
        },
        {
            // "name": "Sweden",
            "dialCode": "+46",
            // "icon": require(`${root_path}/se.png`)
        },
        {
            // "name": "Switzerland",
            "dialCode": "+41",
            // "icon": require(`${root_path}/ch.png`)
        },
        {
            // "name": "Syria",
            "dialCode": "+963",
            // "icon": require(`${root_path}/sy.png`)
        },
        {
            // "name": "Taiwan",
            "dialCode": "+886",
            // "icon": require(`${root_path}/tw.png`)
        },
        {
            // "name": "Tajikistan",
            "dialCode": "+992",
            // "icon": require(`${root_path}/tj.png`)
        },
        {
            // "name": "Tanzania",
            "dialCode": "+255",
            // "icon": require(`${root_path}/tz.png`)
        },
        {
            // "name": "Thailand",
            "dialCode": "+66",
            // "icon": require(`${root_path}/th.png`)
        },
        {
            // "name": "Timor-Leste",
            "dialCode": "+670",
            // "icon": require(`${root_path}/tl.png`)
        },
        {
            // "name": "Togo",
            "dialCode": "+228",
            // "icon": require(`${root_path}/tg.png`)
        },
        {
            // "name": "Tokelau",
            "dialCode": "+690",
            // "icon": require(`${root_path}/tk.png`)
        },
        {
            // "name": "Tonga",
            "dialCode": "+676",
            // "icon": require(`${root_path}/to.png`)
        },
        {
            // "name": "Trinidad and Tobago",
            "dialCode": "+1868",
            // "icon": require(`${root_path}/af.png`)
        },
        {
            // "name": "Tunisia",
            "dialCode": "+216",
            // "icon": require(`${root_path}/tn.png`)
        },
        {
            // "name": "Turkey",
            "dialCode": "+90",
            // "icon": require(`${root_path}/tr.png`)
        },
        {
            // "name": "Turkmenistan",
            "dialCode": "+993",
            // "icon": require(`${root_path}/tm.png`)
        },
        {
            // "name": "Turks and Caicos Islands",
            "dialCode": "+1649",
            // "icon": require(`${root_path}/tc.png`)
        },
        {
            // "name": "Tuvalu",
            "dialCode": "+688",
            // "icon": require(`${root_path}/tv.png`)
        },
        {
            // "name": "U.S. Virgin Islands",
            "dialCode": "+1340",
            // "icon": require(`${root_path}/vi.png`)
        },
        {
            // "name": "Uganda",
            "dialCode": "+256",
            // "icon": require(`${root_path}/ug.png`)
        },
        {
            // "name": "Ukraine",
            "dialCode": "+380",
            // "icon": require(`${root_path}/ua.png`)
        },
        {
            // "name": "United Arab Emirates",
            "dialCode": "+971",
            // "icon": require(`${root_path}/ae.png`)
        },
        {
            // "name": "United Kingdom",
            "dialCode": "+44",
            // "icon": require(`${root_path}/gb.png`)
        },
        {
            // "name": "United States",
            "dialCode": "+1",
            // "icon": require(`${root_path}/us.png`)
        },
        {
            // "name": "Uruguay",
            "dialCode": "+598",
            // "icon": require(`${root_path}/uy.png`)
        },
        {
            // "name": "Uzbekistan",
            "dialCode": "+998",
            // "icon": require(`${root_path}/uz.png`)
        },
        {
            // "name": "Vanuatu",
            "dialCode": "+678",
            // "icon": require(`${root_path}/vu.png`)
        },
        {
            // "name": "Vatican City",
            "dialCode": "+39",
            // "icon": require(`${root_path}/va.png`)
        },
        {
            // "name": "Venezuela",
            "dialCode": "+58",
            // "icon": require(`${root_path}/ve.png`)
        },
        {
            // "name": "Vietnam",
            "dialCode": "+84",
            // "icon": require(`${root_path}/vn.png`)
        },
        {
            // "name": "Wallis and Futuna",
            "dialCode": "+681",
            // "icon": require(`${root_path}/wf.png`)
        },
        {
            // "name": "Western Sahara",
            "dialCode": "+212",
            // "icon": require(`${root_path}/eh.png`)
        },
        {
            // "name": "Yemen",
            "dialCode": "+967",
            // "icon": require(`${root_path}/ye.png`)
        },
        {
            // "name": "Zambia",
            "dialCode": "+260",
            // "icon": require(`${root_path}/zm.png`)
        },
        {
            // "name": "Zimbabwe",
            "dialCode": "+263",
            // "icon": require(`${root_path}/af.png`)
        },
    ]
