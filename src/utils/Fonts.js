import { RF } from "./responsive"

export const fontSizes={
   'ONE':RF(14),
   'TWO':RF(15),
   'THREE':RF(16),
   'FOUR':RF(17),
   'FIVE':RF(18),
}


const fonts = {
    black: 'Poppins-Black',
    light: 'Poppins-Light',
    medium: 'Poppins-Medium',
    bold: 'Poppins-Bold',
    regular: "Poppins-Regular",
    semiBold: "Poppins-SemiBold"
}

export default fonts